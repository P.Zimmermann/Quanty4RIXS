This is the Source Code package for Quanty4RIXS (dated September 2017).
You can use it and redistribute it to who ever finds it useful. 
The only request I have, is that a reference to me (Patric Zimmermann, Utrecht University, 2017) as initial author is given when the source code is copied elsewhere.

A corresponding publication with some instructions is also in the making. 


1.)
The folder Quanty4RIXS-Source contains the last version of the source code. You can copy its content whereever you like. 
Then just open the file Quanty4RIXS.m in Matlab (v2016a or newer) and run it. This should open the GUI. On start up tit checks that the Quanty binary and the repository is found, if it is not in the standard path it should ask for the path.


2.)
The “RepositoryFolder” is the folder named “Quanty4RIXS”. The contents of this repository need to be present to run the GUI.

On Mac OSX that is: /Users/<Your Username>/Quanty4RIXS/
On Windows it can also be in C:\ Quanty4RIXS			

In any case the folder and its subdirectories need write access, as the configuration is saved in the file Quanty4RIXS.cfg.

The subfolder "Quanty" contains the lua modules/sections that are being used to assemble the final lua scripts.
The subfolder "Resources" contains the RCN parameter file and the programs configurations file (Quanty4RIXS.cfg). 
The config file is in Matlab format, if you change the ending to .mat you can open it in Matlab and edit it when required. 
(It contains also a path that needs perhaps some adjustment to get the program to run properly.)


NOTE: Currently we are testing the software under Windows, there are still some issues when running the scripts with Quanty. Especially RIXS calculations can lead to memory issues. We recommend using it under Mac OSX.

The calculation of the “Observables”, “EnergyLevelDiagrams (ELD)” (also called Tanabe Sugano Diagram), and 1s XAS calculations are already working under Windows, but RIXS can trigger errors from Quanty. (This is like to be due to an out if memory.)