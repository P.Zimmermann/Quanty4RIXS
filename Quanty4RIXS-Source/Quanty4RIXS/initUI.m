function handles = initUI(hObject, eventdata, handles)
%INITUI Summary of this function goes here
%   Detailed explanation goes here

    % --------------------------------------------
    debugMsg( 'calling initUI()', 2 )
    % --------------------------------------------
    updateStatus(['Initialising Quanty4RIXS...'])                               
    % --------------------------------------------
    
    
%     handles.appRootDir   = fullfile(getenv('HOME'),'Quanty4RIXS');

    
    
    
    % Choose default command line output for ctm4rixs
    handles.output = hObject;    
    handles.hBackend = viewerBackend();                 % create new instance of the Backend and store handle in handles.hBackend
    handles.spectroscopy = '1s2pRIXS';
    % --------------------------------------------
    % maybe move later to appSettings (to save values to file on exit)
    % used in uiOptions()
    initOptions.calcEigenVals = 1;
    initOptions.noOfEV        = 45;
    initOptions.noOfGS        = 2;    
    handles.options = initOptions;
    clear initOptions;
    % --------------------------------------------
    % resize MainFigure and shift Settings
    hMainFig = findobj('Tag','mainFigure');
    figPos = get(hMainFig,'Position');
%     set(findobj('Tag','mainFigure'),'Position',[figPos(1) figPos(2) 605 880])       % reducing width to 611, (is 1210 in GUIDE to create settings panel)  
    set(hMainFig,'Position',[figPos(1) figPos(2) 605 1075])       % reducing width to 611, (is 1210 in GUIDE to create settings panel)         
    % --------------------------------------------
    % add ToolTipString to uiSuggTable
    suggInfoStr = sprintf(['Fill table with values with the Add-Button'  '\n'...
                   '(current data in GUI is then added to the table).' '\n'...
                   'Remove all but one column with the Delete-Button' '\n'...
                   'Enable with checkBox above this table.' '\n'...
                   'When the Ion selection is changed the stored values' '\n'...
                   'are applied to the GUI.' '\n'...
                   'Values can be changed in the table or GUI.']);
    set(findobj('Tag','uiTab_suggestVals'),'TooltipString',suggInfoStr)
    % --------------------------------------------
  
%     load([handles.appRootDir filesep 'Resources' filesep 'ctm4rixs.cfg'],'settings','-mat')             % loads variable "settings"
    handles = loadAppSettingsFile( handles );
%     handles.appSettings = appSettings;
%     lastDir = settings.lastDir   
    % handles.appSettings.lastDir = pwd;
    % handles.appSettings.lastDir = handles.appSettings.lastDir;

    % ----------------------------------------------------------------------------------
    % ----------------------------------------------------------------------------------
    hidePanelAtomic()
    hidePanelCF()
    hidePanelBroad()
    hidePanelPlotting()
    % --------------------------------------------
    clearAtomicParameter( )
    % --------------------------------------------
    % disable all CT ui items (default is CT off)
    disableCT()
    tagDisable({'edit_Ds_GS','edit_Dt_GS','edit_M_GS'})
    tagDisable({'pm_oxiLevel'})
    setStrTag({'pm_oxiLevel'},{'-'})
    % --------------------------------------------
    
    handles = load_RCNvalues(handles);
        
    % --------------------------------------------
    updateStatus(['Initialising Quanty4RIXS...Done!'])                           
    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------
    % --------------------------------------------


end

