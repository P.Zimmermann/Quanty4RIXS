function pb_addSuggEntry_Callback( hObject, eventdata, handles)


    % ----------------------------------------------------------------------------------------
    debugMsg( 'calling pb_addSuggEntry_Callback()', 2 )
    % ----------------------------------------------------------------------------------------

    if isfield(handles,'currentZ')                      % check if data loaded  
        
        % ----------------------------------------------
        rowData  = {};
        suggData = {};
        % ----------------------------------------------
        % NOT values -> Params
        paraGS = readAllParamsFromGUI(handles, 'GS');
        paraIS = readAllParamsFromGUI(handles, 'IS');
        paraFS = readAllParamsFromGUI(handles, 'FS');   


        % ----------------------------------------------
        % Extract current scaling parameter from GUI 
        currIon = paraGS.Ion;           
        % ----------------------------------------------
        % here adding the symmetry (popup box in CF panel)
        rowData{1} = 'Symmetry'
        pm_Strings = getStrTag( ['pm_Symmetry'] ); 
        suggData{1} = pm_Strings{getValTag( ['pm_Symmetry'] )}; 
        
        % ----------------------------------------------
        fieldsGS = repmat(fieldnames(paraGS), numel(paraGS), 1);
        valuesGS = struct2cell(paraGS);                    
        for k=[3:length(valuesGS)]                                      % skipping line 1 and 2 (Ion and Z stays the same)
    %         thisLine = [char(fieldsGS{k}) ' = ' num2str(valuesGS{k})];        
            rowData{length(rowData)+1}   = char(fieldsGS{k});
            suggData{length(suggData)+1} = num2str(valuesGS{k});
        end

        fieldsIS = repmat(fieldnames(paraIS), numel(paraIS), 1);
        valuesIS = struct2cell(paraIS);                    
        for k=[3:length(valuesIS)]                              
            rowData{length(rowData)+1}   = char(fieldsIS{k});
            suggData{length(suggData)+1} = num2str(valuesIS{k});
        end 

        fieldsFS = repmat(fieldnames(paraFS), numel(paraFS), 1);
        valuesFS = struct2cell(paraFS);                    
        for k=[3:length(valuesFS)]                    
            rowData{length(rowData)+1}   = char(fieldsFS{k});
            suggData{length(suggData)+1} = num2str(valuesFS{k});
        end    

        suggData = suggData';
        % ----------------------------------------------
%         rowData
%          currIon
        % ----------------------------------------------
        hUITab = findobj('Tag','uiTab_suggestVals');
        
        % get current uiTable to check if Ions exists already, getindex
        tabCols = get(hUITab,'ColumnName');
        tabData = get(hUITab,'Data');
         
        found=0;
        for i=1:length(tabCols)
%             currIon
%             tabCols{i}
            if strcmp(currIon, tabCols{i})
                found = i;
                % ---------------------------------------------------
                debugMsg( [' !!! Found Ion string! Replacing entry in sugg Table. found = ' num2str(found) '  -- in pb_addSuggEntry_Callback '], 0 )
                % ---------------------------------------------------             
            end    
        end
        
        % --- if existing then replace ---
        if found > 0
            newColData = tabCols;                       % column data unchanged
            
            tabData(:,found) = suggData;                 % overwrite found data set (column No: found)                     
%              tabData = suggData;                 % overwrite found data set (column No: found)                     
            newData = tabData';                          % newTabData are new sugg values
        else
            % Ion not in list, appending as new column to table data
            newColData = tabCols;
            newColData{length(newColData)+1} = currIon;     % add new Ion name

            oldDat = tabData(:,:) ;          % preparing old Sugg Data to be merged  
            newDat = suggData(:,:) ;         % preparing new Sugg Data    
            newData = [oldDat'; newDat'];    % joing data           
        end
 
        % ----------------------------------------------

        newData = newData';
        
        % ----------------------------------------------
%         suggTable = handles.appSettings.suggTab  

    %     suggIons   = cell2mat(suggTable.Properties.RowNames)            % here we convert the cell to array (to allow editing)
%         suggIons   = suggTable.Properties.RowNames' ;           
%         suggDq     = num2cell(suggTable.valDq)' ;
%         suggSlater = num2cell(suggTable.valSlater)' ;


    %     suggData = {suggIons{:}; suggDq{:}; suggSlater{:}}';        % assemble data for the uiTable

%         suggData = {suggDq{:}; suggSlater{:}}';        % assemble data for the uiTable
        

%     size(newColData)
%          colWidth = {50 50}
        colWidth = repmat({50},size(newColData,1),1)';
        
        set(hUITab,'RowName',rowData)                            % this is easier, but does not allowe editing        
        set(hUITab,'ColumnName',newColData);
%         set(hUITab,'ColumnWidth',colWidth);
%         set(hUITab,'ColumnWidth','auto');
        set(hUITab,'ColumnWidth',colWidth);
        set(hUITab,'Data',newData);

       
        
        
    else
        
        
        hWdlg = warndlg({'No Ion selected, nothing to add.'; 'Select ion before adding parameter.'},'!! Note !!') ;      
        set(hWdlg,'windowstyle','modal','FontSize',12)       % MODAL: STAY ON TOP        

        
        disp(' !!!  NO  Z selected nothing to add ')


    end





    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------
  

end

