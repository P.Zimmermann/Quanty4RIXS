function [ output_args ] = checkEnergyRanges( handles )
%CHECKENERGYRANGES Summary of this function goes here
%   Detailed explanation goes here

    min1 = str2num(getStrTag('edit_Emin1'));
    max1 = str2num(getStrTag('edit_Emax1'));
    
    min2 = str2num(getStrTag('edit_Emin2'));
    max2 = str2num(getStrTag('edit_Emax2'));
    
    diff1 = str2num(getStrTag('edit_dE1'));
    diff2 = str2num(getStrTag('edit_dE2'));    
    
    % check if min1 < max1, exchange if not
    if min1 > max1
        setStrTag('edit_Emin1',num2str(max1,'%f1.1'))
        setStrTag('edit_Emax1',num2str(min1,'%f1.1'))
        tmp=min1; min1=max1; max1=tmp;
        clear tmp
    end
    % check if min2 < max2, exchange if not
    if min2 > max2
        setStrTag('edit_Emin1',num2str(max2,'%f1.1'))
        setStrTag('edit_Emax1',num2str(min2,'%f1.1'))
        tmp=min2; min2=max2; max2=tmp;
        clear tmp
    end    
    % ------------------------------------------------ 
    
    N1  = length(min1:diff1:max1);
    N2 = length(min2:diff2:max2);
    numberOfPoints = N1 * N2;
    % ------------------------------------------------
    % when more than 10000 poingts then display warning message
    if numberOfPoints > 50000
        infoMsg = {'You are calculating a large map.',...
                   ' ',...
                   ['Incident Range: [' num2str(min1) ':' num2str(diff1) ':' num2str(max1) ']    N1 = ' num2str(N1) ' Points' ],...
                   ' ',...
                   ['Emission Range: [' num2str(min2) ':' num2str(diff2) ':' num2str(max2) ']    N2 = ' num2str(N2) ' Points' ],...
                   ' ',...
                   [' That results in a total of N1 x N2 = ' num2str(N1) ' x ' num2str(N2) ' = ' num2str(N1*N2) ],...
                   '',...
                   '(20k Points: ~1MB)'...
                    } ;
                
        sizeDialog()
    end        
            
               

    % --- Display Message Box with size
    function sizeDialog()
        % -----------------------------------------------------------------------------
        % find position fo mainfigure and place uiPeriodic above
        hMainFigPOS = get(findobj('Tag','mainFigure'),'Position'); 
        % -----------------------------------------------------------------------------
        hDia = dialog('Name','Map Size Warning');
%         diaPos = get(hDia,'Position');
        set(hDia,...
            'Position',[hMainFigPOS(1)-60 hMainFigPOS(2)+20 400 280],...
            'windowstyle','modal')
        
        txt = uicontrol('Parent',hDia,...
                   'Style','text',...
                   'Position',[5 70 380 180],...
                   'FontSize',14,...
                   'String',infoMsg);

        btn = uicontrol('Parent',hDia,...
                   'Position',[150 20 100 25],...
                   'FontSize',14,...                   
                   'String','Close',...
                   'Callback','delete(gcf)');    
    end    
    
    
    



end

