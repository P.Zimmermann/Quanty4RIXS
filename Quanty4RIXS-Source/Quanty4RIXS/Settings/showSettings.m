function [ output_args ] = showSettings( input_args )
%SHOWSETTINGS Summary of this function goes here
%   Detailed explanation goes here


    debugMsg( 'calling showSettings()', 1 )
    % --------------------------------------------
    % hiding panels without changing check boxes!!
    if getValTag('cb_expertAtomic') == 1             % IF ON    
        hidePanelAtomic()
    end    
    if getValTag('cb_expertCF') == 1             % IF ON      
        hidePanelCF()
    end
    if getValTag('cb_expertBroad') == 1             % IF ON
        hidePanelBroad()
    end    
    if getValTag('cb_Plotting') == 1             % IF ON      
        hidePanelPlotting()
    end
    % --------------------------------------------
    % adjust size of mainFigure
    newHeight = 530;
    hMainFig = findobj('Tag','mainFigure');    
    mainFigPos = get(hMainFig,'Position');
    shiftDown = 530 - mainFigPos(4);
    set(hMainFig,'Position',[mainFigPos(1) mainFigPos(2)-shiftDown mainFigPos(3) newHeight]);       % shifting settings panel to same position as config panel     

    
    % --------------------------------------------
    % shift left to show, and adjust Y position
    dxShift = 600;
%     dyShift = 230;
    dyShift = 270;
    
    hPanSet = findobj('Tag','uiPan_Settings');    
    panSettPos = get(hPanSet,'Position');
    set(hPanSet,'Position',[panSettPos(1)-dxShift panSettPos(2)+dyShift panSettPos(3) panSettPos(4)]);       % shifting settings panel to same position as config panel     

    hideControl({'uiPan_Configuration'})                                                      % hide Config Panel
    showControl({'uiPan_Settings'})                                                           % show Settings panel 

    uistack(findobj('Tag','uiPan_Settings'),'top');             % BRING TOP FRONT
    % --------------------------------------------



end

