function [ output_args ] = hideSettings( input_args )
%HIDESETTINGS Summary of this function goes here
%   Detailed explanation goes here

    debugMsg( 'calling hideSettings()', 2 )
    % --------------------------------------------

    newHeight = 1075 - 305 - 195 - 105 - 205;
   
    mainFigPos = get(findobj('Tag','mainFigure'),'Position');
    shiftUp = mainFigPos(4) - newHeight;

    set(findobj('Tag','mainFigure'),'Position',[mainFigPos(1) mainFigPos(2)+shiftUp 605 newHeight])       % reducing width to 611, (is 1210 in GUIDE to create settings panel)  
    % --------------------------------------------


    
%     % turn back to old GUI state () the check boxes are not altere on hiding and still showing last state
%     if getValTag('cb_expertAtomic') == 0             % IF ON    
%         hidePanelAtomic()
%     end    
%     if getValTag('cb_expertCF') == 0             % IF ON        
%         hidePanelCF()
%     end
%     if getValTag('cb_expertBroad') == 0             % IF ON
%         hidePanelBroad()
%     end    
%     if getValTag('cb_Plotting') == 0             % IF ON      
%         hidePanelPlotting()
%     end
    

    
%     % --------------------------------------------
%     % adjust size of mainFigure
% %     newHeight = 1110 - 301 - 200 - 101 - 201
%     newHeight = 1110;
%     if getValTag('cb_expertAtomic') == 0             % IF OFF
%         newHeight = newHeight - 301;
%     end
%     if getValTag('cb_expertCF') == 0             % IF OFF
%         newHeight = newHeight - 200;
%     end    
%     if getValTag('cb_expertBroad') == 0             % IF OFF
%         newHeight = newHeight - 101;
%     end 
%     if getValTag('cb_Plotting') == 0             % IF OFF
%         newHeight = newHeight - 201;
%     end     
%     
%     diffHeight = 1110 - newHeight

    % --------------------------------------------
    % shift right to hide, and adjust Y position
    dxShift = 600;
    dyShift = 270;   
%     dyShift = 230;
%     if getValTag('cb_expertAtomic') == 1             % IF ON
%         dyShift = dyShift - 301;
%     end
%     if getValTag('cb_expertCF') == 1             % IF ON
%         dyShift = dyShift - 200;
%     end    
%     if getValTag('cb_expertBroad') == 1             % IF ON
%         dyShift = dyShift - 101;
%     end     

    hPanSet = findobj('Tag','uiPan_Settings');    
    panSettPos = get(hPanSet,'Position');
    set(hPanSet,'Position',[panSettPos(1)+dxShift panSettPos(2)-dyShift panSettPos(3) panSettPos(4)]);       % shifting settings panel to same position as config panel     
    clear panSettPos dxShift dyShift;
    
    hideControl({'uiPan_Settings'})                                                           % initial hide of settings

    % displaying config panel at correct hight again
    showControl({'uiPan_Configuration'})                                                           % initial hide of settings

%     dxShift = 00;
%     dyShift = 230      
%     hPanConf = findobj('Tag','uiPan_Configuration');    
%     panConfPos = get(hPanConf,'Position');
%     set(hPanConf,'Position',[panConfPos(1)+dxShift panConfPos(2)+dyShift panConfPos(3) panConfPos(4)]);       % shifting settings panel to same position as config panel     
% 

end

