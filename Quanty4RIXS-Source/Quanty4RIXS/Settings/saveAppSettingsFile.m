function saveAppSettingsFile( handles )
%SAVEAPPSETTINGSFILE Summary of this function goes here
%   Detailed explanation goes here

    debugMsg( 'calling saveAppSettingsFile()', 2 )
    debugMsg( 'saving settings to .cfg file', 0 )
    % --------------------------------------------------------------------------------

    settings = handles.appSettings

    % --------------------------------------------------------------------------------
    %     save([handles.appRootDir filesep 'Resources' filesep 'ctm4rixs.cfg'],'settings')      % old file             
    save([handles.appRootDir filesep 'Resources' filesep 'Quanty4RIXS.cfg'],'settings')                 
    % --------------------------------------------------------------------------------




end

