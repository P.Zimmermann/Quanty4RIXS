function  pb_delSuggEntry_Callback( hObject, eventdata, handles)


    % ----------------------------------------------------------------------------------------
    debugMsg( 'calling pb_delSuggEntry_Callback()', 0 )
    % ----------------------------------------------------------------------------------------

    


        
    deleteEntryUI()    
        
        
        
        
        
%         found=0;
%         for i=1:length(tabCols)
%             if colData{:} == tabCols{i}
%                 found = i
%             end    
%         end
%            
    
    
    




    % ======================================================================================
    % ======================================================================================
    function deleteEntryUI()
 
        % ----------------------------------------------------------------------------------------
        debugMsg( 'calling deleteEntryUI() :: Subfunction in pb_delSuggEntry_Callback', 2 )
        % ----------------------------------------------------------------------------------------
        % get handle from sugg uiTable
        hUITab = findobj('Tag','uiTab_suggestVals');
        % get current comun names from sugg uiTable
        tabCols = get(hUITab,'ColumnName')
        % -----------------------------------------------------------------------------
        %search and delete old existing Options figure
        hOld = findobj('Tag','uiFig_delSugg');          % should always be empty, but just in case 
        delete(hOld);
        % -----------------------------------------------------------------------------
        hFig_delSugg = figure;
        % -----------------------------------------------------------------------------
        % find position fo mainfigure and place deleteFig above
        hMainFigPOS = get(findobj('Tag','mainFigure'),'Position') 
        % -----------------------------------------------------------------------------        
        set(hFig_delSugg,...
            'Position',[hMainFigPOS(1)+200 hMainFigPOS(2)+200 200 160],...
            'Toolbar','none',...
            'Units','Pixel',...
            'windowstyle','modal',...       % MODAL: STAY ON TOP
            'Tag','uiFig_delSugg',...
            'Menubar','none',...            %'CloseRequestFcn',{delete(hFig_delSugg);},...
            'Name',['Delete entry'],...
            'NumberTitle','off');
        % -----------------------------------------------------------------------------
        % TEXT info
        txtDelInfo = uicontrol(hFig_delSugg,...
            'Style', 'text',...
            'Position',[0 80 200 60],...
            'String',{'Select the Ion string to be'; 'deleted from suggestion table.'; ' '; 'NOTE: Last cannot be removed.'},...             
            'FontSize',12,...
            'HorizontalAlignment','center',...
            'Tag',['txt_DelInfo']); 
  
        % POPUPMENU with ions strings to delete
        pm_delSugg = uicontrol(hFig_delSugg,...
            'Style', 'popupmenu',...
            'Position',[50 45 100 20],...
            'Tag','pm_deleteSugg',...            
            'String',tabCols,...             
            'FontSize',12,...
            'Units','Pixel',...
            'Value',1);             %            'Callback',{@checkOptions, handles});     
        
        % PUSHBUTTON delet ion 
        pb_PMdelete = uicontrol(hFig_delSugg,...
            'Style', 'pushbutton',...
            'Position',[55 10 85 25],...
            'Tag','pb_PMdelete',...            
            'String','Delete',...             
            'FontSize',12,...
            'Units','Pixel',...
            'Value',0,...
            'Callback',{@pb_PMdelete_Callback, handles});        
   
    end

    % ======================================================================================

    function pb_PMdelete_Callback( hObject, eventdata, handles )
        
        % ----------------------------------------------------------------------------------------
        debugMsg( 'calling pb_PMdelete_Callback() :: Subfunction in pb_delSuggEntry_Callback', 2 )
        % ----------------------------------------------------------------------------------------
        
        selPMdel = getValTag('pm_deleteSugg');               % selected entry in del popupmenu
                
        % get current uiTable data  
        hUITab = findobj('Tag','uiTab_suggestVals');        % handle to sugg uiTable
        tabCols = get(hUITab,'ColumnName');                  % current Columns names (Ions strings)
        tabData = get(hUITab,'Data');                        % current suggData in uiTable
        % --------------------------------------------         
        currIon = tabCols{selPMdel};

        % --------------------------------------------        
        newTabData = tabData;
        newTabCols = tabCols;
        
        if length(tabCols) > 1                      % NOTE Delete Any but Last entry
            newTabCols(selPMdel) = [];
            newTabData(:,selPMdel) = []; 
        end    
        
        % --------------------------------------------
%         newTabCols
%         newTabData
        % --------------------------------------------
        set(hUITab,'ColumnName',newTabCols)             % write NEW Column names to uiTable
        set(hUITab,'Data',newTabData)                   % write NEW sugg Data to uiTable
        % --------------------------------------------
        setStrTag('pm_deleteSugg',newTabCols)           % update delPM (popupmenu)
        setValTag('pm_deleteSugg',1)           % update delPM (popupmenu)
        % --------------------------------------------
        
        % --------------------------------------------
        % delete selSugg PM figure automatically when delete is clicked
        % uncommnet if mutli delete should be possible
        delete(findobj('Tag','uiFig_delSugg'));
        % --------------------------------------------
     
        
        
    end
    % ======================================================================================
    % ======================================================================================


end

