function handles = fillSettingsUI( handles )


    % ----------------------------------------------------------------------------------------
    debugMsg( 'calling fillSettingsUI()', 1 )
    % ----------------------------------------------------------------------------------------
    % restoring values in the settings Panel from handles.appSettings

    % DEBUGGING broken SETTINGS FILE
%     handles.appSettings.calcEigenVals = 1
%     handles.appSettings.calcRIXSmaps  = 0
%     handles.appSettings.noOfEV        = 45
%     handles.appSettings.noOfGS        = 10
    
    
    
    % ------------------------------------------------
    % load and restore last path settings 
    setStrTag('edit_QuantyPath',handles.appSettings.QuantyPath);   
    setStrTag('edit_QuantyHome',handles.appSettings.QuantyHome);   
    % ------------------------------------------------
    % plot RAW or interpolated
    setValTag('cb_plotRAW',handles.appSettings.plotRAW);       
    setValTag('rb_multithreadOn',handles.appSettings.dualThread);       
    % ------------------------------------------------

    % ------------------------------------------------
    % plot RAW or interpolated
    setValTag('cb_calcExpectVals',handles.appSettings.calcEigenVals);       
    setValTag('cb_calcELDiagram',handles.appSettings.calcELDiagram);       
    setValTag('cb_calcRIXSmaps',handles.appSettings.calcRIXSmaps);       
    setValTag('cb_calcXAS',handles.appSettings.calcXAS);  
    if getValTag('cb_calcXAS') == 1       
%         setValTag('cb_calcExpectVals',0); 
        tagDisable({'cb_calcExpectVals'})
    end
    % ------------------------------------------------
    
    
    % ----------------------------------------------------------------------------------------
    % writing energy ranges to settings GUI
    setStrTag('edit_Emin1',num2str(handles.appSettings.Emin1));
    setStrTag('edit_Emax1',num2str(handles.appSettings.Emax1));
    setStrTag('edit_dE1',num2str(handles.appSettings.dE1));
    setStrTag('edit_Emin2',num2str(handles.appSettings.Emin2));
    setStrTag('edit_Emax2',num2str(handles.appSettings.Emax2));
    setStrTag('edit_dE2',num2str(handles.appSettings.dE2));
    % ----------------------------------------------------------------------------------------
    % write data to settings panel/table
    setValTag('cb_SettingsSuggVals',handles.appSettings.suggestValsOn);
    
    %  Update suggestions values in uiTable with data in handles.AppSettings
    hUITab = findobj('Tag','uiTab_suggestVals');
    
    colNames = handles.appSettings.suggIons;                % colNames are the ionStrings
    set(hUITab,'ColumnName',colNames)                        

    suggTable = handles.appSettings.suggTab;
    suggRows = handles.appSettings.suggRows;
    set(hUITab,'RowName',suggRows)                         
%     suggData = suggTable{:,:}                       % assemble data for the uiTable
    suggData = suggTable;                         %  data for the uiTable
    set(hUITab,'Data',suggData);
    
    % ----------------------------------------------------------------------------------------



    
    
    

end

