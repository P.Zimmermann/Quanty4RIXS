function handles = loadAppSettingsFile( handles )
%LOADAPPSETTINGSFILE Summary of this function goes here
%   Detailed explanation goes here

    % ----------------------------------------------------------------------------------------
    debugMsg( 'calling loadAppSettingsFile()', 2 )
    debugMsg( 'loading settings from .cfg file and writing it to appSettings', 1 )
    % ----------------------------------------------------------------------------------------

    
%     dbgTmp1 = handles.appRootDir
    % check if repository exists
    if not(exist(handles.appRootDir) == 7)                  % 7 -> folder
        temp = uidirfile('','Select Quanty4RIXS repository folder')         
        if exist(temp) == 7
          handles.appRootDir = temp 
       else
           disp('WARNING: Quanty4RIXS repository folder could not be set!')
        end        
    end        
%     dbgTmp2 = handles.appRootDir
     
    
%     load([handles.appRootDir filesep 'Resources' filesep 'ctm4rixs.cfg'],'settings','-mat')             % loads old file with  variable "settings"
    load([handles.appRootDir filesep 'Resources' filesep 'Quanty4RIXS.cfg'],'settings','-mat')             % loads variable "settings"

    % ----------------------------------------------------------------------------------------
%    settings = rmfield(settings,'calcELDiagramm')    % debug remove after use
%    settings = rmfield(settings,'calcCFdiagram')     % debug remove after use
    % ----------------------------------------------------------------------------------------
    
    % ----------------------------------------------------------------------------------------
    % Check the program paths, if they dont exist then open file browser
%     Lastpath = settings.lastDir   
%     Qpath = settings.QuantyPath
%     exQpath = exist(settings.QuantyPath)
    
    if not(exist(settings.lastDir) == 7)                  % 7 -> folder
        settings.lastDir = '';
    end
        
    if not(exist(settings.QuantyPath) == 2)                  % 2 -> file
        [tempFile, tempPath] = uigetfile('','Select Quanty excutable file')
        %temp1 = fullfile(tempPath,tempFile)
        temp1 = [tempPath filesep tempFile]
        if not(temp1 == 0)
            if (exist(temp1) == 2)
                settings.QuantyPath = temp1 
            else
                disp('WARNING: Quanty Path could not be set!')
            end
        else
            disp('WARNING: Quanty Path could not be set!')          
        end
    end
    clear temp1

    if not(exist(settings.QuantyHome) == 7)                  % 7 -> folder
        temp2 = handles.appRootDir        
        if not(temp2 == 0)
            if (exist(temp2) == 7)
                settings.QuantyHome = temp2 
            else
                disp('WARNING: Quanty4RIXS repository folder could not be set!')
            end
        else
            disp('WARNING: Quanty4RIXS repository folder could not be set!')
        end
    end    
    clear temp2

    % ----------------------------------------------------------------------------------------
    
    handles.appSettings = settings;
    % ----------------------------------------------------------------------------------------

    
    % config file loads: settings
    % Eranges, suggTab
    settings                            % debug output
    clear settings;
    % ----------------------------------------------------------------------------------------

    % ------------------------------------------------
    % restoring values from handles.appSettings
    handles = fillSettingsUI( handles );
    % ------------------------------------------------    

   
  

end   