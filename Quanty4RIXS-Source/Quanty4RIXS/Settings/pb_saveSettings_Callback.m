function [ output_args ] = pb_saveSettings_Callback(hObject, eventdata, handles)
%PB_SAVESETTINGS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here


    % ------------------------------------------------
    debugMsg( 'calling pb_saveSettings_Callback()', 2 )
    % ------------------------------------------------
    updateStatus(['Settings saved! '])
    % ------------------------------------------------    
    
%     load([handles.appRootDir filesep 'Resources' filesep 'ctm4rixs.cfg'],'settings','-mat')
    % ------------------------------------------------
    % get current value from handles, and then overwrite from GUI (this avoid missing values e.g. uiOptions values)
    settings = handles.appSettings                              % NEEDED: OTHERWISE uiOptions Values are missing
    % lastDir NOT read from GUI, done manually here
    if isfield(handles.appSettings,'lastDir')
        settings.lastDir = handles.appSettings.lastDir;
    else
        settings.lastDir = ''; 
    end
    % ------------------------------------------------
    % load and read last path settings 
    settings.QuantyPath = getStrTag('edit_QuantyPath');   
    settings.QuantyHome = getStrTag('edit_QuantyHome');   
    % ------------------------------------------------
    % plot RAW or interpolated
    settings.plotRAW = getValTag('cb_plotRAW');       
    settings.dualThread = getValTag('rb_multithreadOn');
    % ------------------------------------------------
    % writing energy ranges to settings
    settings.Emin1 = str2num(getStrTag('edit_Emin1'));
    settings.Emax1 = str2num(getStrTag('edit_Emax1'));
    settings.dE1   = str2num(getStrTag('edit_dE1'));
    settings.Emin2 = str2num(getStrTag('edit_Emin2'));
    settings.Emax2 = str2num(getStrTag('edit_Emax2')); 
    settings.dE2   = str2num(getStrTag('edit_dE2'));

    % ------------------------------------------------
    % read suggest value from Checkbox
    settings.suggestValsOn  = getValTag('cb_SettingsSuggVals');
     % read suggest values from GUI   
    hUITab = findobj('Tag','uiTab_suggestVals');
    suggTabData = get(hUITab,'Data');    
    rowLabels    = get(hUITab,'RowName')    ;               % current RowNames (Atomic Params) in uiTable
    colLabels    = get(hUITab,'ColumnName')'   ;            % current ColumnNames (IonStrings) in the uiTable
    % ------------------------------------------------
    
    % ------------------------------------------------
    handles.appSettings = settings; 
    % ------------------------------------------------
%     size(tabData)
%     size(colLabels)
%     size(rowLabels)
%     handles.appSettings.suggTab = table(suggTabData,'RowNames',rowLabels);                  
    handles.appSettings.suggTab  = suggTabData;
    handles.appSettings.suggRows = rowLabels;                  
    handles.appSettings.suggIons = colLabels;

    % ------------------------------------------------
%     save([handles.appRootDir filesep 'Resources' filesep 'ctm4rixs.cfg'],'settings')     
    saveAppSettingsFile( handles )
    % ------------------------------------------------
    hideSettings()
    % ------------------------------------------------

    
    
    % -----------------------------------------
    guidata(hObject, handles)
    % -----------------------------------------    
      
    


end

