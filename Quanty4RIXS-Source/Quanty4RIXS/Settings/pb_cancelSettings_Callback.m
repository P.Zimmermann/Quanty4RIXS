function [ output_args ] = pb_cancelSettings_Callback(hObject, eventdata, handles)
%PB_SAVESETTINGS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here


    % ------------------------------------------------
    debugMsg( 'calling pb_cancelSettings_Callback()', 2 )
    % ------------------------------------------------
    updateStatus(['Settings NOT saved! '])
    % ------------------------------------------------    
    hideSettings()
    % ------------------------------------------------    

    % ------------------------------------------------
    % restoring values from handles.appSettings
    handles = fillSettingsUI( handles );
    % ------------------------------------------------    

    % ------------------------------------------------    
    guidata(hObject, handles)
    % ------------------------------------------------    
    % ------------------------------------------------    

end

