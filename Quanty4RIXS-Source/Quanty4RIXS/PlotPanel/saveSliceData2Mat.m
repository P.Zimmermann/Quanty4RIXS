function [ output_args ] = saveSliceData2Mat( input_args )


        % ------------------------------------------------------------
        % check or create ./mat/ folder
        matFolder = [thisObj.mapFolder 'mat' filesep];
        if not(exist(matFolder) == 7)            % 7 = folder exists 
            mkdir(matFolder)
        end  
        % ------------------------------------------------------------

        fullSaveName = fullfile(matFolder, [thisObj.baseName '_SliceData.mat' ]);

%             debugMsg( [' saving mapData to file: ' fullSaveName], 1 )
        disp( [' saving sliceData to file: ' fullSaveName] )

        sliceData.xData
        
        sliceData.yData

        save(fullSaveName,'mapData')



end

