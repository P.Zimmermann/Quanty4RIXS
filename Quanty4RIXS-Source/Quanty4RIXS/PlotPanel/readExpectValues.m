function [header, tmpGS, tmpIS, tmpFS] = readExpectValues( fileName )
%READEXPECTVALUES Summary of this function goes here
%   Detailed explanation goes here

    if iscell(fileName)
        fileName = fileName{1};
    end

    if exist(fileName) == 2
     
        % open File for reading
        fileID = fopen(fileName);
         
        % read first line seperately (contains header info)
        line1 = fgets(fileID);                         % read line by line  
%         header = strsplit(deblank(line1))               % remove leading or trailing spaces and split line 
        header = strsplit(line1,{' ','#','\n'},'CollapseDelimiters',true);
        % --------------------------------------------------
        % start reading the 3 data blicks for GS, IS, FS
        valData{1} = '';
        valData{2} = '';
        valData{3} = '';
        blockIndex = 1;        
        while blockIndex > 0       
            thisline = fgets(fileID);                         % read line by line  
            
            if thisline == -1
                % stop loop if empty
                blockIndex = 0;         % Abort condition when line is empty . (This should only happen at the end)
             
            else    
                splitLine = strsplit(deblank(thisline));               % remove leading or trailing spaces and split line     
%                 splitLine = strsplit(deblank(thisline),{' ','#','\n'},'CollapseDelimiters',true)               % remove leading or trailing spaces and split line     

                if length(splitLine) > 0          
                    if strcmp(splitLine{1},'#GS')
                        % GS section starts (thisLines is not stpred)
                        blockIndex = 1;
                    elseif strcmp(splitLine{1},'#IS')    
                        % IS section starts (thisLines is not stpred)
                        blockIndex = 2;                    
                    elseif strcmp(splitLine{1},'#FS')    
                        % FS section starts (thisLines is not stpred)
                        blockIndex = 3;
                    else
                        % append current line to block (GS, IS, FS)
                        valData{blockIndex} = [valData{blockIndex}  thisline];               % collecting the header block lines
                    end
                else
                    blockIndex = 0;         % Abort condition when line is empty . (This should only happen at the end)
                end
                clear thisline splitLine
            end
            
            
        end           
        fclose(fileID);
        % --------------------------------------------------     
     
%         tmpStringDataGS = valData{1}
        
        % --------------------------------------------------    
        dataMapping = '';
        for k=1:length(header)                               
            dataMapping = [dataMapping '%n ' ];
        end
                        
        if length(valData{1}) > 0
            dataGS = textscan(valData{1},...
                                dataMapping,...                             %'%n %n %n %n %n %n %n %n %n %n %n',...
                                'Delimiter',' ',...                         % 'HeaderLines',2,...
                                'MultipleDelimsAsOne',1);
        end
        if length(valData{2}) > 0
            dataIS = textscan(valData{2},...
                                dataMapping,...                             %'%n %n %n %n %n %n %n %n %n %n %n',...
                                'Delimiter',' ',...                         % 'HeaderLines',2,...
                                'MultipleDelimsAsOne',1);
        end
        if length(valData{3}) > 0
            dataFS = textscan(valData{3},...
                                dataMapping,...                             %'%n %n %n %n %n %n %n %n %n %n %n',...
                                'Delimiter',' ',...                         % 'HeaderLines',2,...
                                'MultipleDelimsAsOne',1);
        end
        % --------------------------------------------------      
        
        % --------------------------------------------------
        header = header(~cellfun('isempty',header)) ;                     % remove empty cells  
        dataGS = dataGS(~cellfun('isempty',dataGS)) ;                     % remove empty cells  
        dataIS = dataIS(~cellfun('isempty',dataIS)) ;                     % remove empty cells  
        dataFS = dataFS(~cellfun('isempty',dataFS)) ;                     % remove empty cells  
        
        tmpGS = cell2mat(dataGS);
        tmpIS = cell2mat(dataIS);
        tmpFS = cell2mat(dataFS);
        clear fileID line1 valData dataGS dataIS dataFS   
        % --------------------------------------------------
    else
        % --------------------------------------------------
        header= [];
        tmpGS = [];
        tmpIS = [];
        tmpFS = [];
        % --------------------------------------------------
    end   
    
%     a1 = header
%     a2 = tmpGS(1,:)  
    

end


       
        
            
%         line1 = fgets(fileID)                         % read line by line 
%         line2 = fgets(fileID)                         % read line by line 


%         line1 = strsplit(deblank(line1));               % remove leading or trailing spaces and split line 
%         header = strsplit(deblank(line2))               % remove leading or trailing spaces and split line 
        
        
    
%                 % TODO: see for better/more efficient reading (file is slow) here just for header!!
%                 % GET THE HEADER information 
%                 % <E>  <S^2>  <L^2>  <J^2>  <l.s>  <F[2]> <F[4]>  <Nx2y2>  <Nz2>  <Nxy>  <Nxzyz>                 
%                 fileID = fopen(fName_OPvals);
%                 tmpLines = textscan(fileID, '%s','delimiter', '\n');
%                 tmpLines = tmpLines{:};
%                 header = strsplit(tmpLines{2},{' ','#'},'CollapseDelimiters',true);                % header is the second line
%                 clear tmpLines;
%                 thisObj.valuesLegend = header(~cellfun('isempty',header)) ;                     % remove empty cells  
%                 clear header;
%                 
%                 
        
% 
%         thisline = fgets(fid);                         % read line by line  
%         headBlock = [headBlock  thisline];               % collecting the header block lines
%         
%         thisline = strsplit(deblank(thisline));               % remove leading or trailing spaces and split line     
%         % todo, save head here as needed
%
%         thisline{3}
