function handles = makeSlicesFigure( handles )
%
% NOTE: HERE Multimaps are splitted:  see subfunction makeFigureAndPlots(figNum, dataX,dataY,dataZ, titleStr) 
%

    % -------------------------------------------------------
    debugMsg( 'calling makeSlicesFigure()', 2 )
    % --------------------------------------------
    updateStatus(['Opening SLICES figure...'])
    % --------------------------------------------
%     pause(0.01);    

    % -------------------------------------------------------
    % add other mapSets from handles.mapSet{} cell array
    if length(handles.hBackend.mapData) >= getValTag({'pm_mapSet'})
        % ------------------------------------------------------------------------
        % NOTE: this loads also multimaps as they are. XX and width of ZZ doent match for multimaps!!!
        clear thisMapSet
        
        thisMapSet = handles.hBackend.mapData{getValTag({'pm_mapSet'})};
        % ------------------------------------------------------------------------------------------------------------
        

        % ------------------------------------------------------------------------------------------------------------
        thisFigNum = 1000*getValTag({'pm_mapSet'});       
        % ------------------------------------------------------------------------
        % LCP plot (plot LCP if exists and selected)
        % ------------------------------------------------------------------------
        
        lcpFigNum = thisFigNum+10;
        if (getValTag({'cb_lcpMap'}) == 1)                  
            % ------------------------------------------------------------------------           
            hLCPFig = makeSingleFigureSlices(lcpFigNum,...
                            thisMapSet.energyIn, thisMapSet.energyOut, thisMapSet.lcpMap,...            % getting the SUM map data 
                            'LCP',...
                            thisMapSet.mapFolder, [filesep 'img_' thisMapSet.baseName '-LCP' ]) ;           
            % ------------------------------------------------------------------------                     
        else
            close(figure(lcpFigNum))                  % DELETING figure when deselected
        end
        % ------------------------------------------------------------------------

        % ------------------------------------------------------------------------
        % RCP plot (plot RCP if exists and selected)
        % ------------------------------------------------------------------------
        rcpFigNum = thisFigNum+20;
        if (getValTag({'cb_rcpMap'}) == 1)                  
            % ------------------------------------------------------------------------           
            hRCPFig = makeSingleFigureSlices(rcpFigNum,...
                            thisMapSet.energyIn, thisMapSet.energyOut, thisMapSet.rcpMap,...            % getting the SUM map data 
                            'RCP',...
                            thisMapSet.mapFolder, ['img_' thisMapSet.baseName '-RCP' ]) ;           
            % ------------------------------------------------------------------------  
        else
            close(figure(rcpFigNum))                  % DELETING figure when deselected            
        end
        % ------------------------------------------------------------------------

        % ------------------------------------------------------------------------
        % SUM plot (plot SUM if exists and selected)
        % ------------------------------------------------------------------------
        sumFigNum = thisFigNum+30;
        if (getValTag({'cb_sumMap'}) == 1)                  
            % ------------------------------------------------------------------------           
            hSUMFig = makeSingleFigureSlices(sumFigNum,...
                            thisMapSet.energyIn, thisMapSet.energyOut, thisMapSet.sumMap,...            % getting the SUM map data 
                            'LCP + RCP = SUM',...
                            thisMapSet.mapFolder, ['img_' thisMapSet.baseName '-SUM' ]) ;           
            % ------------------------------------------------------------------------                     
        end
        % ------------------------------------------------------------------------
               
        % ------------------------------------------------------------------------
        % MCD plot (plot MCD if exists and selected)
        mcdFigNum = thisFigNum+40;
        % ------------------------------------------------------------------------
        if (getValTag({'cb_mcdMap'}) == 1)
            % ------------------------------------------------------------------------           
            hMCDFig = makeSingleFigureSlices(mcdFigNum,...
                            thisMapSet.energyIn,  thisMapSet.energyOut,  thisMapSet.mcdMap,...            % getting the MCD mapSet 
                            'LCP - RCP = MCD',...
                            thisMapSet.mapFolder, ['img_' thisMapSet.baseName '-MCD' ]);
            % ------------------------------------------------------------------------   
        end   
        % ------------------------------------------------------------------------
        
        
        % ------------------------------------------------------------------------
        pause(0.01);    
        % ------------------------------------------------------------------------      
        
        % ------------------------------------------------------------------------------------------------------------
        updateStatus(['Opening SLICES figure...Done!'])
        disp('done makeSlicesFigure()')
        % ------------------------------------------------------------------------------------------------------------
        clear thisMapSet;
        % ===============================================================================================================         
    end
      
    % =============================================================================================================== 
    % =============================================================================================================== 

end
 