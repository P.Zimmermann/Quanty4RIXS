function para = readLuaConfigFile( configFileName )


    debugMsg( 'calling readLuaConfigFile()', 2 )

    para = [];

    if exist(configFileName) == 2                   % 2: normal file
       
        % open File for reading
        fidConf = fopen(configFileName);
%         thisline = fgets(fidConf);                         % read line by line  
%         thisline = strsplit(deblank(thisline));               % remove leading or trailing spaces and split line     


        stopFlag = 1;
        while stopFlag       
            thisline = fgets(fidConf);                         % read line by line  
            
%             thisline
%             length(thisline)
            
            
            if (length(thisline) > 1)                                        %  && ( num2str(thisline) > 0 )
                
                if ~strcmp(thisline(1:2),'--')
                    thisline = strsplit(deblank(thisline),'=');               % remove leading or trailing spaces and split line     
                        
                    if strfind(char(thisline(1)),'Emin1')
                        para.Emin1 = str2num(thisline{2});       % extracted Emin1
                    elseif strfind(char(thisline(1)),'Emax1')    
                        para.Emax1 = str2num(thisline{2});       % extracted Emax1
                    elseif strfind(char(thisline(1)),'dE1')    
                        para.dE1 = str2num(thisline{2});       % extracted dE1
                    elseif strfind(char(thisline(1)),'Emin2')    
                        para.Emin2 = str2num(thisline{2});       % extracted Emin2
                    elseif strfind(char(thisline(1)),'Emax2')    
                        para.Emax2 = str2num(thisline{2});       % extracted Emax2    
                    elseif strfind(char(thisline(1)),'dE2')    
                        para.dE2 = str2num(thisline{2});       % extracted dE2
%                     elseif strcmp(thisline(1),'Energy')
%                        headers = thisline;
%                        stopFlag = 0;                          % Abort condition, nest line starts with data
                    end   
                        
                end    
            else
                stopFlag = 0;                               % Abort condition when line is empty . (ToDo Check: This should only happen at the end)
            end            

        end   
        
        fclose(fidConf);                            % close (text) file reader
        
        

    end






end

