function [ output_args ] = makeValuesPanel( hFig, valsGS, valsIS, valsFS, header )
%MAKEVALUESPANEL Summary of this function goes here
%   Detailed explanation goes here


    % -------------------------------------------------------
    debugMsg( 'calling makeValuesPanel()', 2 )

    
%     valsGS
%     
%     valsGS(:,1)
%     
%     size(valsGS)    
    
    % -------------------------------------------------------
    clear posX0 posY0
    %  valsGS(:,1)
    axW = 260;
    axH = 170;
    posX0 = 40;
    posY0 = 500;
    
    maxGS = getValTag('sl_OPvalMax');   % get max GS from GUI ()

    disp(['Plotting expectation values for ' num2str(size(header,2)) ' observables.' ])
    
    % loop through number of oberservables
%     for k=1:size(valsGS,2)
    for k=1:size(header,2)

        legStr = {[] [] []};

        col = k;
        row = 1;
        if (4 < k) && (k <= 8)
            col = col - 4;          % counting columns number for axis
            row = 2;
        elseif (8 < k) && (k <= 12)
            col = col - 8;          % counting columns number for axis
            row = 3;            
        end    
        posXX = posX0 + (col-1)*(axW+45);
        posYY = posY0 - (row-1)*(axH+60);

        % add data to the k-th axes/plot
        hax{k} = axes('Parent',hFig,'Units','pixels',...
                    'Position',[posXX, posYY, axW, axH],...
                    'Tag','axVals',...
                    'Box','on',...
                    'HandleVisibility','On');   

        grid on
        hold on
        % put GS,IS,FS in one plot (indivisula check if files exist)
        
%         header
%          szHead = size(header)
%         valsGS(1,:)
%          szAll = size(valsGS)
%          k
%          sz_col = size(valsGS(1:maxGS,k))
        
        if k <= size(valsGS,2) 
            hPlt = stairs( [valsGS(1:maxGS,k), valsIS(1:maxGS,k), valsFS(1:maxGS,k)] );
%             hPlt = stairs( [1:k]-0.5 , [valsGS(1:maxGS,k), valsIS(1:maxGS,k), valsFS(1:maxGS,k)] );
            legStr = {'GS','IS','FS'};
            title(header{k})
        end

        legStr = legStr(~cellfun('isempty',legStr));                      % remove empty cells  
        legend(legStr,'FontSize',9)
   
    end
    
    % ------------------------------------------------------------------------------------------------------------
    pause(0.001)                        % NOTE:  1ms delay to allow updating the lots (avoids grey panels)    
    % ------------------------------------------------------------------------------------------------------------
 
        
   
    
    set(findobj('Tag','axVals'),'Units','normalized')
    
    
    
%     for k=1:size(valsGS,2)
%         hold on
%         plot(valsGS(:,k))
%         title(header{k})
%     end 
%     for k=1:size(selMapSet.valuesIS,2)
%         figure(10000+k)
%         hold on
%         plot(selMapSet.valuesIS(:,k))
%         title(selMapSet.valuesLegend{k})
%     end 
%     for k=1:size(selMapSet.valuesFS,2)
%         figure(10000+k)
%         hold on
%         plot(selMapSet.valuesFS(:,k))
%         title(selMapSet.valuesLegend{k})
%     end                 
    %                 legend(selMapSet.valuesLegend)


    output_args = hax;              % return all axis handles

end

