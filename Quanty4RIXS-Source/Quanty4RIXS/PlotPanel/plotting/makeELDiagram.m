function [ output_args ] = makeELDiagram( hFig, posX0, posY0, header, cfVals, diagramVals )
%MAKEVALUESPANEL Summary of this function goes here
%   Detailed explanation goes here


    % -------------------------------------------------------
    debugMsg( 'calling makeELDiagram()', 2 )

    
% %     diagramVals
% %     diagramVals(:,1)            % first column
% %     diagramVals(1,:)            % first row
%     numOfVals = size(diagramVals(:,1),1)    
%     numOfColumns  = size(diagramVals(1,:),2)    
% %     sz_all    = size(diagramVals)    
    
    % -------------------------------------------------------
%     clear posX0 posY0
    %  valsGS(:,1)
    axW = 380;
%     axH = 620;
    axH = 320;

    
    maxStates = getValTag('sl_ELDvalMax');   % get max GS from GUI ()
    absVals   = getValTag('cb_ELDabsVals');   % get max GS from GUI ()

    
    hax = axes('Parent',hFig,'Units','pixels',...
                    'Position',[posX0, posY0, axW, axH],...
                    'Tag','axVals',...
                    'FontSize',12,...
                    'Box','on',...
                    'HandleVisibility','On');       
%     cla;
        
%     x_Vals  = diagramVals(1,:);
%     y1_Vals = diagramVals(2,:);
%          
%     x_Vals(1:10)
%     y1_Vals(1:10)
%     

    % check and adjust number of maxStates (GUI has the highest value of all found data sets.)
    numOfStates  = size(diagramVals(1,:),2);
    disp(['Number of States: ' num2str(numOfStates-1) ])   
    if maxStates > numOfStates-1 
        maxStates = numOfStates-1;
    end
    
    for thisCol = 2:maxStates+1                    % first column is X value (Dq, Ds, ect) 
        hold on
        grid on
        xVals = diagramVals(:,1);
        lowestGSvals = diagramVals(:,2);

        if absVals == 1
           yVals = diagramVals(:,thisCol);
        else    
            yVals = diagramVals(:,thisCol) - lowestGSvals;
        end
        
%         cm = colormap(parula(2+maxStates));
        cm = colormap(jet(2+maxStates));
%         cm = colormap(cool(2+maxStates));
%         cm = colormap(hsv(2+maxStates));
%         cm = colormap(winter((2+maxStates)));       
        myColor = cm( thisCol , :);  
        
        plot(xVals,yVals,'Color',myColor)
        
        legendStrgs{thisCol-1} = header{thisCol} ;
     
        
    end
    legend(legendStrgs,'Location','eastoutside')
    
    title( { [ header{1} ' = ' num2str(min(xVals)) ' ... ' num2str(max(xVals)) ' eV ' ] ,...
             [ ' (Dq=' num2str(cfVals(1)) ',Ds=' num2str(cfVals(2)) ',Dt=' num2str(cfVals(3)) ',M=' num2str(cfVals(4)) ')' ] }, 'FontSize',14)
    xlabel([header{1} ' in eV'])

    if absVals == 1
        ylabel(['E in eV'])    
    else    
        ylabel(['E - E(psi1) in eV'])
    end
%     xlim([0 min(1, max(xVals))])
%     ylim([-0.01*max(yVals) 1.01*max(yVals) ])
%     ylim([-0.01 20 ])
  
    
%     for k=1:size(valsGS,2)
% 
%         legStr = {[] [] []};
% 
%         col = k;
%         row = 1;
%         if (3 < k) && (k <= 7)
%             col = col - 3;          % counting columns number for axis
%             row = 2;
%         elseif (7 < k) && (k <= 12)
%             col = col - 7;          % counting columns number for axis
%             row = 3;            
%         end    
%         posXX = posX0 + (col-1)*(axW+45);
%         posYY = posY0 - (row-1)*(axH+60);
% 
%         hax{k} = axes('Parent',hFig,'Units','pixels',...
%                     'Position',[posXX, posYY, axW, axH],...
%                     'Tag','axVals',...
%                     'Box','on',...
%                     'HandleVisibility','On');   
% 
%         grid on
%         hold on
%         % put GS,IS,FS in one plot (indivisula check if files exist)
%         if k <= size(valsGS,2) 
%             hPlt = stairs( [valsGS(1:maxGS,k), valsIS(1:maxGS,k), valsFS(1:maxGS,k)] );
%             legStr = {'GS','IS','FS'};
%         end
% 
%         title(header{k})
%         legStr = legStr(~cellfun('isempty',legStr));                      % remove empty cells  
%         legend(legStr,'FontSize',9)
%    
%     end
%     
%     % ------------------------------------------------------------------------------------------------------------
%     pause(0.001)                        % NOTE:  1ms delay to allow updating the lots (avoids grey panels)    
%     % ------------------------------------------------------------------------------------------------------------
 
        
   
    
    set(findobj('Tag','axVals'),'Units','normalized')
    
    
    
%     for k=1:size(valsGS,2)
%         hold on
%         plot(valsGS(:,k))
%         title(header{k})
%     end 
%     for k=1:size(selMapSet.valuesIS,2)
%         figure(10000+k)
%         hold on
%         plot(selMapSet.valuesIS(:,k))
%         title(selMapSet.valuesLegend{k})
%     end 
%     for k=1:size(selMapSet.valuesFS,2)
%         figure(10000+k)
%         hold on
%         plot(selMapSet.valuesFS(:,k))
%         title(selMapSet.valuesLegend{k})
%     end                 
    %                 legend(selMapSet.valuesLegend)


    output_args = hax;              % return all axis handles

end

