function updateValuesGSISFS( handles, selMapSet )
%UPDATEVALUESGSISFS Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------------
    debugMsg( 'calling updateValuesGSISFS()', 2 )
    % -------------------------------------------------------


    pltFigWidth = 1240;
    pltFigHeight = 720;

    if length(handles.hBackend.mapData) >= getValTag({'pm_mapSet'})
   
        thisMapSet = handles.hBackend.mapData{getValTag({'pm_mapSet'})};

        thisFigNum = 50+100*getValTag('pm_mapSet');
        
        % make figure and ploits with data in  dataX, dataY, dataZ
        hViewFig = figure(thisFigNum);
        clf

        figPos = get(hViewFig,'Position');                
        set(hViewFig,'Position',[figPos(1) figPos(2) pltFigWidth pltFigHeight])            
        set(hViewFig,...
            'Menubar','none',...
            'Tag','uiFig_ExpectationValues',...
            'CloseRequestFcn',@myDeleteFig);
                        
        hAllAx = makeValuesPanel( hViewFig, selMapSet.valuesGS, selMapSet.valuesIS, selMapSet.valuesFS, selMapSet.valuesLegend );

        % --------------------------------------------------------------
        % save tha OPval data to .mat File 
        if getValTag('cb_saveData') == 1
            thisMapSet.saveOPvalData();
        end   
        % --------------------------------------------------------------
        % save figure to file im save is on
        if getValTag('cb_savePlots') == 1 
            % ---------------------------------------------
            saveImgName = ['img_' thisMapSet.baseName '-Observables' ];
            % ---------------------------------------------
            updateStatus(['Saving Plot: ' saveImgName '.png' ])   
            % ---------------------------------------------
            saveFigure2Image(hViewFig, thisMapSet.mapFolder, saveImgName)
            % ---------------------------------------------
        end
        % -------------------------------------------------------------- 
        clear selMS thisFigNum hViewFig figPos  saveImgName            % clean up

    end
        
        
    % ------------------------------------------
    function myDeleteFig(arg1,arg2)
%         arg1
%         arg2        
        delete(gcf)
    end        
    % ------------------------------------------


end

