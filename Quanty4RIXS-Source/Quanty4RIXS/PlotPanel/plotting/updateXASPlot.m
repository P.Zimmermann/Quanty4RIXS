function updateXASPlot( handles, selMapSet )
%UPDATEVALUESGSISFS Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------------
    debugMsg( 'calling updateXASPlot()', 2 )
    % -------------------------------------------------------

    pltFigWidth = 1900;
    pltFigHeight = 720;

    if length(handles.hBackend.mapData) >= getValTag({'pm_mapSet'})
   
        thisMapSet = handles.hBackend.mapData{getValTag({'pm_mapSet'})};

        thisFigNum = 73+1000*getValTag('pm_mapSet');
        
%         % make figure and ploits with data in  dataX, dataY, dataZ
%         hViewFig = figure(thisFigNum);
%         clf
% 
%         figPos = get(hViewFig,'Position');                
%         set(hViewFig,'Position',[figPos(1) figPos(2) pltFigWidth pltFigHeight])            
%         set(hViewFig,...
%             'Menubar','none',...
%             'Tag','uiFig_ELDiagram',...
%             'CloseRequestFcn',@myDeleteFig);
%         % --------------------------------------------------------------
%         ax_x0 = 60;
%         ax_y0 = 50;
%         ax_dx = 450;        
%         
% %         datlen = length(thisMapSet.eld_valData)
        
        % make all ELDiagrams
%         for m=1:length(thisMapSet.lcpXAS)
% %             hAx_Ds = makeXASplot( thisFigNum, ax_x0+(m-1)*ax_dx, ax_y0, thisMapSet.lcpEin, thisMapSet.lcpXAS, thisMapSet.rcpEin, thisMapSet.rcpXAS );
%         end
        
        thisPara = thisMapSet.para
        
        % build Bx string
        if isfield(thisPara,'Bx') strBx = num2str(thisPara.Bx); else strBx = '?'; end
        if isfield(thisPara,'By') strBy = num2str(thisPara.By); else strBy = '?'; end
        if isfield(thisPara,'Bz') strBz = num2str(thisPara.Bz); else strBz = '?'; end
        
        
        if (length(thisMapSet.lcpXAS) > 0) && length(thisMapSet.rcpXAS) > 0
            hViewFig = plotStacksAndSum(thisFigNum, thisMapSet.lcpEin, thisMapSet.lcpXAS, thisMapSet.rcpEin, thisMapSet.rcpXAS,...
                            {['k|| (' strBx strBy strBz ')'] , [' Dq=' num2str(thisPara.Dq_GS) ',Ds=' num2str(thisPara.Ds_GS) ',Dt=' num2str(thisPara.Dt_GS) ',SOC=' num2str(thisPara.SOCv_GS) 'eV']}, size(thisMapSet.lcpXAS,2) );         
        end
        
%         % --------------------------------------------------------------
%         % save tha ELDiagram data to .mat File 
%         if getValTag('cb_saveData') == 1
%             thisMapSet.saveELDData();
%         end    
%         % --------------------------------------------------------------
        
        
        % --------------------------------------------------------------
        % save figure to file im save is on
        if getValTag('cb_savePlots') == 1 
            % ---------------------------------------------
            saveImgName = ['img_' thisMapSet.baseName '-XAS1s' ];
            % ---------------------------------------------
            updateStatus(['Saving Plot: ' saveImgName '.png' ])   
            % ---------------------------------------------
%             saveFigure2Image(hViewFig, [thisMapSet.mapFolder saveImgName])
            saveFigure2Image(hViewFig, thisMapSet.mapFolder, saveImgName)
            % ---------------------------------------------
        end
        % -------------------------------------------------------------- 
        clear selMS thisFigNum hViewFig figPos  saveImgName            % clean up

    end
        
        
    % ------------------------------------------
    function myDeleteFig(arg1,arg2)
%         arg1
%         arg2        
        delete(gcf)
    end        
    % ------------------------------------------


end

