% function plotMap_CMD(xx,yy,zz,imgFullFilePath,titleStr)
function plotMap(xx,yy,zz,titleStr, horizontalLimits, veticalLimits)


    % -------------------------------------------------------
    debugMsg( 'calling plotMap()', 2 )
    % -------------------------------------------------------

    
        % --------------------------------------------------------
%         linColors = linspace(min(min(zz)),max(max(zz)),30);
%         logColors = logspace(min(min(zz)),max(max(zz)),50);
        % --------------------------------------------------------        
        
%         szXX = length(xx)
%         szYY = length(yy)
%         szZZ = size(zz)
        
        
        % --------------------------------------------------------        
%         contourf(xx,yy,zz,linColors,'LineStyle','None')
%         contourf(xx,yy,zz,log(logColors),'LineStyle','None')
        % --------------------------------------------------------   
        % testing variants
%          mesh(xx,yy,zz)

        % --------------------------------------------------------   
        % read plot mode from settings
        if getValTag('cb_plotRAW') == 1
            imagesc('XData',xx,'YData',yy,'CData',zz)
        else
            contourf(xx,yy,zz,80,'LineStyle','None')
        end
        colorbar('westoutside');   

        % --------------------------------------------------------        
        title(titleStr,'FontSize',14,'Interpreter','none')
        % --------------------------------------------------------
        set(gca,'FontSize',12)
        set(gca,'XAxisLocation','top')       % is due to rotation right
        % --------------------------------------------------------


        % --- set Xlim --------------------------------
        if length(horizontalLimits) == 2
            xlim(horizontalLimits)
        else
            xlim([min(min(xx)) max(max(xx))])
        end  
        % --- set Ylim -------------------------------
        if length(veticalLimits) == 2
             ylim(veticalLimits)            % ka1 and ka2
        else
             ylim([min(min(yy)) max(max(yy))])
        end  
        % --------------------------------------------------------

        clear xx yy zz


end

% --- Below only OLD CODE ----

%          colorbar('north');   
%         colorbar('YTick',log(colors),'YTickLabel',colors);
%         caxis(log([linColors(1) linColors(length(linColors))]));
%         colorbar('FontSize',12,'YTick',log(colors),'YTickLabel',colors);
%         hColBar = colorbar('FontSize',12,'YTick',linColors,'YTickLabel',linColors);
% 

%         set(gca,'YAxisLocation','right')     % is due to rotation top
%         set(gca,'yticklabel',[])
%         ylabel('FS energy (eV)');


%         xlabel('Incident energy (eV)');
%         set(gca,'xtick',[])
%         set(gca,'xticklabel',[])

%         daspect([1 1 1])