function plotProjV(mapXX,map_ZZ, veticalLimits)


    map_ZZ(isnan(map_ZZ)) = 0;            % replace NaN with zeros


    projV_X = mapXX(:,1);
%     projV_X = mapXX(1,:);
    projV_Y = mean(map_ZZ,2);              % sum(A,2) sum of elements in a row 
%     projV_Y = sum(map_ZZ,2);              % sum(A,2) sum of elements in a row 
    projV_Y(isnan(projV_Y)) = 0;            % replace NaN with zeros
   
    
    % ---------------------------------------------
    % in RAW mode make stem plot
    if getValTag('cb_plotRAW') == 1
        stem(projV_X,projV_Y,'Marker','.')
    else
        plot(projV_X,projV_Y)
    end        
    % ---------------------------------------------
    view([-90 90])
    set(gca,'XAxisLocation','top')       % is due to rotation right
    set(gca,'YAxisLocation','right')     % is due to rotation top


    % --------------------------------------------------------        
    xlabel('FS energy (eV)');
    ylabel('Intensity');
%     daspect([1 1 1])
%     xlim([-6.5 6.5])
%     xlim([-10 12])            % NOTE: rotated by 90 degree

    if min(min(projV_X)) < max(max(projV_X))
        xmin = min(min(projV_X));
        xmax = max(max(projV_X));
    else
       xmin = -1; 
       xmax = +1;
    end
    
    if min(min(projV_Y)) < max(max(projV_Y))
        ymin = min(min(projV_Y));
        ymax =  max(max(projV_Y));
    else
        ymin = -1;
        ymax = +1; 
    end
    
    if length(veticalLimits) == 2
         xlim(veticalLimits)            % ka1 and ka2
    else
         xlim([xmin xmax])
    end
    ylim([ymin ymax])   
    % --------------------------------------------------------

%         title(titleStr,'FontSize',13)
%                
%         % --------------------------------------------------------

    set(gca,'FontSize',12)
    % ------------------------------------------
    clear mapXX map_ZZ
    % ------------------------------------------
end

