function updateELDiagram( handles, selMapSet )
%UPDATEVALUESGSISFS Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------------
    debugMsg( 'calling updateELDiagram()', 2 )
    % -------------------------------------------------------



    if length(handles.hBackend.mapData) >= getValTag({'pm_mapSet'})
   
        thisMapSet = handles.hBackend.mapData{getValTag({'pm_mapSet'})};
        % --------------------------------------------------------------
        pltFigWidth = 100 + 400 * length(thisMapSet.eld_valData);
    %     pltFigHeight = 720;
        pltFigHeight = 420;
        % --------------------------------------------------------------
        thisFigNum = 70+100*getValTag('pm_mapSet');
        
        % make figure and ploits with data in  dataX, dataY, dataZ
        hViewFig = figure(thisFigNum);
        clf

        figPos = get(hViewFig,'Position');                
        set(hViewFig,'Position',[figPos(1) figPos(2) pltFigWidth pltFigHeight])            
        set(hViewFig,...
            'Menubar','none',...
            'Tag','uiFig_ELDiagram',...
            'CloseRequestFcn',@myDeleteFig);
        % --------------------------------------------------------------
        ax_x0 = 60;
        ax_y0 = 50;
        ax_dx = 450;        
        
%         datlen = length(thisMapSet.eld_valData)
        
        % make all ELDiagrams
        for m=1:length(thisMapSet.eld_valData)
            hAx_Ds = makeELDiagram( hViewFig, ax_x0+(m-1)*ax_dx, ax_y0, thisMapSet.eld_headers{m}, thisMapSet.eld_cfValues{m}, thisMapSet.eld_valData{m} );
        end
        
        % --------------------------------------------------------------
        % save tha ELDiagram data to .mat File 
        if getValTag('cb_saveData') == 1
            thisMapSet.saveELDData();
        end    
        % --------------------------------------------------------------
        % save figure to file im save is on
        if getValTag('cb_savePlots') == 1 
            % ---------------------------------------------
            saveImgName = ['img_' thisMapSet.baseName '-ELDiagram' ];
            % ---------------------------------------------
            updateStatus(['Saving Plot: ' saveImgName '.png' ])   
            % ---------------------------------------------
%             saveFigure2Image(hViewFig, [thisMapSet.mapFolder saveImgName])
            saveFigure2Image(hViewFig, thisMapSet.mapFolder, saveImgName)
            % ---------------------------------------------
        end
        % -------------------------------------------------------------- 
        clear selMS thisFigNum hViewFig figPos  saveImgName            % clean up

    end
        
        
    % ------------------------------------------
    function myDeleteFig(arg1,arg2)
%         arg1
%         arg2        
        delete(gcf)
    end        
    % ------------------------------------------


end

