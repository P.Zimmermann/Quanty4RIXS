function hSummaryFig = makeSummaryFigure( handles, thisMapSet )
%UPDATESUMMARYFIGURE Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------------
    debugMsg( 'calling updateSummaryFigure()', 2 )
    % -------------------------------------------------------
    sumFigWidth = 1425;     %1325;
    sumFigHeight = 980;

    dataX = thisMapSet.energyIn;
    dataY = thisMapSet.energyOut;
    
    dataLCP = thisMapSet.lcpMap;            % getting the SUM mapSet 
    dataRCP = thisMapSet.rcpMap;            % getting the SUM mapSet 
    dataSUM = thisMapSet.sumMap;            % getting the SUM mapSet 
    dataMCD = thisMapSet.mcdMap;            % getting the MCD mapSet 
    
    % --------------------------------------------------------------
    % NOTE: Here a check is needed for Multimaps. XX and width of ZZ doent match for multimaps!!!
    if getValTag('cb_splitMM') == 1            
        subMapNo = getValTag('sl_selMultimap');
        xasRange = size(dataX,2);                       % get range of energies in Ein
        indMin = 1 + (subMapNo-1)*xasRange;             % get index Min for selected range
        indMax = xasRange + (subMapNo-1)*xasRange;      % get index Max for selected range
        % ------------------------------------
        dataLCP = dataLCP(:,indMin:indMax);             % split Multimap LCP   
        dataRCP = dataRCP(:,indMin:indMax);             % split Multimap RCP   
        dataSUM = dataSUM(:,indMin:indMax);             % split Multimap SUM   
        dataMCD = dataMCD(:,indMin:indMax);             % split Multimap MCD
        % ------------------------------------
    elseif getValTag('cb_joinMaps') == 1   
        
        subMapNo = getValTag('sl_selMultimap');
        xasRange = size(dataX,2);                       % get range of energies in Ein
       
        newDataLCP = zeros(size(dataLCP,1),xasRange);       % init empty map with Zeros for the loop
        newDataRCP = newDataLCP;
        newDataSUM = newDataLCP;
        newDataMCD = newDataLCP;
        
        for x=1:subMapNo
             % ------------------------------------
            indMin = 1 + (x-1)*xasRange;             % get index Min for selected range
            indMax = xasRange + (x-1)*xasRange;      % get index Max for selected range
            newDataLCP = newDataLCP + dataLCP(:,indMin:indMax);                  % split and add submap   
            newDataRCP = newDataRCP + dataRCP(:,indMin:indMax);                  % split and add submap    
            newDataSUM = newDataSUM + dataSUM(:,indMin:indMax);                  % split and add submap    
            newDataMCD = newDataMCD + dataMCD(:,indMin:indMax);                  % split and add submap    
             % ------------------------------------
        end
        clear x
        dataLCP = newDataLCP;
        dataRCP = newDataRCP;
        dataSUM = newDataSUM;
        dataMCD = newDataMCD;
        % ------------------------------------  
    end
    % --------------------------------------------------------------
    % compare XASrange and mapData width (needed for Multimaps, will be used if splitting of not selected other disagreements)
    % otherwise:  width of dataX and dataZ is the same, dataX from mapset is used
    if ~(size(dataX,2) == size(dataLCP,2))             % compare widths of both maps
        % generate dataX range for multimap here.
        dataX = [1:size(dataLCP,2)];
        
    end
    % --------------------------------------------------------------
    
    % -----------------------------------------------------------------------------------------------------------------
    % -----------------------------------------------------------------------------------------------------------------
    %  Create figure for Plots and Infos
    sumFigNum = 1000*getValTag({'pm_mapSet'});
    hSummaryFig = figure(sumFigNum);
%     clf
    set(hSummaryFig,'Units','pixel', 'MenuBar', 'None');
    figPos = get(hSummaryFig,'Position');
    set(hSummaryFig,'Position',[figPos(1) figPos(2) sumFigWidth sumFigHeight]);
    % Assign the GUI a name to appear in the window title.

    sumPosX0 = 10;  % X0 of the sum map
    sumPosY0 = 7; % Y0 of the sum map
    lcpPosX0 = 10;  % X0 of the sum map
    lcpPosY0 = 492; % Y0 of the sum map
    bigGap = 455;   % large gap between sum and diff
    % ========================================================================================================================
    % LCP plots: MAP, V proj, H, proj
    hPanLCP = makePanelMapAndProjections(hSummaryFig, lcpPosX0, lcpPosY0, dataX,dataY,dataLCP,['LCP'] );
    % ========================================================================================================================
    % RCP plots: MAP, V proj, H, proj
    hPanRCP = makePanelMapAndProjections(hSummaryFig, lcpPosX0+bigGap, lcpPosY0, dataX, dataY,dataRCP,['RCP'] );
    % ========================================================================================================================
    % SUM plots: MAP, V proj, H, proj
    hPanSUM = makePanelMapAndProjections(hSummaryFig, sumPosX0, sumPosY0,dataX,dataY,dataSUM, ['SUM']);
    % ========================================================================================================================
    % MCD plots: MAP, V proj, H, proj
    hPanMCD = makePanelMapAndProjections(hSummaryFig, sumPosX0+bigGap, sumPosY0, dataX,dataY,dataMCD,['MCD']);       
    % ========================================================================================================================
    % Add Parameter Panel
    if (length(thisMapSet.para) > 0)
        makePanelConfigAndParams(hSummaryFig, thisMapSet );
    end
    % ========================================================================================================================
 
    % --------------------------------------------------------------
    % normalised figure and all children (to allow scaling of the window )
    normaliseAll(hSummaryFig);
    % --------------------------------------------------------------

    % ========================================================================================================================

    clear dataX dataY dataLCP dataRCP dataSUM dataMCD

end

