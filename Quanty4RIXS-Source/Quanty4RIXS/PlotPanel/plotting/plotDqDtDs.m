function [ output_args ] = plotDqDtDs( Dq, Ds, Dt )
%PLOTDQDTDS Summary of this function goes here
%   Detailed explanation goes here

    
    % ----------------------------------------
%     axPos = get(gca,'Position');
%     axX0     = axPos(1);
%     axY0     = axPos(2);
%     axWidth  = axPos(3);
%     axHeight = axPos(4);

    % ----------------------------------------
    defPlotLim_Max = +1;
    defPlotLim_Min = -1;
    % ----------------------------------------
    % Franks book p 120
    x2y2 = 6*Dq + 2*Ds - Dt;
    z2   = 6*Dq - 2*Ds - 6*Dt;
    xy   = -4*Dq + 2*Ds - Dt;
    xzyz = -4*Dq - Ds + 4*Dt;
    % ----------------------------------------
    % Setting the plot limits (increase if needed)
    diffY = 0.65; % space between line and axes limit
    xlim([0,1])
    yMax = max([-4*Dq 6*Dq x2y2 z2 xy xzyz]);
    yMin = min([-4*Dq 6*Dq x2y2 z2 xy xzyz]);
    if yMin >= defPlotLim_Min+diffY && yMax <= defPlotLim_Max-diffY
        ylim([defPlotLim_Min,defPlotLim_Max])
    elseif yMin >= defPlotLim_Min+diffY
        ylim([defPlotLim_Min,yMax+diffY])
    elseif yMax <= defPlotLim_Max-diffY
        ylim([yMin-diffY,defPlotLim_Max])     
    else
        ylim([yMin-diffY,yMax+diffY])
    end    
    % ------------------------------------------------------
    % axes('Tag','TSaxes')
    % ------------------------------------------------------
    makeLabeledLine(0.0,0.02,0.0,0.1,{'3d',' '},'black')
    
    makeConnectionLines(0.1,0.0,6*Dq,-4*Dq)
    
    makeLabeledLine(0.16,0.02,6*Dq,0.24,{'eg', num2str(6*Dq,'%1.3f')},[0.9 0.0 0.6])
    makeLabeledLine(0.16,0.12,-4*Dq,0.24,{'t2g', num2str(-4*Dq,'%1.3f')},[0.4 0.0 0.9])
    
    makeConnectionLines(0.4,6*Dq,x2y2,z2)
    makeConnectionLines(0.4,-4*Dq,xy,xzyz)
   
    makeLabeledLine(0.46,0.01,x2y2,0.55,{'x^2-y^2', num2str(x2y2,'%1.3f')},'red')
    makeLabeledLine(0.46,0.15,z2  ,0.55,{'z2', num2str(z2,'%1.3f')},[0.2 0.8 0.2])
    makeLabeledLine(0.46,0.27,xy  ,0.55,{'xy', num2str(xy,'%1.3f')},'blue')
    makeLabeledLine(0.46,0.41,xzyz,0.55,{'xzyz', num2str(xzyz,'%1.3f')},'black')
       
    
    box('on')
    
    % ------------------------------------------------------    
%   xlabel('Incident energy (eV)');
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])    
    set(gca,'ytick',[])
    set(gca,'yticklabel',[])  
    % ------------------------------------------------------
    function makeLabeledLine(xIn,x0Txt,yIn,yLineLen,str,col)
        line([xIn xIn+yLineLen],[yIn yIn],'Color',col)
        text(xIn+x0Txt,yIn+0.02,str,'Color',col,'fontunits','normalized')     
    end

    function makeConnectionLines(xIn1,xIn2,yIn1,yIn2)
        line([xIn1 xIn1+0.06],[xIn2 yIn1],'Color','black','LineStyle',':')
        line([xIn1 xIn1+0.06],[xIn2 yIn2],'Color','black','LineStyle',':')
    end
    % ------------------------------------------------------



end

