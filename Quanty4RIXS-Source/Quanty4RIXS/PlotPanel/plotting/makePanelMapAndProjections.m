function hPan = makePanelMapAndProjections(hFig, posPanelX0, posPanelY0, map_XX,map_YY,map_ZZ, titleStr)


    % -------------------------------------------------------
    debugMsg( 'calling pb_startCalculation_Callback()', 2 )
    % -------------------------------------------------------
%     map_XX
    figure(hFig)                    % set current figure 
    % -------------------------------------------------------
    % plotranges/limits
    if size(map_XX,2) == size(map_ZZ,2)                     % check width of EnergyIn and map
       horizontalLimits = [min(map_XX) max(map_XX)];         % set XAS limits from EnergyIn
    else   
       horizontalLimits = [1 size(map_ZZ,2)] ;               % set XAS limits based on data width
    end
    veticalLimits    = [min(map_YY) max(map_YY)];
    % -------------------------------------------------------
    debugMsg( ['!!! PlotLimits set to:  horLim = ' num2str(horizontalLimits) '   and    vertLim = '  num2str(veticalLimits) '  in pb_startCalculation_Callback()'], 1 )
    % -------------------------------------------------------
    % -------------------------------------------------------
    hPan = uipanel('Parent',hFig,...
          'Title',titleStr,...
          'FontSize',12,...
          'Units','Pixel',...
          'Position',[posPanelX0 posPanelY0 450 480],...
          'BackgroundColor','white');
    % -------------------------------------------------------
    posX0 = 20;  % X0 of the sum map
    posY0 = 40; % Y0 of the sum map
    mapW = 250;      %360;     % Map Width
    mapH = 250;      %450;     % Map Height
    projH = 100;    % Height of all projection plots
    gap = 20;       % small gap between map and projection
    % -------------------------------------------------------
    
    % RIXS 2D plots: MAP
    hax3D = axes('Parent',hPan,'Units','pixels','Position',[posX0, posY0+projH+gap+5, mapW, mapH],'Tag','ax3Dmap','HandleVisibility','On');          %,'Tag',[titleStr '3D']
    plotMap(map_XX,map_YY,map_ZZ, titleStr, horizontalLimits, veticalLimits );
    posMap = get(hax3D,'Position');  

    % -----------------------------------------
    % V proj, H, proj
    haxProjV = axes('Parent',hPan,'Units','pixels','Position',[posMap(1)+posMap(3)+gap, posMap(2)+5, projH, posMap(4)],'Tag','axProjV');
    plotProjV(map_YY,map_ZZ, veticalLimits)
%     set(refline([0 0]),'Color','k')     % reference line with mx+c where m=c=0 
    
    haxProjH = axes('Parent',hPan,'Units','pixels','Position',[posMap(1)+9, posMap(2)-projH-gap+5, 0.945*posMap(3), projH],'Tag','axProjH');
    plotProjH(map_XX,map_ZZ, horizontalLimits)
    
    % -----------------------------------------    
    pause(0.001)                        % NOTE:  1ms delay to allow updating the lots (avoids grey panels)    
    % -----------------------------------------    


    clear  map_XX map_YY map_ZZ


end

