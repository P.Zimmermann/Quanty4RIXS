function hPan = makePanelSlices(hFig, posPanelX0, posPanelY0, map_XX,map_YY,map_ZZ, titleStr, mapFolder)


    % -------------------------------------------------------
    debugMsg( 'calling makePanelSlices()', 2 )
    % -------------------------------------------------------
%     map_XX
    figure(hFig)                    % set current figure 
    % -------------------------------------------------------
    % plotranges/limits
    if size(map_XX,2) == size(map_ZZ,2)                     % check width of EnergyIn and map
       horizontalLimits = [min(map_XX) max(map_XX)];         % set XAS limits from EnergyIn
    else   
       horizontalLimits = [1 size(map_ZZ,2)] ;               % set XAS limits based on data width
    end
    veticalLimits    = [min(map_YY) max(map_YY)];
    % -------------------------------------------------------
    debugMsg( ['!!! PlotLimits set to:  horLim = ' num2str(horizontalLimits) '   and    vertLim = '  num2str(veticalLimits) '  in pb_startCalculation_Callback()'], 1 )
    % -------------------------------------------------------
    % -------------------------------------------------------
    hPan = uipanel('Parent',hFig,...
          'Title',titleStr,...
          'FontSize',12,...
          'Units','Pixel',...
          'Position',[posPanelX0 posPanelY0 650 480],...
          'BackgroundColor','white');
    % -------------------------------------------------------
    posX0 = 20;  % X0 of the sum map
    posY0 = 80; % Y0 of the sum map
    mapW = 280;      %360;     % Map Width
    mapH = 320;      %450;     % Map Height
    sliceH = 125;    % Height of all projection plots
    gap = 20;       % small gap between map and projection
    % -------------------------------------------------------
    
    % RIXS 2D plots: MAP
    hax3D = axes('Parent',hPan,'Units','pixels','Position',[posX0, posY0, mapW, mapH],'Tag','ax3Dmap','HandleVisibility','On');          %,'Tag',[titleStr '3D']
    set(hax3D,'UserData',{map_XX,map_YY,map_ZZ})            % Store Plot data in UserData for easy cut of slices
    plotMap(map_XX,map_YY,map_ZZ, titleStr, horizontalLimits, veticalLimits );
    posMap = get(hax3D,'Position');  


    haxCET = axes('Parent',hPan,'Units','pixels','Box','on',...
                    'Position',[posX0+mapW+50, posY0+gap, mapW, sliceH],...
                    'Tag','axCET','HandleVisibility','On');          %,'Tag',[titleStr '3D']
    xlim([min(map_YY) max(map_YY)])
    title('CET slice')    
    
    haxCIE = axes('Parent',hPan,'Units','pixels','Box','on',...
                    'Position',[posX0+mapW+50, posY0+sliceH+3*gap, mapW, sliceH],...
                    'Tag','axCIE','HandleVisibility','On');          %,'Tag',[titleStr '3D']
    xlim([min(map_XX) max(map_XX)])
    title('CIE slice')

    
    % -----------------------------------------
%     % V proj, H, proj
%     haxProjV = axes('Parent',hPan,'Units','pixels','Position',[posMap(1)+posMap(3)+gap, posMap(2)+5, projH, posMap(4)],'Tag','axProjV');
%     plotProjV(map_YY,map_ZZ, veticalLimits)
% %     set(refline([0 0]),'Color','k')     % reference line with mx+c where m=c=0 
%     
%     haxProjH = axes('Parent',hPan,'Units','pixels','Position',[posMap(1)+9, posMap(2)-projH-gap+5, 0.945*posMap(3), projH],'Tag','axProjH');
%     plotProjH(map_XX,map_ZZ, horizontalLimits)
    % -----------------------------------------
    

    
    
    % -----------------------------------------
    cetStrCell{1} = 'Select CET';
    for k=2:size(map_ZZ,1)+1
        cetStrCell{k} = char([ 'CET-Slice ' num2str( map_YY(k-1,:) ) ]);
    end    
    pm_cetSliceSel = uicontrol('Style','popup',...
                                'Parent',hPan,...
                                'String',cetStrCell,...
                                'FontSize',12,...
                                'Tag','pm_cetSlice',...
                                'Position',[20 20 180 35],...
                                'Callback',{@pm_cetSelect_Callback, hax3D,haxCET});
    % -----------------------------------------
    cieStrCell{1} = 'Select CIE';
    for k=2:size(map_ZZ,2)+1
        cieStrCell{k} = char([ 'CIE-Slice ' num2str( map_XX(:,k-1) ) ]);
    end    
    pm_cieSliceSel = uicontrol('Style','popup',...
                                'Parent',hPan,...
                                'String',cieStrCell,...
                                'FontSize',12,...
                                'Tag','pm_cieSlice',...
                                'Position',[210 20 180 35],...
                                'Callback',{@pm_cieSelect_Callback, hax3D,haxCIE});
%      % -----------------------------------------      
%      pb_saveSlices = uicontrol('Style','pushbutton',...
%                                 'Parent',hPan,...
%                                 'String','Mouse select',...
%                                 'FontSize',12,...
%                                 'Position',[400 31 100 25]...      %,...    'Callback',{@pb_saveSlices_Callback, hax3D, pm_cetSliceSel, pm_cieSliceSel, mapFolder}
%                             );
%                         
% %     [x,y] = ginput(1)                        
%     % -----------------------------------------

    
                            
     % -----------------------------------------      
     pb_saveSlices = uicontrol('Style','pushbutton',...
                                'Parent',hPan,...
                                'String','Save Slices',...
                                'FontSize',12,...
                                'Position',[540 31 100 25],...
                                'Callback',{@pb_saveSlices_Callback, hax3D, pm_cetSliceSel, pm_cieSliceSel, mapFolder});
    % -----------------------------------------
    
    % -----------------------------------------    
    pause(0.001)                        % NOTE:  1ms delay to allow updating the lots (avoids grey panels)    
    % -----------------------------------------    


    clear  map_XX map_YY map_ZZ


end

