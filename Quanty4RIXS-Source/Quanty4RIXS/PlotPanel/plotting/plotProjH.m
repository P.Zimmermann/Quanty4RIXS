function plotProjH(mapXX,map_ZZ,horizontalLimits)


    map_ZZ(isnan(map_ZZ)) = 0;            % replace NaN with zeros


    projH_X = mapXX(1,:);
%     projH_X = mapXX(:,1);
    projH_Y = mean(map_ZZ,1);              % sum(A,2) sum of elements in a row 
%     projH_Y = sum(map_ZZ,1);              % sum(A,2) sum of elements in a row 
    projH_Y(isnan(projH_Y)) = 0;            % replace NaN with zeros
   
    
    % ---------------------------------------------
    % in RAW mode make stem plot
    if getValTag('cb_plotRAW') == 1
        stem(projH_X,projH_Y,'Marker','.')
    else
        plot(projH_X,projH_Y)
    end    
    % ---------------------------------------------
%     view([-90 90])
    set(gca,'XAxisLocation','bottom')       % 
    set(gca,'YAxisLocation','left')     % is due to rotation top


    % --------------------------------------------------------        
    xlabel('Incident energy (eV)');
    ylabel('Intensity');
%     daspect([1 1 1])
%     xlim([-6.5 6.5])
%     xlim([-10 12])            
%       xlim([-6.5 6.5])
    
%     ax2D = findobj('Tag','ax2Dmap')
%     xlim2D = get(ax2D,'XLim')
% 
% 
%     xlim([xlim2D(1) xlim2D(2)])
        
        
%     if min(min(projH_X)) < max(max(projH_X))
%         xmin = min(min(projH_X));
%         xmax = max(max(projH_X));
    if min(projH_X) < max(projH_X)
        xmin = min(projH_X);
        xmax = max(projH_X);
    else
       xmin = -1; 
       xmax = +1;
    end
    
    if min(min(projH_Y)) < max(max(projH_Y))
        ymin = min(min(projH_Y));
        ymax =  max(max(projH_Y));
    else
        ymin = -1;
        ymax = +1; 
    end
    if length(horizontalLimits) == 2
        xlim(horizontalLimits)
    else
        % xlim([min(min(xx)) max(max(xx))])
        xlim([xmin xmax])
    end  
%     

    ylim([ymin ymax])         
% %     xlim([min(min(projH_X)) max(max(projH_X))])
% %     ylim([min(min(projH_Y)) max(max(projH_Y))])  
              
    % --------------------------------------------------------

%         title(titleStr,'FontSize',13)
%                
%         % --------------------------------------------------------

    set(gca,'FontSize',12)


    clear mapXX map_ZZ
    
end

