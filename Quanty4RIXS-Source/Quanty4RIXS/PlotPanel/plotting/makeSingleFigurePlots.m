function hViewFig =  makeSingleFigurePlots( figNum, dataX,dataY,dataZ, titleStr, mapFolder, saveImgName )


    % -------------------------------------------------------
    debugMsg( 'calling makeSingleFigurePlots()', 2 )
    % -------------------------------------------------------
    pltFigWidth = 465;
    pltFigHeight = 500;

    % --------------------------------------------------------------
    % Note !!!for Multimaps X and map do NOT match!!!)                                
    % NOTE: Here a check is needed for Multimaps. XX and width of ZZ doent match for multimaps!!!
    if getValTag('cb_splitMM') == 1            
        subMapNo = getValTag('sl_selMultimap');
        xasRange = size(dataX,2);                       % get range of energies in Ein
        indMin = 1 + (subMapNo-1)*xasRange;             % get index Min for selected range
        indMax = xasRange + (subMapNo-1)*xasRange;      % get index Max for selected range
        dataZ = dataZ(:,indMin:indMax);                  % split Multimap    
        % ------------------------------------
        saveImgName = [saveImgName '-Sub' num2str(subMapNo) 'of' num2str(size(dataZ,2)/xasRange) ];               % update Savename to show which MM is selected
    elseif getValTag('cb_joinMaps') == 1    
        % sum maps over several states (selected with multimap slider)
        subMapNo = getValTag('sl_selMultimap');
        xasRange = size(dataX,2);                       % get range of energies in Ein

        newDataZ = zeros(size(dataZ,1),xasRange);       % init empty map with Zeros for the loop

        for x=1:subMapNo
             % ------------------------------------
            indMin = 1 + (x-1)*xasRange;             % get index Min for selected range
            indMax = xasRange + (x-1)*xasRange;      % get index Max for selected range
            newDataZ = newDataZ + dataZ(:,indMin:indMax);                  % split Multimap    
             % ------------------------------------
        end
        clear x
        dataZ = newDataZ;
        % ------------------------------------        
        clear newDataZ
        % ------------------------------------
%         saveImgName = [saveImgName '-Joined' num2str(subMapNo) 'to' num2str(size(dataZ,2)/xasRange) ];               % update Savename to show which MM is selected    
        saveImgName = [saveImgName '-Joined_1to' num2str(getValTag('sl_selMultimap'))  ];               % update Savename to show which MM is selected

    end
    % --------------------------------------------------------------
    % compare XASrange and mapData width (needed for Multimaps, will be used if splitting of not selected other disagreements)
    % else:  width of dataX and dataZ is the same, dataX from mapset is used
    if ~(size(dataX,2) == size(dataZ,2))             % compare widths of both maps
        debugMsg( '!!! NOTE: LENGTH MISMATCH: Using 1..length(dataZ) as dataX range. ', 1 )
        dataX = [1:size(dataZ,2)];                               % assign 1...length(dataZZ) as dataXX
    end        
    % --------------------------------------------------------------
    % make figure and plots with data in  dataX, dataY, dataZ
    hViewFig = figure(figNum);
    set(hViewFig,'Units','pixel');
    figPos = get(hViewFig,'Position');                
    set(hViewFig,...
        'Position',[figPos(1) figPos(2) pltFigWidth pltFigHeight],...   
        'MenuBar', 'None',...
        'CloseRequestFcn',@myDeleteFig)
    hPan = makePanelMapAndProjections(hViewFig, 10, 10, dataX, dataY, dataZ, titleStr);         
    % --------------------------------------------------------------
    % save figure to file im save is on
    if getValTag('cb_savePlots') == 1 
        % ---------------------------------------------
        [p, name, ext] = fileparts(saveImgName);
        updateStatus(['Saving Plot: ' name '.png' ])   
        clear p name ext
        % ---------------------------------------------
%         saveFigure2Image(hViewFig,saveImgName)
        saveFigure2Image(hViewFig,mapFolder, saveImgName)
        % ---------------------------------------------
    end
    % --------------------------------------------------------------
    % normalised figure and all children (to allow scaling of the window )
    normaliseAll(hViewFig)
    % --------------------------------------------------------------
%     pause(0.003)                        % NOTE:  1ms delay to allow updating the lots (avoids grey panels)
    % --------------------------------------------------------------

    % =============================================================================================================== 
   
    % ------------------------------------
    function myDeleteFig(arg1,arg2)
%         arg1
%         arg2        
        delete(gcf)
    end
    % =============================================================================================================== 
    % =============================================================================================================== 




    clear dataX dataY dataZ 



end

