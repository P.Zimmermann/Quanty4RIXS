function [ output_args ] = plotTS( valsGS, selectorTagGS )
%PLOTTS Summary of this function goes here
%   Detailed explanation goes here

    selMM = getValTag('sl_selMultimap');                % Number of selected submaps
    if getValTag('cb_splitMM')
        selMaps = [selMM];
    elseif getValTag('cb_joinMaps')
        selMaps = [1:selMM];
    else
        selMaps = [];
    end    

    for n=1:length(valsGS)
        
        if ismember(n, selMaps) && selectorTagGS
            col = 'red';
            dx = 0.05;
        else
            col = 'blue';
            dx = 0;
        end    
        
%         offSet = - (limY(2) - limY(1))
        offSet = 0;
        
	    hl = line([0.1-dx 0.9+dx],[valsGS(n) valsGS(n)]+offSet,'Color',col,'LineWidth',0.1);
 
    end

    yLimits(1) = min(valsGS)-0.5;
    yLimits(2) = max(valsGS)+0.5;
        
    % Plotranges
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])   
%     xlim([0 4.2])
%     yLimits = get(gca,'YLim');   
    ylim([yLimits(1) yLimits(2)])    
 
    pause(0.001)
    

end

