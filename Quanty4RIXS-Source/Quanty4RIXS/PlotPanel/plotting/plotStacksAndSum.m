function hFig = plotStacksAndSum(figNum, rangeEin_L,xasSlices_L, rangeEin_R,xasSlices_R, distString, maxGS )
%PLOTSTACKS Summary of this function goes here
%   Detailed explanation goes here


%     % ------------------------------------
%     % plot settings
%     x0_l = 0.05;
%     x0_c = 0.33 + x0_l;
%     x0_r = 0.6 + 2*x0_l;
%     axW = 0.28;
%     axH = 0.2;
%     % ------------------------------------
    % ------------------------------------
    % plot settings
    x0_l = 0.05;
    x0_c = 0.49 + x0_l;
    axW = 0.43;
    axH = 0.18;
    % ------------------------------------
    
    % ------------------------------------    
%     contourf(xasSlices_L)    
    hFig = figure(figNum); clf;
    figPos = get(hFig,'Position');
    set(hFig,'Position',[figPos(1:2) 500 900])
    
    tbox = uicontrol('Style','text','String',distString,...
                        'FontSize',16,'FontWeight','bold',...
                        'Units','Normalized','Position',[0.01 0.94 0.98 0.06]);
    
    % ------------------------------------    
    
    % plot LCP XAS    
    axes('Position',[x0_l 0.73 axW axH])
    pltSlicesStack(rangeEin_L,xasSlices_L, ['LCP'],[0.3,0.9,0.5], maxGS)    
    
    
    % plot RCP XAS
%     figure(2); clf;
    axes('Position',[x0_l 0.49 axW axH])
    pltSlicesStack(rangeEin_R,xasSlices_R, ['RCP'], 'blue',maxGS)    
        
    % plot XAS SUM
%     figure(3); clf;
    axes('Position',[x0_l 0.26 axW axH])
    pltSlicesStack(rangeEin_L,xasSlices_L+xasSlices_R, ['L+R'], 'red',maxGS)    
        
    % plot XAS MCD
%     figure(4); clf;
    axes('Position',[x0_l 0.025 axW axH])
    pltSlicesStack(rangeEin_R,xasSlices_L-xasSlices_R, ['L-R (MCD)'], 'red',maxGS)    
  
    % ------------------------------------

    
    
    % ------------------------------------
    % plot LCP XAS
    axes('Position',[x0_c 0.73 axW axH])
    pltSlicesSum(rangeEin_L,xasSlices_L, ['Total LCP'], [0.3,0.9,0.5],maxGS)    
    
    
    % plot RCP XAS
%     figure(2); clf;
    axes('Position',[x0_c 0.49 axW axH])
    pltSlicesSum(rangeEin_L,xasSlices_R, ['Total RCP'], 'blue',maxGS)    
        
    % plot XAS SUM
%     figure(3); clf;
    axes('Position',[x0_c 0.26 axW axH])
    pltSlicesSum(rangeEin_L,xasSlices_L+xasSlices_R, ['Total L+R'], 'red',maxGS)    
        
    % plot XAS MCD
%     figure(4); clf;
    axes('Position',[x0_c 0.025 axW axH])
    pltSlicesSum(rangeEin_R,xasSlices_L-xasSlices_R, ['Total L-R (MCD)'], 'red',maxGS)    
  
    % ------------------------------------
       
    
    
    
    
    
%     % ------------------------------------
%     % plot LCP XAS
%     axes('Position',[x0_r 0.745 axW axH])
%     pltSlicesTotal(rangeEin_L,xasSlices_L, ['Total LCP'], [0.3,0.9,0.5],maxGS)    
%     
%     
%     % plot RCP XAS
% %     figure(2); clf;
%     axes('Position',[x0_r 0.505 axW axH])
%     pltSlicesTotal(rangeEin_L,xasSlices_R, ['Total RCP'], 'blue',maxGS)    
%         
%     % plot XAS SUM
% %     figure(3); clf;
%     axes('Position',[x0_r 0.265 axW axH])
%     pltSlicesTotal(rangeEin_L,xasSlices_L+xasSlices_R, ['Total L+R'], 'red',maxGS)    
%         
%     % plot XAS MCD
% %     figure(4); clf;
%     axes('Position',[x0_r 0.025 axW axH])
%     pltSlicesTotal(rangeEin_R,xasSlices_L-xasSlices_R, ['Total L-R (MCD)'], 'red',maxGS)    
%   
%     clear rangeEin_R rangeEin_L xasSlices_L xasSlices_R
%    
    % ================================================================================
    function pltSlicesStack(rangeEin,xasSlices, titleStr,col, maxGS)
%         for s = 1:size(xasSlices,2)   
        for s = 1:min(size(xasSlices,2),maxGS)                     % only lowest maxGS GS   
            hold on
            plot(rangeEin,0.5*s+xasSlices(:,s),...
                'Color',col,...
                'LineWidth',2)
        end        
        title(titleStr)
        xlim([-5 3])               
        
    end    
    % ================================================================================
    function pltSlicesSum(rangeEin,xasSlices, titleStr,col, maxGS)

        sumSlice = xasSlices(:,1);  
        if maxGS > 1
            for s = 2:min(size(xasSlices,2),maxGS)                     % only lowest maxGS GS   
                hold on
                sumSlice = sumSlice + xasSlices(:,s);
            end 
        end        
        plot(rangeEin,sumSlice,...
            'Color',col,...
            'LineWidth',2)
        title(titleStr)
        xlim([-4 3])               
        
    end    
    % ================================================================================
    function pltSlicesTotal(rangeEin,xasSlices, titleStr,col, maxGS)

        sumSlice = xasSlices(:,1);  
        if maxGS > 1
            for s = 2:min(size(xasSlices,2),maxGS)                     % only lowest maxGS GS   
                hold on
                sumSlice = sumSlice + xasSlices(:,s);
            end 
        end        
        plot(rangeEin,sumSlice,...
            'Color',col,...
            'LineWidth',2)
        title(titleStr)
        xlim([-4 3])               
        
    end    
    % ================================================================================
 
end

