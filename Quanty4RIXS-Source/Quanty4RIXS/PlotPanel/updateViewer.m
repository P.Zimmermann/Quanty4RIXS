function handles = updateViewer( handles )
%
% NOTE: HERE Multimaps are splitted:  see subfunction makeFigureAndPlots(figNum, dataX,dataY,dataZ, titleStr) 
%

    % -------------------------------------------------------
    debugMsg( 'calling updateViewer()', 2 )
    % --------------------------------------------
    updateStatus(['Updating Plots...'])
    % --------------------------------------------
%     pause(0.01);    

    % -------------------------------------------------------
    % add other mapSets from handles.mapSet{} cell array
    if length(handles.hBackend.mapData) >= getValTag({'pm_mapSet'})
        % ------------------------------------------------------------------------
        % NOTE: this loads also multimaps as they are. XX and width of ZZ doent match for multimaps!!!
        clear thisMapSet
        
        thisMapSet = handles.hBackend.mapData{getValTag({'pm_mapSet'})};
        
        % ------------------------------------------------------------------------------------------------------------
        % ------------------------------------------------------------------------------------------------------------
        % SHOW ELDDiagram VALUES if cb is selected
        % ------------------------------------------------------------------------------------------------------------
        if (getValTag('cb_plotXAS') == 1) && (getValTag({'pm_mapSet'}) > 0)
                % --------------------------------------------
            updateStatus(['Plotting XAS spectra...'])   
            % --------------------------------------------
            updateXASPlot( handles, thisMapSet );
            % --------------------------------------------
        end        
        % ------------------------------------------------------------------------------------------------------------
        % ------------------------------------------------------------------------------------------------------------
        
        
        % ------------------------------------------------------------------------      
        % SHOW OBSERVABLES/EXPECTATION VALUES if cb is selected
        % ------------------------------------------------------------------------      
        if (getValTag('cb_showVals') == 1) && (getValTag({'pm_mapSet'}) > 0)
                % --------------------------------------------
            updateStatus(['Plotting expectation values for GS,IS,FS.'])   
            % --------------------------------------------
            updateValuesGSISFS( handles, thisMapSet );
            % --------------------------------------------
        end
        % ------------------------------------------------------------------------      

        % ------------------------------------------------------------------------      
        % SHOW ELDDiagram VALUES if cb is selected
        % ------------------------------------------------------------------------      
        if (getValTag('cb_ELDiagram') == 1) && (getValTag({'pm_mapSet'}) > 0)
                % --------------------------------------------
            updateStatus(['Plotting Energy Level Diagram.'])   
            % --------------------------------------------
            updateELDiagram( handles, thisMapSet );
            % --------------------------------------------
        end        
        % ------------------------------------------------------------------------------------------------------------
        % ------------------------------------------------------------------------------------------------------------
        thisFigNum = 100*getValTag({'pm_mapSet'});       
        % ------------------------------------------------------------------------
        % LCP plot (plot LCP if exists and selected)
        % ------------------------------------------------------------------------
        
        lcpFigNum = thisFigNum+10;
        if (getValTag({'cb_lcpMap'}) == 1)                  
            % ------------------------------------------------------------------------           
            hLCPFig = makeSingleFigurePlots(lcpFigNum,...
                            thisMapSet.energyIn, thisMapSet.energyOut, thisMapSet.lcpMap,...            % getting the SUM map data 
                            'LCP',...
                            thisMapSet.mapFolder, [filesep 'img_' thisMapSet.baseName '-LCP' ]) ;  
            if getValTag('cb_saveData') == 1
                thisMapSet.saveMapDataLCP();
            end    
            % ------------------------------------------------------------------------                          
            % ------------------------------------------------------------------------                     
        else
            close(figure(lcpFigNum))                  % DELETING figure when deselected
        end
        % ------------------------------------------------------------------------

        % ------------------------------------------------------------------------
        % RCP plot (plot RCP if exists and selected)
        % ------------------------------------------------------------------------
        rcpFigNum = thisFigNum+20;
        if (getValTag({'cb_rcpMap'}) == 1)                  
            % ------------------------------------------------------------------------           
            hRCPFig = makeSingleFigurePlots(rcpFigNum,...
                            thisMapSet.energyIn, thisMapSet.energyOut, thisMapSet.rcpMap,...            % getting the SUM map data 
                            'RCP',...
                            thisMapSet.mapFolder, ['img_' thisMapSet.baseName '-RCP' ]) ;   
            % --------------------------------------------------------------
            if getValTag('cb_saveData') == 1
                thisMapSet.saveMapDataRCP();
            end    
            % ------------------------------------------------------------------------  
        else
            close(figure(rcpFigNum))                  % DELETING figure when deselected            
        end
        % ------------------------------------------------------------------------

        % ------------------------------------------------------------------------
        % SUM plot (plot SUM if exists and selected)
        % ------------------------------------------------------------------------
        sumFigNum = thisFigNum+30;
        if (getValTag({'cb_sumMap'}) == 1)                  
            % ------------------------------------------------------------------------           
            hSUMFig = makeSingleFigurePlots(sumFigNum,...
                            thisMapSet.energyIn, thisMapSet.energyOut, thisMapSet.sumMap,...            % getting the SUM map data 
                            'LCP + RCP = SUM',...
                            thisMapSet.mapFolder, ['img_' thisMapSet.baseName '-SUM' ]) ;           
            % ------------------------------------------------------------------------                     
        end
        % ------------------------------------------------------------------------
               
        % ------------------------------------------------------------------------
        % MCD plot (plot MCD if exists and selected)
        mcdFigNum = thisFigNum+40;
        % ------------------------------------------------------------------------
        if (getValTag({'cb_mcdMap'}) == 1)
            % ------------------------------------------------------------------------           
            hMCDFig = makeSingleFigurePlots(mcdFigNum,...
                            thisMapSet.energyIn,  thisMapSet.energyOut,  thisMapSet.mcdMap,...            % getting the MCD mapSet 
                            'LCP - RCP = MCD',...
                            thisMapSet.mapFolder, ['img_' thisMapSet.baseName '-MCD' ]);
            % ------------------------------------------------------------------------   
        end   
        % ------------------------------------------------------------------------
        
        % ------------------------------------------------------------------------      
        % SUM & MCD SUMMARY plot 
        % ------------------------------------------------------------------------       
        % TODO:
        % MOVE this save2file of section to the makeSummery routine.
        %
        if (getValTag({'cb_mapSummary'}) == 1)
            % --------------------------------------------------------------        
            hSummaryFig = makeSummaryFigure( handles, thisMapSet );
            % --------------------------------------------------------------
            if getValTag('cb_saveData') == 1
                thisMapSet.saveMapData();
            end    
            % --------------------------------------------------------------
            % save figure to file im save is on
            if getValTag('cb_savePlots') == 1 
                saveImgName = ['img_' thisMapSet.baseName '-Summary' ];
                % add GS info to name if split multimap is selected
                if getValTag('cb_splitMM') == 1 
                    saveImgName = [saveImgName '-Split' num2str(getValTag('sl_selMultimap')) 'of' num2str( get(findobj('Tag','sl_selMultimap'),'Max') ) ];               % update Savename to show which MM is selected
                elseif getValTag('cb_joinMaps') == 1 
%                     saveImgName = [saveImgName '-Join' num2str(getValTag('sl_selMultimap')) 'to' num2str( get(findobj('Tag','sl_selMultimap'),'Max') ) ];               % update Savename to show which MM is selected
                    saveImgName = [saveImgName '-Joined_1to' num2str(getValTag('sl_selMultimap'))  ];               % update Savename to show which MM is selected
                end
                % ---------------------------------------------
                updateStatus(['Saving Plot: ' saveImgName '.png' ])   
                clear p name ext
                % ---------------------------------------------
%                 saveFigure2Image(hSummaryFig, [thisMapSet.mapFolder saveImgName])
                saveFigure2Image(hSummaryFig, thisMapSet.mapFolder, saveImgName)
                % ---------------------------------------------
            end
            % --------------------------------------------------------------      
        end 
        % ------------------------------------------------------------------------      


        pause(0.01);    
        
        % ------------------------------------------------------------------------------------------------------------
        updateStatus(['Updating Plots...Done!'])       
        disp('done updateViewer()')
        % ------------------------------------------------------------------------------------------------------------
        clear thisMapSet;
        % ===============================================================================================================         
    end
      
    % =============================================================================================================== 
    % =============================================================================================================== 

end
 