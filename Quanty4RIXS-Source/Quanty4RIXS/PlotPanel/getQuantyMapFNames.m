function mapFiles = getQuantyMapFNames( mapSaveDir, mapBaseName )

    % -------------------------------------------------------
    debugMsg( 'calling getQuantyMapFNames()', 1 )
    % -------------------------------------------------------
    
    mapFiles = {};

    % look in the given path for all   mapBaseName*.txt files 
%     fullMapsPath = [mapSaveDir filesep mapBaseName '*Spec.txt'];        % we use this as extension for Quanty maps caluclated with this program
    fullMapsPath = [mapSaveDir mapBaseName '*Spec.txt'];        % we use this as extension for Quanty maps caluclated with this program
    mapDirList = dir(fullMapsPath);

    if isfield(mapDirList,'name') && length(mapDirList) > 0
           
        if length(mapDirList) > 1
            for m=1:length(mapDirList)

                thisFName = mapDirList(m).name
                % here the map file name is written to the mapfiles cell list
%                 mapFiles{m} = [mapSaveDir filesep thisFName];
                mapFiles{m} = thisFName;

            end
        else
            
            mapFiles = mapDirList.name;
        
        end        
        
        
    end
        




end

