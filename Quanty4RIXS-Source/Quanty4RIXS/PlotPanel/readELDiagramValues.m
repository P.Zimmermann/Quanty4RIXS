function [headers, cfValues, valData ] = readELDiagramValues( fileName )
% Read Energy Level diagram


    if iscell(fileName)
        fileName = fileName{1};
    end

    if exist(fileName) == 2
        % --------------------------------------------------    
        % open File for reading
        fileID = fopen(fileName);
        % --------------------------------------------------
        blockIndex = 0;        
        while blockIndex >= 0   
            
            thisline = fgets(fileID);                         % read line by line  
            if thisline == -1
                % stop loop if empty
                blockIndex = -1;         % Abort condition when line is empty . (This should only happen at the end)            
            else
                % --------------------------------------------------
                splitLine = strsplit(deblank(thisline));               % remove leading or trailing spaces and split line     
                % --------------------------------------------------
                if length(splitLine) > 0
                    % ------------------------------------------------------------------------------------------------
                    if strcmp(splitLine{1},'#CF_10Dq') || strcmp(splitLine{1},'#CF_Ds') || strcmp(splitLine{1},'#CF_Dt') || strcmp(splitLine{1},'#CF_M')  || strcmp(splitLine{1},'#CF_Fdd')  || strcmp(splitLine{1},'#CF_F0dd')  || strcmp(splitLine{1},'#CF_F2dd') || strcmp(splitLine{1},'#CF_F4dd') || strcmp(splitLine{1},'#CF_SOC')                        
                        % MARKER found, this Line has CF values and next line will be the header line
                        blockIndex = blockIndex + 1;                    % next block begins, as we start a new header
                        
                        splitLine
                        length(splitLine)
                        
                        if length(splitLine) == 5
                            cfValues{blockIndex} = [str2num(splitLine{2}) str2num(splitLine{3}) str2num(splitLine{4}) str2num(splitLine{5})];
                        elseif length(splitLine) == 11
                            cfValues{blockIndex} = [str2num(splitLine{2}) str2num(splitLine{3}) str2num(splitLine{5}) str2num(splitLine{7}) str2num(splitLine{9}) ];
                        else
                            cfValues{blockIndex} = [0 0 0 0 0];
                        end
                        
                        lineH = fgets(fileID);                         % read line by line 
                        thisHeader = strsplit(lineH,{' ','#','\n'},'CollapseDelimiters',true);
                        thisHeader = thisHeader(~cellfun('isempty',thisHeader));                      % remove empty cells  
                        
%                         sz_head=size(thisHeader)                    % TODO: remove after ebugging
                        % generat eheader mapping (series of numbers '%n ')
                        headerMapping = '';
                        for k=1:length(thisHeader)                               
                            headerMapping = [headerMapping '%f ' ];
                        end
%                         headerMapping                            % TODO: remove after ebugging   
                        
                        headers{blockIndex} = thisHeader;                           % store header in cell
                        valData{blockIndex} = zeros(0, length(thisHeader));       % init empty row                        
                    else % end of marker found and header line reading
                        % ==============================================================================
                        % reading the actual data block
                        % ------------------------------------------------------------------------------------------------
                        % extract data from line
                        % returns cell list with 1x2 double 
                        dataVals = textscan(thisline,...
                                headerMapping,...
                                'collectoutput', true,...
                                'Delimiter',' ',...                         % 'HeaderLines',2,...
                                'MultipleDelimsAsOne',1);                                             
                        % returns the 2x46 data set, second row is NaNs (why???)
                        dataVals = dataVals{1,:};                

                        if size(dataVals,1) > 0
                            % extract the first row with the actual values
                            dataVals = dataVals(1,:);    
    %                         size(dataVals)
    %                         dv = dataVals(1,2)
                            
                            % ------------------------------------------------------------------------------------------------
                            % collecting the data for the current block 
                            valData{blockIndex} = [valData{blockIndex}; dataVals ];              
                            % ------------------------------------------------------------------------------------------------
                        end
                        % ------------------------------------------------------------------------------------------------
                        % ==============================================================================
                    end
                    % ------------------------------------------------------------------------------------------------                
                else
                    blockIndex = -1;         % Abort condition when line is empty . (This should only happen at the end)
                end
                clear thisline splitLine
                % --------------------------------------------------
            end % if thisline 
        
        end % while blockindex
        % --------------------------------------------------
        fclose(fileID);        
        % --------------------------------------------------
%         
%         
%         
%         sz_vd1 = size(valData{1})
%         
%         
%         
%         
%         
%         
%         
%         % --------------------------------------------------
%         % Return Values 
%         cfValues
%         
%         
%         headerDq = headers{1};
%         headerDs = headers{2};
%         headerDt = headers{3};
%         tmpDq = valData{1};
%         tmpDs = valData{2};
%         tmpDt = valData{3};
    % --------------------------------------------------
    else
        % --------------------------------------------------
%         headerDq = [];
%         headerDs = [];
%         headerDt = [];
%         tmpDq = [];
%         tmpDs = [];
%         tmpDt = [];
        cfValues = [];
        headers = [];
        valData = [];
        % --------------------------------------------------
    end   
    
    
    
    
    
    
    
    
    
    
    
    

end


       
        
            
