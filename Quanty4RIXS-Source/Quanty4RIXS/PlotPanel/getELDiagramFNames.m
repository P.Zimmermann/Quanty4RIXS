function eldFiles = getELDiagramFNames( mapSaveDir, mapBaseName )

    % -------------------------------------------------------
    debugMsg( 'calling getELDiagramFNames()', 1 )
    % -------------------------------------------------------
    
    eldFiles = {};

    % look in the given path for all   mapBaseName*.txt files 
%     fullMapsPath = [mapSaveDir filesep mapBaseName '*Spec.txt'];        % we use this as extension for Quanty maps caluclated with this program
    fullMapsPath = [mapSaveDir mapBaseName '*ELDiagram.txt'];        % we use this as extension for Quanty maps caluclated with this program
    mapDirList = dir(fullMapsPath);

    if isfield(mapDirList,'name') && length(mapDirList) > 0
    
        eldFiles = mapDirList.name;
        
    end
        
%     if length(mapDirList) > 0
%         for m=1:length(mapDirList)
%             
%             thisFName = mapDirList(m).name
%             % here the map file name is written to the mapfiles cell list
%             mapFiles{m} = [mapSaveDir filesep thisFName];
%     
%         end
%     end




end

