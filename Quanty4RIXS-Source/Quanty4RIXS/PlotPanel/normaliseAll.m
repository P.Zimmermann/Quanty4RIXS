function normaliseAll( thisHandle )

% this function stes the Units of all choldren to normalized


    if isprop(thisHandle,'Units')
        set(thisHandle,'Units','normalized') 
    end

    hChildren = get(thisHandle,'children');

    if length(hChildren) > 0
        for l=1:length(hChildren)
            if iscell(hChildren)
                if ~isempty(hChildren{l})
                    normaliseAll(hChildren{l})
                end 
            else    
                if ~isempty(hChildren)
                    normaliseAll(hChildren)
                end 
            end    
        end
    end


end

