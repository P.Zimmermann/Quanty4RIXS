function xasFiles = getXASFNames( mapSaveDir, mapBaseName )

    % -------------------------------------------------------
    debugMsg( 'calling getXASFNames()', 1 )
    % -------------------------------------------------------
    
    xasFiles = {};

    % look in the given path for all   mapBaseName*.txt files 
    fullMapsPath = [mapSaveDir mapBaseName '*XAS-Spec.txt']        % we use this as extension for Quanty maps caluclated with this program
    mapDirList = dir(fullMapsPath);
        
    if length(mapDirList) > 0
        for m=1:length(mapDirList)
            
%             thisFName = mapDirList(m).name
            % here the map file name is written to the mapfiles cell list
%             xasFiles{m} = [mapSaveDir filesep thisFName];
            xasFiles{m} = mapDirList(m).name;
    
        end
    end
    % -------------------------------------------------------

%     if isfield(mapDirList,'name') && length(mapDirList) > 0 
%         xasFiles = mapDirList.name;
%     end


end

