function handles = updateControl( handles )
%UPDATECONTROL Summary of this function goes here
%   Detailed explanation goes here


  % -------------------------------------------------------
    debugMsg('calling updateControl()   ---  TODO: change functionName from "Control"  to "Plotting" ', 0 )
  % -------------------------------------------------------




    if length(handles.hBackend.mapData) > 0
        % ---------------------------------------------
        % fill pm_mapSets with entries 1:length(handles.hBackend.mapData)
        pmStr = {};
        for n=1:length(handles.hBackend.mapData)
            
            thisBaseName = handles.hBackend.mapData{n}.baseName;
            pathStr = handles.hBackend.mapData{n}.mapFolder;
            
            % split the complete path at the fileseperator (NOTE: fileparts is not working well, with . in filename)
            splitPath = strsplit(pathStr,filesep,'CollapseDelimiters',true);                   
            splitPath(cellfun('isempty',splitPath)) = [];
            for k=1:min(length(splitPath),3)
                name{k} = splitPath{end+1-k};                       % collect elements of the path in name
            end
            % -------------------------------------------------------
            debugMsg( [ ' pathStr: ' pathStr '   name1: ' name{3} '   name2: ' name{2} '   name3: ' name{1} ' - in updateControl()'], 1 )
            % -------------------------------------------------------                         
            % fill popupmenu with parts of path and basename of spectra
            dispPath = [filesep name{3} filesep name{2} filesep name{1} filesep thisBaseName ];                      % baseName '-*-Spec.txt'
            if (length(dispPath) < 30) && (length(name) > 3)                                       % add one more level is path is shorter than 35 character
                dispPath = [ filesep name{4} filesep name{3} filesep name{2} filesep name{1} filesep thisBaseName ];
            end               
            pmStr{n} = [ num2str(n,'%02d') ': ..' dispPath ];              % assamble all paths for popupmenu  
            % -------------------------------------------------------
        end
        clear n        
        setStrTag({'pm_mapSet'},[pmStr])                                    % writing Data to the pop-up menu
%         setValTag({'pm_mapSet'},1)                                        % selecting first entry the pop-up menu
        tagEnable({'pm_mapSet'});                                           % enable popupmenu after filling
        % ---------------------------------------------          
    else
        % ---------------------------------------------    
        setStrTag({'pm_mapSet'},{'-'}) 
        setValTag({'pm_mapSet'},1)                  % selecting first entry the pop-up menu
        tagDisable({'pm_mapSet'});
        % ---------------------------------------------          
    end






end

