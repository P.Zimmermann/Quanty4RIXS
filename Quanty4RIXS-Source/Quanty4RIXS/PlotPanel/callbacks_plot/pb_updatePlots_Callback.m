function [ output_args ] = pb_updatePlots_Callback( hObject, eventdata, handles )

    % hObject == indobj('Tag','pb_updatePlots')
    % --------------------------------------------
    debugMsg( 'calling pb_updatePlots_Callback()', 2 )
    % --------------------------------------------           
        
    % --------------------------------------------
    handles = updateViewer( handles );
    % --------------------------------------------

    % -------------------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % -------------------------------------------------------



end

