function sl_OPvalMax_Callback(  hObject, eventdata, handles)


    % --------------------------------------------
    debugMsg( 'calling sl_OPvalMax_Callback()', 2 )
    % --------------------------------------------  

    maxGS = getValTag('sl_OPvalMax');
    if ~isinteger(maxGS)                    % set to integer is not, otherwise plotEnergyes() will complain about non integer array
        setValTag('sl_OPvalMax',round(maxGS) )
    end    
    % --------------------------------------------
    
    setStrTag('txt_OPmaxVal',[ '1 to ' num2str(maxGS) ])
    
    % --------------------------------------------
    % directly plot/update ploits on click
%   handles = updateViewer( handles );
    % --------------------------------------------


        
end

