function [ output_args ] = pb_importMaps_Callback(hObject,eventdata,handles)
%PB_LOADMAPS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------------
    debugMsg( 'calling pb_importMaps_Callback()', 2 )
    % -------------------------------------------------------
%     hObject
%     handles
        
    if isfield(handles.appSettings,'lastDir')
        lastDir = handles.appSettings.lastDir;
    else
        lastDir = ''; 
    end
    [filenames, pathname, filterindex] = uigetfile( ...
                                              {'*Spec.txt;*OPvals.txt;*ELDiagram.txt','Quanty4RIXS Files (*Spec.txt, *OPvals.txt, *ELDiagram.txt)'; ...
                                              '*Spec.txt','QUANTY Maps (*Spec.txt)'; ...
                                              '*OPvals.txt','Obs Values (*OPvals.txt)'; ...                      %'*.mat','MATLAB Files (*.mat)'; ...                             % '*_quanty.dat;*.dat','QUANTY Maps (*_quanty.dat, *.dat)'; ...
                                              '*ELDiagram.txt','Energy Level Diagrams (*ELDiagram.txt)'; ...
                                               '*.*',  'All Files (*.*)'}, ...
                                               'Pick file(s)...', ...
                                               lastDir,...
                                               'MultiSelect', 'on');       
    clear lastDir;                                       
    % -------------------------------------------------------
    pathname                                     
    filenames
    % -------------------------------------------------------
    if ~isequal(pathname,0)
        handles.appSettings.lastDir = pathname;               % update lastDir in handles

        % ------------------------------------------------
        % write last Dir and all current App settings to file   
        saveAppSettingsFile( handles )
        % -------------------------------------------------------
        % do according step
        if length(filenames) > 0
            handles.hBackend.importMapDirData( pathname, filenames)
        else    
            disp('Nothing to do - in pb_importMaps_Callback() ')
        end        
          
    end
                
    % --------------------------------------------------------------------------------    
    % update viewer cpmtrol panel
    handles = updateControl( handles ) ;   
    % --------------------------------------------------------------------------------    
    % selecting last entry in popupmenu (last is the last added one)
    setValTag({'pm_mapSet'},length(getStrTag({'pm_mapSet'})))                  % selecting last entry the pop-up menu
    pm_mapSet_Callback( findobj('Tag','pm_mapSet'), 0, handles)         % launch callback to auload last mapset    
    % --------------------------------------------------------------------------------    
    % update plots and views
%     handles = updateViewer( handles );
     % --------------------------------------------------------------------------------    

    % -----------------------------------------
    % Update handles structure
%     guidata(handles.output, handles);                   % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % -----------------------------------------

end

