function [ output_args ] = pb_clearMapSetList_Callback(hObject, eventdata, handles)
%PB_CLEARMAPSETLIST_CALLBACK Summary of this function goes here
%   Detailed explanation goes here


  % -------------------------------------------------------
    debugMsg( 'calling pb_clearMapSetList_Callback()', 2 )
    % -------------------------------------------------------

    % --------------------------------------------
    % delete Data from backend.
    handles.hBackend.clearMapSets();        
    % --------------------------------------------

    % --------------------------------------------
    % clean up old checkboxes
    for k=1:9
        oldHandle = findobj('Tag',['cbMapSet_' num2str(k)]);        
        delete(oldHandle)  
    end     
    clear k oldHandle
    % --------------------------------------------
    tagDisable({'cb_plotXAS','cb_plotRIXS'})    
    tagDisable({'cb_lcpMap','cb_rcpMap','cb_sumMap','cb_mcdMap','cb_mapSummary'});    
    tagDisable({'cb_showVals','sl_OPvalMax','txt_OPmaxVal'});        
    tagDisable({'cb_ELDiagram','sl_ELDvalMax','txt_ELDmaxVal'});        
    tagDisable({'cb_joinMaps','cb_splitMM','sl_selMultimap'});                % disable old Multiplot CB  
    tagDisable({'pb_updatePlots'});           % disable the UpdatePlotting Button Bakcend is cleared/empty
    tagDisable({'cb_saveData','cb_savePlots'});                                      % enable save CB            

    
    % rest values to zero for split/join CB
    setValTag({'cb_plotXAS','cb_plotRIXS'},0)
    setValTag({'cb_lcpMap','cb_rcpMap','cb_sumMap','cb_mcdMap','cb_mapSummary'},0)
    setValTag({'cb_joinMaps','cb_splitMM','cb_showVals','cb_ELDiagram'},0)
    
    % rest String infos
    setStrTag('txt_OPmaxVal',' 0 / 0')    
    set(findobj('Tag','sl_OPvalMax'),...
            'max', 1, 'SliderStep', [1, 1 ], 'Value',1); 

    setStrTag('txt_ELDmaxVal',' 0 / 0')    
    set(findobj('Tag','sl_ELDvalMax'),...
            'max', 1, 'SliderStep', [1, 1 ], 'Value',1);         

    setStrTag('txt_MMno',' 0 / 0')
    set(findobj('Tag','sl_selMultimap'),...
            'max', 1, 'SliderStep', [1, 1 ], 'Value',1);         
                
    % --------------------------------------------
    % update Plotting Panel
    handles = updateControl( handles );
    % --------------------------------------------
 
    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------
     
end

