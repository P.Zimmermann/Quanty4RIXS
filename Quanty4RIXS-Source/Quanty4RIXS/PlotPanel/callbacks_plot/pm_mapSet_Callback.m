function pm_mapSet_Callback( hObject, eventdata, handles)
%PM_MAPSET_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    % --------------------------------------------
    debugMsg( 'calling pm_mapSet_Callback()', 0 )
    % --------------------------------------------    
%     hObject


    pmSelVal = hObject.Value;

    if (pmSelVal > 0) && (length(handles.hBackend.mapData) >= pmSelVal )
    
        
%         testPathMAP = handles.hBackend.mapData{pmSelVal}.mapFolder
        
        
        % -------------------------------------------------------      
        debugMsg(['MapsSet ' num2str(hObject.Value) ' selected -- in pm_mapSet_Callback()' ],1)
        % -------------------------------------------------------      
        % delete old checkboxes
%         cbhandles = findobj('Tag',['cbMapSet_' '-regexp [123456789]') ])
%                 cbhandles = findobj('Tag',['cbMapSet_' num2str(j)])    
        % -------------------------------------------------------  
        for k=1:9
            oldHandle = findobj('Tag',['cbMapSet_' num2str(k)]);  
            delete(oldHandle)
        end 
        clear k
        
        tagDisable({'pb_slices'})            
        tagDisable({'cb_showVals','sl_OPvalMax','txt_OPmaxVal'});                                      % disable old Observables CB    
        tagDisable({'cb_ELDiagram','cb_ELDabsVals','sl_ELDvalMax','txt_ELDmaxVal'});                                      % disable old Observables CB    
        tagDisable({'cb_splitMM','cb_joinMaps','sl_selMultimap'});                      % disable old Multiplot CB    
        tagDisable({'cb_plotRIXS'})            
        tagDisable({'cb_plotXAS'})            
        tagDisable({'cb_lcpMap','cb_rcpMap','cb_sumMap','cb_mcdMap','cb_summaryMap'});  % Disable old Maps CB
        % -------------------------------------------------------      
        % get the selected mapset from the BackendHandle 
        selMapSet      = handles.hBackend.mapData{pmSelVal}
        % -------------------------------------------------------
        % fetching the Plotting Panel handle to place the checkbox
        hCtrlPan = findobj('Tag','uiPan_Plotting');
        % -------------------------------------------------------
        % adding a grey line
        annotation(hCtrlPan,'line',[0.025 0.975],[0.51 0.51],'LineWidth',0.25,'Color',[.6 .6 .6])


        % -------------------------------------------------------
        % enable XAS if LCP or RCP XAS is found/loaded in backend
        lenXAS_LCP = length( selMapSet.lcpXAS );
        lenXAS_RCP = length( selMapSet.rcpXAS );
        if (lenXAS_LCP > 0)  
            tagEnable({'cb_plotXAS'})                        
        end
        if (lenXAS_RCP > 0)    
            tagEnable({'cb_plotXAS'})                        
        end        
        % -------------------------------------------------------
        % enable RIXS if LCP or RCP XAS is found/loaded in backend
        % enable LCP and RCP CBs
        lenLCP = length( selMapSet.lcpMap );
        lenRCP = length( selMapSet.rcpMap );
        if (lenLCP > 0)  
            tagEnable({'cb_plotRIXS'})                        
            tagEnable({'cb_lcpMap'})
            tagEnable({'pb_slices'})
        end
        if (lenRCP > 0)    
            tagEnable({'cb_plotRIXS'})                        
            tagEnable({'cb_rcpMap'})
            tagEnable({'pb_slices'})            
        end
        if (lenLCP > 0) && (lenRCP > 0)    
            tagEnable({'cb_sumMap','cb_mcdMap','cb_mapSummary'})
%             tagEnable({'pb_slices'})          % redundant, will be done above for lcp/rcp            
        end
             
        % -------------------------------------------------------
        % CHECK for MULTIMAP and enable split/join uiControl
        if ((lenLCP > 1) && ~(size(selMapSet.energyIn,2) == size(selMapSet.lcpMap,2))) || ...
                ((lenRCP > 1) && ~(size(selMapSet.energyIn,2) == size(selMapSet.rcpMap,2)))
            
            % MULTIMAP DETECTED (width of energyIn and mapData dont match)
            debugMsg(['!!! FOUND Multimap in pm_mapSet_Callback() '],1)

            % -------------------------------------------------------
            % SET range for Multimap Slider
            % assuming all maps in one mapSet have same size, using first mapRaw to check width of Multimap
            widthZ = max(size(selMapSet.lcpMap, 2),size(selMapSet.rcpMap, 2));                 % width of the Multimap                             % !!! NOTE: LCP used for MM testing, RCP alone not working
            widthX = size(selMapSet.energyIn, 2);            % width of the XAS range
            noOfMaps = widthZ/widthX;     

            debugMsg( ['Multimap: ' num2str(noOfMaps) ' submaps assumed based on width.' ], 1 )
            
            % set and activate slider box
            if noOfMaps == 0
                set(findobj('Tag','sl_selMultimap'),...
                        'enable','off',...
                        'max', 1,...
                        'SliderStep',  [1, 1],... 
                        'Value',1);             
            else
                set(findobj('Tag','sl_selMultimap'),...
                        'enable','on',...
                        'max', noOfMaps,...
                        'SliderStep',  [1/(noOfMaps-1) , 1/(noOfMaps-1) ],... 
                        'Value',min(round(getValTag('sl_selMultimap')),noOfMaps));             

            end    
   
            clear noOfMaps
                        
            % -------------------------------------------------------            
            % enable checkbox for OP values
            tagEnable({'cb_splitMM','cb_joinMaps'});
            % -------------------------------------------------------            

        end
        
        % -------------------------------------------------------
        % CHECK if Expectations values exist:
        if length(selMapSet.valuesGS) > 0
            % tmpGSVals = selMapSet.valuesGS
            % size(tmpGSVals)            
            % tmpGSVals(:,1)
            % ---------------------------------------------------
            debugMsg(['!!! FOUND Expectation Values in pm_mapSet_Callback() '],1)

            % update MaxVal slider range
            maxStates = size(selMapSet.valuesGS,1);
            set(findobj('Tag','sl_OPvalMax'),...
                    'enable','on',...
                    'max', maxStates,...
                    'SliderStep',  [1/(maxStates-1) , 1/(maxStates-1) ],... 
                    'Value',min(10,maxStates)...                  
                );
            clear maxStates
            setStrTag('txt_OPmaxVal',[ '1 to ' num2str(getValTag('sl_OPvalMax')) ])
            
            % enable checkbox for OP values
            tagEnable({'cb_showVals','sl_OPvalMax','txt_OPmaxVal'});
             % ---------------------------------------------------
        else
            set(findobj('Tag','sl_OPvalMax'),...
                    'enable','off',...
                    'max', 1,...
                    'SliderStep',  [1, 1 ],... 
                    'Value',1);    
            % disable checkbox for OP values
            tagDisable({'cb_showVals','sl_OPvalMax','txt_OPmaxVal'});
        end
        % -------------------------------------------------------
            
        % -------------------------------------------------------
        % CHECK if EnegyLevel Daigram values exist:
        if length(selMapSet.eld_valData) > 0
            % tmpGSVals = selMapSet.valuesGS
            % size(tmpGSVals)            
            % tmpGSVals(:,1)
            % ---------------------------------------------------
            debugMsg(['!!! FOUND Energy Level Diagram Values in pm_mapSet_Callback() '],1)
            
            % update MaxVal slider range
%             maxStates = size(selMapSet.eld_valData{1},1)
%             maxStates = size(selMapSet.eld_valData{})

            % check all ELD data for max number of values
            % NOTE: might cause problems when plotting => make sure length is checked for plotting.
            maxStates = 0;
            for u=1:length(selMapSet.eld_valData)          
                maxStatesNew = length(selMapSet.eld_headers{u}) - 1;               % NOTE -1, because fiorst columns is the floating value  
                if maxStatesNew > maxStates
                    maxStates = maxStatesNew;
                end    
            end

            set(findobj('Tag','sl_ELDvalMax'),...
                    'enable','on',...
                    'max', maxStates,...
                    'SliderStep',  [1/(maxStates-1) , 1/(maxStates-1) ],... 
                    'Value',min(10,maxStates)...                  
                );
            clear maxStates
            setStrTag('txt_ELDmaxVal',[ '1 to ' num2str(getValTag('sl_ELDvalMax')) ])
            
            % enable checkbox and slider for ELD values
            tagEnable({'cb_ELDiagram','cb_ELDabsVals','sl_ELDvalMax','txt_ELDmaxVal'});
             % ---------------------------------------------------
        else
            set(findobj('Tag','sl_ELDvalMax'),...
                    'enable','off',...
                    'max', 1,...
                    'SliderStep',  [1, 1 ],... 
                    'Value',1);    

            tagDisable({'cb_ELDiagram','cb_ELDabsVals','sl_ELDvalMax','txt_ELDmaxVal'});
        end
        % -------------------------------------------------------
      
        
        % -------------------------------------------------------            
        % if pmMapSet is selected, then enable update plot button
        tagEnable('pb_updatePlots')    
        % enable save CBs
        tagEnable({'cb_saveData','cb_savePlots'});                                      % enable save CB            
        % -------------------------------------------------------        
    else
        % -------------------------------------------------------        
        % nothing selected or loaded: disable all
        tagDisable({'cb_showVals','sl_OPvalMax'});                                      % disable old Observables CB    
        tagDisable({'cb_ELDiagram','cb_ELDabsVals'});                                   % disable old ELDiagram CB    
        tagDisable({'cb_splitMM','cb_joinMaps','sl_selMultimap'});                      % disable old Multiplot CB    
        tagDisable({'cb_lcpMap','cb_rcpMap','cb_sumMap','cb_mcdMap','cb_mapSummary'});  % Disable old Maps CB
        tagDisable({'pb_updatePlots'})         
        tagDisable({'pb_slices'})  
        tagDisable({'cb_plotRIXS'})            
        tagDisable({'cb_plotXAS'})             
        % -------------------------------------------------------        
    end
    
    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------
     
end        
        
        
% --------------------------------------------------------------------------------------------------------------
% olnly old code below
% --------------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------------------------------
        
%         % add check boxes for indicided RAW maps in this mapSet
%         if length(selMapSet.rawMaps) > 0
%             
%             for j=1:length(selMapSet.rawMaps)
%                 % -------------------------------------------------------              
%                 handles.cbMapSet{j} = uicontrol(hCtrlPan,...
%                                                 'Style','checkbox',...
%                                                 'Units','Pixel',...
%                                                 'Tag',['cbMapSet_' num2str(j)],...
%                                                 'Position',[20 70-(j-1)*25 250 20],...                                                  % 'Position',[20+(j-1)*60 85 60 20],...
%                                                 'FontSize',12,...
%                                                 'String',['Map ' num2str(j) ': ' selMapSet.rawMaps{j}.filename ],...                   % 'Value',0,...
%                                                 'CallBack',{@cb_MapSelect_Callback, handles, j} );  
%                 % -------------------------------------------------------             
%             end

            % -------------------------------------------------------
            
%             % checkboxes for SUM and MCD
%             lenLCP = length( handles.hBackend.mapData{pmSelVal}.lcpMap )
%             lenRCP = length( handles.hBackend.mapData{pmSelVal}.rcpMap )
% 
%             if (lenLCP > 0) && (lenRCP > 0)
%                 
%                 tagEnable({'cb_lcpMap','cb_rcpMap','cb_sumMap','cb_mcdMap','cb_mapSummary'})
%                 
% %                 handles.cbMapSUM = uicontrol(hCtrlPan,...
% %                                                 'Style','checkbox',...
% %                                                 'Units','Pixel',...
% %                                                 'Tag',['cbMaps_SUM'],...
% %                                                 'Position',[280 70 100 20],...
% %                                                 'FontSize',12,...
% %                                                 'String',['RIXS SUM' ],...
% %                                                 'Value',0,...
% %                                                 'CallBack',{@cb_MapSelect_Callback, handles, j} );  
% %                 
% %                 handles.cbMapMCD = uicontrol(hCtrlPan,...
% %                                                 'Style','checkbox',...
% %                                                 'Units','Pixel',...
% %                                                 'Tag',['cbMaps_MCD'],...
% %                                                 'Position',[280 45 100 20],...
% %                                                 'FontSize',12,...
% %                                                 'String',['RIXS MCD' ],...
% %                                                 'Value',0,...
% %                                                 'CallBack',{@cb_MapSelect_Callback, handles, j} );  
% %                 handles.cbMCDSummary = uicontrol(hCtrlPan,...
% %                                                 'Style','checkbox',...
% %                                                 'Units','Pixel',...
% %                                                 'Tag',['cb_MCD_Summary'],...
% %                                                 'Position',[280 12 160 20],...
% %                                                 'FontSize',12,...
% %                                                 'String',['SUM & MCD Summary' ],...
% %                                                 'Value',0);              
%             end


%             % -------------------------------------------------------
%             % CHECK for MULTIMAP and Add Split uiControl
%             if ~(size(selMapSet.energyIn,2) == size(selMapSet.rawMaps{1}.mapIm,2))
%                 % MULTIMAP DETECTED (width of energyIn and mapData dont match)
% 
%                 disp(' FOUND Multimap in classMapSet            ---  in pm_mapSet_Callback() ')
% %                 updateStatus(['Multimap detected. - Plot as one map or split by ground state.'])      % NOTE: This removes the elapsed Time when autoload is on.
% 
%                 % enable checkbox for OP values
%             tagEnable({'cb_splitMM','cb_joinMaps'});
%             
            
            

%             % -------------------------------------------------------
%             % assuming all maps in one mapSet have same size, using first mapRaw to check width of Multimap
%             widthZ = size(selMapSet.rawMaps{1}.mapIm, 2);    % width of the Multimap
%             widthX = size(selMapSet.energyIn, 2);            % width of the XAS range
%             tmp = widthZ/widthX;     
% 
%     %         tmpPara = selMapSet.para
% 
%             if tmp == round(tmp)            % check for integer
%                 noOfMaps = tmp;                         % number of submaps in the Multimap
%                 debugMsg( ['Multimap: ' num2str(noOfMaps) ' submaps assumed based on width.' ], 1 )
%             end    
%             clear tmp;
%             
%             set(findobj('Tag','sl_selMultimap'),...
%                     'enable','on',...
%                     'max', noOfMaps,...
%                     'SliderStep',  [1/(noOfMaps-1) , 1/(noOfMaps-1) ],... 
%                     'Value',min(round(getValTag('sl_selMultimap')),noOfMaps));             
%                 
% %                 set(findobj('Tag','cb_splitMM'),...
% %                         'enable','on',...
% %                         'CallBack',{@cb_splitMM_Callback, handles} ); 
% 
% %                 handles.cbSplitMultimap = uicontrol(hCtrlPan,...
% %                                 'Style','checkbox',...
% %                                 'Units','Pixel',...
% %                                 'Tag',['cb_splitMM'],...
% %                                 'Position',[280 123 150 20],...
% %                                 'FontSize',12,...
% %                                 'String',['Split Multimap' ],...
% %                                 'Value',0,...
% %                                 'CallBack',{@cb_splitMM_Callback, handles} ); 
%                 
%             end            
%             % -------------------------------------------------------                
%         end % if length(selMapSet.rawMaps) > 0
        
        % -------------------------------------------------------
        % load Observables also when no MapData exists (length(selMapSet.rawMaps) == 0)
        % -------------------------------------------------------
        % CHECK if Expectations value files exist:
        % fnames: <baseName>_ValuesGS.txt, <baseName>_ValuesIS.txt, <baseName>_ValuesFS.txt
        % should be done in classMapSet and then it will be available via the BackendClass
% 
%         if length(selMapSet.valuesGS) > 0
%             % tmpGSVals = selMapSet.valuesGS
%             % size(tmpGSVals)            
%             % tmpGSVals(:,1)
%             % ---------------------------------------------------
%             disp(' FOUND Expectation Values  in classMapSet ---  in pm_mapSet_Callback() ')
% 
%             % enable checkbox for OP values
%             tagEnable({'cb_showVals'});
% 
%             % update MaxVal slider range
%             maxStates = size(selMapSet.valuesGS,1);
%             set(findobj('Tag','sl_OPvalMax'),...
%                     'enable','on',...
%                     'max', maxStates,...
%                     'SliderStep',  [1/(maxStates-1) , 1/(maxStates-1) ],... 
%                     'Value',min(10,maxStates)...                  
%                 );
% 
%             setStrTag('txt_maxVal',[ '1 to ' num2str(getValTag('sl_OPvalMax')) ])
%             
%             
% %             handles.cbShowValues = uicontrol(hCtrlPan,...
% %                             'Style','checkbox',...
% %                             'Units','Pixel',...
% %                             'Tag',['cb_showVals'],...
% %                             'Position',[20 123 200 25],...
% %                             'FontSize',12,...
% %                             'String',['Observables'],...
% %                             'Value',0);
% % %                                 'CallBack',{@cb_showVals_Callback, handles, selMapSet} );    
% %             
% %             handles.sl_ValMax = uicontrol(hCtrlPan,...
% %                     'Style','slider',...
% %                     'min', 1,...
% %                     'max', maxStates,...
% %                     'SliderStep',  [1/(maxStates-1) , 1/(maxStates-1) ],... 
% %                     'Units','Pixel',...
% %                     'Tag','sl_OPvalMax',...
% %                     'Position',[120 123 120 20],...
% %                     'FontSize',12,...
% %                     'String',['ValMax selector' ],...
% %                     'Value',min(10,maxStates),...
% %                     'CallBack',{@sl_OPvalMax_Callback, handles} ); 
% 
%             % ---------------------------------------------------
%         else
%             
%             tagDisable('cb_showVals');
%             set(findobj('Tag','sl_OPvalMax'),...
%                     'enable','off',...
%                     'max', 1,...
%                     'SliderStep',  [1, 1 ],... 
%                     'Value',1);    
%             
%         end
%         % -------------------------------------------------------

%         % -------------------------------------------------------            
%         % if pmMapSet is selected, then enable update plot button
%         tagEnable('pb_updatePlots')
%         %         set(findobj('Tag','pb_updatePlots'),...
% %                     'Enable','On',...
% %                     'CallBack',{@pb_updatePlots_Callback, handles})        
%         % -------------------------------------------------------            
% 
%         
%     end % pmSelVal > 0    
%     % --------------------------------------------    
%     
%     % --------------------------------------------
%     % Update handles structure
%     guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
%     % --------------------------------------------

    % ==================================================================================================================
    % ==================================================================================================================

    
    

    % ==================================================================================================================
    % ==================================================================================================================
    % --------------------------------------------------------------------------------------------------------------
%     function cb_splitMM_Callback( hObject, eventdata, handles)
% 
%         % -------------------------------------------------------
% %          hObject
%     %     eventdata
%     %     handles
%     
%         % -------------------------------------------------------
%         % assuming all maps in one mapSet have same size, using first mapRaw to check width of Multimap
%         widthZ = size(selMapSet.rawMaps{1}.mapIm, 2);    % width of the Multimap
%         widthX = size(selMapSet.energyIn, 2);            % width of the XAS range
%         tmp = widthZ/widthX;     
%         
% %         tmpPara = selMapSet.para
%         
%         if tmp == round(tmp)            % check for integer
%             noOfMaps = tmp;                         % number of submaps in the Multimap
%             debugMsg( ['Multimap: ' num2str(noOfMaps) ' submaps assumed based on width.' ], 1 )
%         end    
%         clear tmp;
%         % -------------------------------------------------------
%         if hObject.Value == 1
% 
%             set(findobj('Tag','sl_selMultimap'),...
%                     'enable','on',...
%                     'max', noOfMaps,...
%                     'SliderStep',  [1/(noOfMaps-1) , 1/(noOfMaps-1) ],... 
%                     'Value',1; 
%             
% %             handles.sl_selMultimap = uicontrol(hCtrlPan,...
% %                     'Style','slider',...
% %                     'min', 1,...
% %                     'max', noOfMaps,...
% %                     'SliderStep',  [1/(noOfMaps-1) , 1/(noOfMaps-1) ],... 
% %                     'Units','Pixel',...
% %                     'Tag','sl_selMultimap',...
% %                     'Position',[390 120 120 20],...
% %                     'FontSize',12,...
% %                     'String',['Multimap selector' ],...
% %                     'Value',1,...
% %                     'CallBack',{@sl_selMultimap_Callback, handles} ); 
%          
%             handles.txt_MMno = uicontrol(hCtrlPan,...
%                     'Style', 'text',...
%                     'Position',[520 123 80 20],...
%                     'String',['GS: '  num2str(1) ' of ' num2str(noOfMaps) ],...             
%                     'FontSize',13,...
%                     'HorizontalAlignment','left',...
%                     'Tag',['txt_MMno']); 
% 
%             % inialise the JOIN maps cb 
%             tagEnable('cb_joinMaps')
%                             
% %         else
% %             oldSL = findobj('Tag',['sl_selMultimap']);
% %             delete(oldSL)
% %             oldSL = findobj('Tag',['txt_MMno']);
% %             delete(oldSL)            
%         end
%         % -------------------------------------------------------
% %         handles = updateViewer( handles );
%         % -------------------------------------------------------
%         % Update handles structure
%         guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
%         % -------------------------------------------------------
%         
% 
%     end   
  

