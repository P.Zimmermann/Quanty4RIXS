function sl_ELDvalMax_Callback(  hObject, eventdata, handles)


    % --------------------------------------------
    debugMsg( 'calling sl_ELDvalMax_Callback()', 2 )
    % --------------------------------------------  

    maxGS = getValTag('sl_ELDvalMax');
    if ~isinteger(maxGS)                    % set to integer is not, otherwise plotEnergyes() will complain about non integer array
        setValTag('sl_ELDvalMax',round(maxGS) )
    end    
    % --------------------------------------------
    
    setStrTag('txt_ELDmaxVal',[ '1 to ' num2str(maxGS) ])
    
    % --------------------------------------------
    % directly plot/update ploits on click
%   handles = updateViewer( handles );
    % --------------------------------------------


        
end

