function sl_selMultimap_Callback( hObject, eventdata, handles )

    % -------------------------------------------------------
    debugMsg( 'calling sl_selMultimap_Callback()', 2 )
    % -------------------------------------------------------

        
        % -------------------------------------------------------
        % INT check and update txt (selecte submap)
        a = round(getValTag('sl_selMultimap'));
        setValTag('sl_selMultimap',a)
        % -------------------------------------------------------        
        b = get(findobj('Tag','sl_selMultimap'),'Max');
        % -------------------------------------------------------
        
        if getValTag('cb_splitMM') == 1
%             setStrTag('txt_MMno',[ num2str(a) '/' num2str(b) ])
            setStrTag('txt_MMno',[ num2str(a) ' of ' num2str(b) ]);   
        elseif getValTag('cb_joinMaps') == 1
            setStrTag('txt_MMno',[ num2str(1) ' to ' num2str(getValTag('sl_selMultimap')) ]);   
        else
            setStrTag('txt_MMno',[ num2str(getValTag('sl_selMultimap')) ]);               
        end
        
        % -------------------------------------------------------
%         if (getValTag('cb_splitMM') == 1) || (getValTag('cb_joinMaps') == 1)
%             handles = updateViewer( handles );
%         end    
        % -------------------------------------------------------
        % Update handles structure
        guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
        % -------------------------------------------------------
  




end

