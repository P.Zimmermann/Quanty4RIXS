function [ output_args ] = pm_cetSelect_Callback( hObject, eventdata, hax3D, haxCET )


    % --------------------------------------------
    debugMsg( 'calling pm_cetSelect_Callback()', 0 )
    % --------------------------------------------           
    % hObject == indobj('Tag','pm_updatePlots')
    hObject
    eventdata
    hax3D

    % --------------------------------------------
	selValue = hObject.Value - 1;        % -1 because of top element empty
% 	pmS = hObject.String
    
    % get stored plot data from axes
    pltData = get(hax3D,'UserData');   
    xData = pltData{1};
    yData = pltData{2};
    zData = pltData{3};

%     sz_X = size(xData)
%     sz_Y = size(yData)
%     sz_Z = size(zData)

    sliceData.xData      = xData(1,:)
    sliceData.yData      = zData(selValue,:)
    sliceData.sliceValue = yData(selValue);
    
       
    % --------------------------------------------
     
    disp([' Sel ET : ' num2str( yData(selValue) ) ])    

    % --------------------------------------------
    % make CIE slice plot
    axes(haxCET)
    set(haxCET,'UserData',sliceData)            % Store Plot data in UserData for easy cut of slices
    
    plot(sliceData.xData, sliceData.yData)
    title('CET slice')
    xlim([min(sliceData.xData) max(sliceData.xData)])
    ylim([min(sliceData.yData) max(sliceData.yData)])
    % --------------------------------------------
    % mark slice with Line in map
    axes(hax3D)
    delete(findobj('Parent',hax3D,'Tag','selCET'))
    hline = line([min(sliceData.xData) max(sliceData.xData)],[sliceData.sliceValue sliceData.sliceValue],...
                   'Parent',hax3D,...
                    'LineWidth',1,...
                   'Tag','selCET',...
                   'Color',[.8 .8 .8]);
                               
    % --------------------------------------------
    guidata(hObject)

    % --------------------------------------------
    


end

