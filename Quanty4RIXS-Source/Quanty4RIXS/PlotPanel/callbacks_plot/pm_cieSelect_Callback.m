function [ output_args ] = pm_cieSelect_Callback( hObject, eventdata, hax3D, haxCIE )


    % --------------------------------------------
    debugMsg( 'calling pm_cieSelect_Callback()', 0 )
    % --------------------------------------------           
    % hObject == indobj('Tag','pm_updatePlots')
%     hObject
%     eventdata
%     hax3D

    % --------------------------------------------
	selValue = hObject.Value - 1;
% 	pmS = hObject.String
    

    pltData = get(hax3D,'UserData')
    
    xData = pltData{1};
    yData = pltData{2};
    zData = pltData{3};

    
    sz_X = size(xData)
    sz_Y = size(yData)
    sz_Z = size(zData)
    % --------------------------------------------

    disp([' Sel IE :' num2str( xData(selValue) ) ])
     
    sliceData.xData      = yData(:,1);
    sliceData.yData      = zData(:,selValue);
    sliceData.sliceValue = xData(selValue);
    
    % --------------------------------------------

    axes(haxCIE)
    set(haxCIE,'UserData',{sliceData})            % Store Plot data in UserData for easy cut of slices
    
    plot(sliceData.xData, sliceData.yData)
    title('CIE slice')    
    xlim([min(sliceData.xData) max(sliceData.xData)])
    ylim([min(sliceData.yData) max(sliceData.yData)])     
    
    % --------------------------------------------
    % mark slice with Line in map
    axes(hax3D)
    delete(findobj('Parent',hax3D,'Tag','selCIE_line'))
    hline = line([sliceData.sliceValue sliceData.sliceValue],[min(sliceData.xData) max(sliceData.xData)],...
                   'Parent',hax3D,...
                   'LineWidth',1,...
                   'Tag','selCIE_line',...
                   'Color',[.8 .8 .8]);
                               
    % --------------------------------------------
    guidata(hObject,haxCIE);

    % --------------------------------------------



end

