function [ output_args ] = pb_saveSlices_Callback( hObject, eventdata, hax3D,  pm_cetSliceSel, pm_cieSliceSel, mapFolder )


    % --------------------------------------------
    debugMsg( 'calling pb_saveSlices_Callback()', 0 )
    % --------------------------------------------           
    % hObject == indobj('Tag','pm_updatePlots')
%     hObject
%     eventdata
%     hax3D
    % -------------------------------------------------------
    matFolder = [mapFolder 'mat' filesep];
    if not(exist(matFolder) == 7)            % 7 = folder exists 
        mkdir(matFolder)
    end  
    % --------------------------------------------
% 	sbV = hObject.Value
%  	pbS = hObject.String
    

    % --------------------------------------------
    mapTitle = get(hax3D,'Title');    
    mapTitle = mapTitle.String;
    if length(mapTitle) > 2
        mapStr = mapTitle(end-2:end)
    else
        mapStr = mapTitle        
    end    
    % --------------------------------------------
    mapData = get(hax3D,'UserData')
    
    mapDataX = mapData{1};
    mapDataY = mapData{2};
    mapDataZ = mapData{3};
    
    % --------------------------------------------        
    cetSel = get(pm_cetSliceSel,'Value');
    if(cetSel > 1)
%         xData = mapDataX(:,1);
        xData = mapDataX;
        yData = mapDataZ(cetSel-1,:);

        fullNameCET = [matFolder mapStr '_CET_' num2str(mapDataY(cetSel-1,1)) '_Slice.mat' ];     
        disp(['Saving CET as: ' fullNameCET])        
        save(fullNameCET,'xData','yData')
    end
    
    % --------------------------------------------
    cieSel = get(pm_cieSliceSel,'Value');
    if(cieSel > 1)    
%         xData = mapDataY(1,:);
        xData = mapDataY;
        yData = mapDataZ(:,cieSel-1);
   
        
        fullNameCIE = [matFolder mapStr '_CIE_' num2str(mapDataX(1,cieSel-1)) '_Slice.mat' ];  
        disp(['Saving CIE as: ' fullNameCIE])
        save(fullNameCIE,'xData','yData')
    end
    
    % --------------------------------------------



end

