function saveFigure2Image( hViewFig, mapFolder, imgFName )


    % -------------------------------------------------------
    debugMsg( 'calling saveFigure2Image()', 1 )
    % -------------------------------------------------------
    imgFolder = [mapFolder 'img' filesep];
    if not(exist(imgFolder) == 7)            % 7 = folder exists 
        mkdir(imgFolder)
    end    
    imgSaveFName = [imgFolder imgFName];
    
    % ------------------------------------------------------------------------------------------------------    
    imgSaveFName = char([imgSaveFName '.png']);
    % ------------------------------------------------------------------------------------------------------    
    set(hViewFig,'Units','pixel');    
    figPos = get(hViewFig,'Position');
    
    set(hViewFig,'PaperUnits','points','PaperSize',[figPos(3) figPos(4)],'PaperPosition',[0 0 figPos(3) figPos(4)])
    % ------------------------------------------------
    disp([' Saving Image as: ' imgSaveFName ])
    % ------------------------------------------------

    % ------------------------------------------------------------------------------------------------------       
    print(hViewFig,imgSaveFName,'-dpng','-r200')                % SAVE IMAGE TO FILE
    % ------------------------------------------------------------------------------------------------------       

    % ------------------------------------------------------------------------------------------------------       
    debugMsg([' Plot saved to file: ' imgSaveFName],1)
    % ------------------------------------------------------------------------------------------------------       


end

