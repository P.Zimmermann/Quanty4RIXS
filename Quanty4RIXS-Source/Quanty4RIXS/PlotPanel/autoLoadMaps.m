function handles = autoLoadMaps( handles, mapSaveDir, mapBaseName )
%AUTOLOADMAPS Summary of this function goes here
%   Detailed explanation goes here

    debugMsg( 'calling autoLoadMaps()', 2 )

%     mapSaveDir 
%     mapBaseName
    
%     ctm4rixsViewer(mapSaveDir, mapBaseName )   
%     MapViewer(mapSaveDir, mapBaseName )   
   
    
%     handles.mapSaveDir  = char(mapSaveDir);
%     handles.mapBaseName = char(mapBaseName);    
%     
    % --------------------------------------------------------------------------------
    % --------------------------------------------------------------------------------
    % autoloading maps if during init mapSaveDir and mapBaseName is already defines (see main function)
%     if isfield(handles,'mapSaveDir') && isfield(handles,'mapBaseName')

    if getValTag('cb_autoplot')
        % ------------------------------------------------------------------
        handles.appSettings.lastDir = mapSaveDir;             
        % ------------------------------------------------------------------
        obsFiles = getObservableFNames( mapSaveDir, mapBaseName )
        % load Observable file into a new map class and store map data in backend
        if length(obsFiles) > 0
            handles.hBackend.importMapDirData( mapSaveDir, obsFiles)
        end
        % ------------------------------------------------------------------       
        
        % ------------------------------------------------------------------
        eldFiles = getELDiagramFNames( mapSaveDir, mapBaseName )
        % load EnergyLEvelDiagram file into a new map class and store map data in backend
        if length(eldFiles) > 0
            handles.hBackend.importMapDirData( mapSaveDir, eldFiles)
        end
        % ------------------------------------------------------------------       
        
        
        % ------------------------------------------------------------------
        mapFiles = getQuantyMapFNames( mapSaveDir, mapBaseName )           % get MAP files from mapDir if they exist                      
        % load Quanty map into a new map class and store map data in backend
        if length(mapFiles) > 0
            handles.hBackend.importMapDirData( mapSaveDir, mapFiles)
%             newMap = classMapSet(mapFiles);           % new class directly load mapdata
% %             newMap = classMapSet() 
% %             newMap.importQuantyMaps(mapFiles);
%             handles.hBackend.addMap(newMap)
%             % handles = loadQuantyMapSet(handles, mapFiles );
        end
        % ------------------------------------------------------------------
 
        % ------------------------------------------------------------------
        xasFiles = getXASFNames( mapSaveDir, mapBaseName );
        % load EnergyLEvelDiagram file into a new map class and store map data in backend
        if length(xasFiles) > 0
            handles.hBackend.importMapDirData( mapSaveDir, xasFiles)
        end
        % ------------------------------------------------------------------       

        
        
        
    end    
    % --------------------------------------------------------------------------------    
    % updating control panel with maps data 
    handles = updateControl( handles );           
    % --------------------------------------------------------------------------------    
    % selecting last entry in popupmenu (last is the last added one)
    setValTag({'pm_mapSet'},length(getStrTag({'pm_mapSet'})))                  % selecting last entry the pop-up menu
    
    pm_mapSet_Callback( findobj('Tag','pm_mapSet'), 0, handles)         % launch callback to auload last mapset
    % -------------------------------------------------------------------------------- 
    if getValTag({'cb_Plotting'}) == 0
        setValTag({'cb_Plotting'},1)
        showPanelPlotting();
    end
    % --------------------------------------------------------------------------------
       




end

