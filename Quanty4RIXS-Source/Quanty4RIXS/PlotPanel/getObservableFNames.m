function obsFiles = getObservableFNames( mapSaveDir, mapBaseName )

    % -------------------------------------------------------
    debugMsg( 'calling getObservableFNames()', 0 )
    % -------------------------------------------------------
    
    obsFiles = {};

    % look in the given path for all   mapBaseName*.txt files 
%     fullMapsPath = [mapSaveDir filesep mapBaseName '*Spec.txt'];        % we use this as extension for Quanty maps caluclated with this program
    fullMapsPath = [mapSaveDir mapBaseName '*OPvals.txt'];        % we use this as extension for Quanty maps caluclated with this program
    mapDirList = dir(fullMapsPath);

    if isfield(mapDirList,'name') && length(mapDirList) > 0
    
        obsFiles = mapDirList.name;
        
    end
        
%     if length(mapDirList) > 0
%         for m=1:length(mapDirList)
%             
%             thisFName = mapDirList(m).name
%             % here the map file name is written to the mapfiles cell list
%             mapFiles{m} = [mapSaveDir filesep thisFName];
%     
%         end
%     end




end

