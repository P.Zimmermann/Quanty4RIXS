function xasData = readQuantyXASSpec( quantyXASFile, para )

    % --------------------------------------------------
%     debugMsg( ['calling readQuantyRIXSmap()' quantyXASFile ], 1 )
    % --------------------------------------------------
    % storing the parameter in the final data struct xasData
    xasData.para = para;

    quantyXASFile
    
    % --------------------------------------------------
    % --------------------------------------------------
    % open File for reading
    fid = fopen(quantyXASFile);
    % --------------------------------------------------     
    % --------------------------------------------------     
    
    % --------------------------------------------------     
    % reading first few lines indivdually to extract header info  
    % (stops at "Energy" tag in the file)
    stopFlag = 1;
    while stopFlag       
        thisline = fgets(fid);                              % read line by line          
        thisline = strsplit(deblank(thisline));             % remove leading or trailing spaces and split line     
        
        if length(thisline) > 0
            if strcmp(thisline(1),'#Spectra:')
%                 thisline
                noOfSpec = str2num(thisline{2})            % extracted number 3 from "#Spectra: 3"
            elseif strfind(char(thisline(1)),'Emin_')    
                energy2_Min = str2num(thisline{2});         % extracted Emin : is identical to first column, energy1_Column below
                energy2_Max = str2num(thisline{3});         % extracted Emax : is identical to first column, energy1_Column below
            elseif strfind(char(thisline(1)),'dE_')    
                energy2_dE = str2num(thisline{2});          % extracted dE
                energy2_Gamma = str2num(thisline{3});       % extracted Gamma (natural broadening) 
            elseif strcmp(thisline(1),'Energy')
               stopFlag = 0;                            % Abort condition, nest line starts with data
            end   
        else
            stopFlag = 0;                               % Abort condition when line is empty . (ToDo Check: This should only happen at the end)
        end
    end
    % Head completed
    % --------------------------------------------------   
    % READ map Data     
    formatString = ['%f64 ' repmat('%n ',1,2*noOfSpec)];      % Create format string based on the number of columns, e.g. ('%f %f %f %f %f %f %f \n') 
    quantyMapRAW = textscan(fid,formatString);                % Read data block
    % --------------------------------------------------        
    fclose(fid);                            % close (text) file reader
    % --------------------------------------------------
%     size(quantyMapRAW)
%     quantyMapRAW{1}
    % --------------------------------------------------
    % convert raw mapdata into a matrix (still with Re and Im parts as alternatig columns)
    quantyMap = cell2mat(quantyMapRAW);
    
    
    qSize = size(quantyMap)
    % --------------------------------------------------    
%     % FIRST COLUMN is emission energy
%     energyOut = quantyMap(:,1);                % Same as:   [energy2_Min:energy2_dE:energy2_Max];             % created the energy Range from Emin, Emax and dE
%     energyIn_Row = [1:noOfSpec];                                     % created the energy Range als lin space. (needs to be updated after from a config file.)    
%     map_Ein = repmat(energyIn_Row,length(energyOut),1);           % extending the ROW of energies into a matrix/map
%     % --------------------------------------------------
    % --------------------------------------------------
    
    
    

    % --------------------------------------------------
    % preaparing data to be returned =>  mapData
    % seperating Re, Im columns into seperate maps, and create also the Energy map based on column 1 (column cloning). 
%     map_Eout = [];
    map_Ein = quantyMap(:,1);
    map_Re = [];
    map_Im = [];
    for k=1:noOfSpec
%         map_Eout = [map_Eout energyOut];
%         header_Re{k} = char( headers(1+(2*k-1)) );                % Re[xy]
%         header_Im{k} = char( headers(1+(2*k)) );                  % Im[]
        map_Re = [map_Re quantyMap(:,1+(2*k-1) )];
        map_Im = [map_Im quantyMap(:,1+(2*k) )];       
    end
    % --------------------------------------------------


    % --------------------------------------------------
%     map_Eout = map_Eout(:,1);       % this is correct! Row wector with N columns
%     map_Ein  = map_Ein(1,:);        % correct! Columns vecotr with M rows

%     % --------------------------------------------------
%     % --------------------------------------------------
%     debugMsg([' !!! Incident Energy (E2) has Size   : ' num2str( size(map_Ein)  ) ],0)
% %     debugMsg([' !!! Emission Energy (E1) has Size   : ' num2str( size(map_Eout) ) ],0)
%     debugMsg([' !!! RIXS Map Data has Size          : ' num2str( size(map_Im)   ) ],0)
%     ieRatio = (size(map_Im,2)/size(map_Ein,2) );
%     if (ieRatio > 1) && (ieRatio == round(ieRatio))
%         debugMsg([' MULTIMAP with  ' num2str( ieRatio ) ' ground states in one map.' ],0)
%     end
%     % creating datastructure in mapData to be saved in handles structure and for saving as .mat file
    xasData.energyIn  = map_Ein;
    xasData.energyOut = [];
    xasData.map_Re = -map_Re;
    xasData.map_Im = -map_Im;
    % --------------------------------------------------
%     disp(['Result of mapData at the end of readQuantyRIXSmap()'])
%     mapData
    % --------------------------------------------------
    % --------------------------------------------------
    
    
    
    
    
    
    
end

