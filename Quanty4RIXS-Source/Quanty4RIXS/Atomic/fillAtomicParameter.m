function handles = fillAtomicParameter(configStrGS, configStrIS, configStrFS, handles)
%FILLATOMICPARAMETER Summary of this function goes here
%   Detailed explanation goes here

    % ----------------------------------------------------------------------
    debugMsg( 'calling fillAtomicParameter()', 2 )
    % ----------------------------------------------------------------------
    
    handles.configStrGS = configStrGS;
    handles.configStrIS = configStrIS;
    handles.configStrFS = configStrFS;

    % --------------------------------------------------  
    clearAtomicParameter( )
    % --------------------------------------------------  

    
    fillState(configStrGS,'GS');
    fillState(configStrIS,'IS');
    fillState(configStrFS,'FS');
    

    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
    function fillState(configStr,stateStr)
    
        
        if length(configStr) > 4

            strCore = configStr{2}(1:2);  
            strVal  = configStr{3}(1:2);

            for i=5:length(configStr)

                thisCell = char(configStr{i});
                
                thisCell = deblank(thisCell);                       % just remove leading or trailing spaces        
                thisCellParts = strsplit(thisCell,{'=','eV'});      % split cellData at '=' and 'eV'

                thisCellLabel =   char(thisCellParts(1));
                thisCellValue =   num2str(str2num(char(thisCellParts(2))),'%2.3f');


                if length(thisCell) >= 9
                    % ------------------------------------------------------------------------                    
                    % core - valence interactions
                    if (strcmp(thisCell(4:5),strCore) && strcmp(thisCell(6:7),strVal))
                        if strcmp(thisCell(1:2),'F0')
                            setStrTag({['txt_F0cv_' stateStr]},thisCellLabel )
                            setStrTag({['txtVal_F0cv_' stateStr]},[thisCellValue ' eV'])
                        elseif strcmp(thisCell(1:2),'F2')
                            setStrTag({['txt_F2cv_' stateStr]},thisCellLabel )
                            setStrTag({['txtVal_F2cv_' stateStr]},[thisCellValue ' eV'])
                        elseif ( strcmp(thisCell(1:2),'G0') || strcmp(thisCell(1:2),'G1') )
                            setStrTag({['txt_G0cv_' stateStr]},thisCellLabel )
                            setStrTag({['txtVal_G0cv_' stateStr]},[thisCellValue ' eV'])
                        elseif ( strcmp(thisCell(1:2),'G2') || strcmp(thisCell(1:2),'G3') )
                            setStrTag({['txt_G2cv_' stateStr]},thisCellLabel )
                            setStrTag({['txtVal_G2cv_' stateStr]},[thisCellValue ' eV'])
                        end                            
                    % valence - valence interactions
                    elseif (strcmp(thisCell(4:5),strVal) && strcmp(thisCell(6:7),strVal))
                        if strcmp(thisCell(1:2),'F0')
                            setStrTag({['txt_F0vv_' stateStr]},thisCellLabel )                        
                            setStrTag({['txtVal_F0vv_' stateStr]},[thisCellValue ' eV'])
                        elseif strcmp(thisCell(1:2),'F2')
                            setStrTag({['txt_F2vv_' stateStr]},thisCellLabel )                        
                            setStrTag({['txtVal_F2vv_' stateStr]},[thisCellValue ' eV'])
                        elseif strcmp(thisCell(1:2),'F4')
                            setStrTag({['txt_F4vv_' stateStr]},thisCellLabel )                        
                            setStrTag({['txtVal_F4vv_' stateStr]},[thisCellValue ' eV'])
                        end  
                    elseif (strcmp(thisCell(1:3),'SOC') && strcmp(thisCell(5:6),strCore))    
                        setStrTag({['txt_SOCc_' stateStr]},thisCellLabel )                        
                        setStrTag({['txtVal_SOCc_' stateStr]},[thisCellValue ' eV'])
                    elseif (strcmp(thisCell(1:3),'SOC') && strcmp(thisCell(5:6),strVal))    
                        setStrTag({['txt_SOCv_' stateStr]},thisCellLabel )                        
                        setStrTag({['txtVal_SOCv_' stateStr]},[thisCellValue ' eV'])                        
                    end
                    % ------------------------------------------------------------------------


                end
            end
            
            clear strCore strVal
            
            
        end


    
    end

    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
        

end

