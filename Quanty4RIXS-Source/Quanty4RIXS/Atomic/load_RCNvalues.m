function handles = load_RCNvalues(handles)

    % --------------------------------------------
    debugMsg( 'calling load_RCNvalues()', 2 )
    % --------------------------------------------


    rcnParamFName = [handles.appRootDir filesep 'Resources' filesep 'RCNparameter.txt'];
    debugMsg(['Loading Atomic RCN parameter from ' rcnParamFName],0)
        
    fid1 = fopen(rcnParamFName,'r');
    
    rcnLines = {};
    lineIndex = 0;
    while ~feof(fid1)
        lineIndex = lineIndex+1;                 % counting lines as index
        lineString = fgets(fid1);                % read line by line
        % ------------------------------------------------------------
        lineString = deblank(lineString);           % just remove leading or trailing spaces        
%         lineString = strtrim(lineString);           % just remove leading or trailing spaces
        splittedLine = strsplit(lineString,' ');
        % ------------------------------------------------------------
        % writing all lines also into a cellarray of lines (for later use)
        rcnLines{lineIndex} = splittedLine;
        % ------------------------------------------------------------
      
    end
    fclose(fid1);
    % ------------------------------------------------------------
    % ------------------------------------------------------------
    handles.rcnDataLines = rcnLines;

%     size(rcnLines)    str
%     rcnLines{1}


    % ------------------------------------------------------------
    % ------------------------------------------------------------
    clear rcnLines lineIndex fid1 lineString rcnParamFName
    % ------------------------------------------------------------



end

