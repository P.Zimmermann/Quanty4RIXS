function [ output_args ] = hidePanelAtomic(  )

    % ============================================================================================================    
    debugMsg( 'calling hidePanelAtomic()', 2 )
    % ----------------------------------------------------------------------    
    % check if cb value is correct (show function is also called internally)
    if getValTag({'cb_expertAtomic'}) == 1
        setValTag({'cb_expertAtomic'},0)
    end
    % ----------------------------------------------------------------------    
       
    % ============================================================================================================    
    dY_CalcPanel = 305;
    shiftPanels = {'uiPan_Settings','uiPan_Configuration'};   
    hideControl({'uiPan_Atomic'})
     % ----------------------------------------------------------------------
    for k=1:length(shiftPanels)
        % --- shifting the other panels up to make room for the Atomic Panel ---
        atomPos = get(findobj('Tag',shiftPanels{k}),'Position');
        newParaPos = [atomPos(1) atomPos(2)-dY_CalcPanel atomPos(3) atomPos(4)]; 
        set(findobj('Tag',shiftPanels{k}),'Position',newParaPos)
    end
    % ----------------------------------------------------------------------
    % reduce size of main figure window
    figPos = get(findobj('Tag','mainFigure'),'Position');

    newParaPos = [figPos(1) figPos(2)+dY_CalcPanel figPos(3) figPos(4)-dY_CalcPanel]; 

    set(findobj('Tag','mainFigure'),'Position',newParaPos)    

% ============================================================================================================    

    % ----------------------------------------------------------------------
%     debugMsg(['  New position of the calculate panel : ' mat2str(calcPos) ] ,2)
%     debugMsg(['  New position of the parameter panel : ' mat2str(newParaPos) ] ,2)
    % ----------------------------------------------------------------------
    % ----------------------------------------------------------------------

   % ============================================================================================================    

    
    clear dY_CalcPanel shiftPanels

end

