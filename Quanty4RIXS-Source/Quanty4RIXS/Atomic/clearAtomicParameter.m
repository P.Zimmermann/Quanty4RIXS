function [ output_args ] = clearAtomicParameter( )
%CLEARATOMICPARAMETER Summary of this function goes here
%   Detailed explanation goes here


    % ----------------------------------------------------------------------
    debugMsg( 'calling clearAtomicParameter()', 2 )
    % ----------------------------------------------------------------------
   
    % --------------------------------------------------
    % reset all GS values
    resetState('GS')
    % reset all IS values
    resetState('IS')
    % reset all FS values
    resetState('FS')
    % --------------------------------------------------
    
    
    % --------------------------------------------------
    % --------------------------------------------------
    function resetState(stateStr)        
        zeroStr       = '0.000 eV';
        defaultHFStr  = '1.00';
        oneStr        = '1.00';
        % ----------------------------
        setStrTag({['txt_F0cv_' stateStr]},'F0cv')
        setStrTag({['txt_F2cv_' stateStr]},'F2cv')
        setStrTag({['txt_G0cv_' stateStr]},'G0cv')
        setStrTag({['txt_G2cv_' stateStr]},'G2cv')
        setStrTag({['txt_F0vv_' stateStr]},'F0vv')
        setStrTag({['txt_F2vv_' stateStr]},'F2vv')
        setStrTag({['txt_F4vv_' stateStr]},'F4vv')

        setStrTag({['txtVal_F0cv_' stateStr]},zeroStr)
        setStrTag({['txtVal_F2cv_' stateStr]},zeroStr)
        setStrTag({['txtVal_G0cv_' stateStr]},zeroStr)
        setStrTag({['txtVal_G2cv_' stateStr]},zeroStr)
        setStrTag({['txtVal_F0vv_' stateStr]},zeroStr)
        setStrTag({['txtVal_F2vv_' stateStr]},zeroStr)
        setStrTag({['txtVal_F4vv_' stateStr]},zeroStr)  
        
        setStrTag({['edit_F0cv_' stateStr]},defaultHFStr)
        setStrTag({['edit_F2cv_' stateStr]},defaultHFStr)
        setStrTag({['edit_G0cv_' stateStr]},defaultHFStr)
        setStrTag({['edit_G2cv_' stateStr]},defaultHFStr)
        setStrTag({['edit_F0vv_' stateStr]},defaultHFStr)
        setStrTag({['edit_F2vv_' stateStr]},defaultHFStr)
        setStrTag({['edit_F4vv_' stateStr]},defaultHFStr)          

        
        setStrTag({['txt_SOCc_' stateStr]},'SOCc')
        setStrTag({['txt_SOCv_' stateStr]},'SOCv')

        setStrTag({['txtVal_SOCc_' stateStr]},zeroStr)
        setStrTag({['txtVal_SOCv_' stateStr]},zeroStr)
        
        setStrTag({['edit_SOCc_' stateStr]},oneStr)
        setStrTag({['edit_SOCv_' stateStr]},oneStr)                       
    end    
    % --------------------------------------------------
    % --------------------------------------------------

end

