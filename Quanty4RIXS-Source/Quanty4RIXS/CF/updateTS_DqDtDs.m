function updateTS_DqDtDs(  )
%UPDATETS_DQDTDS Summary of this function goes here
%   Detailed explanation goes here


    Dq_ground = str2num(getStrTag('edit_10Dq_GS'))/10;
    Ds_ground = str2num(getStrTag('edit_Ds_GS'));
    Dt_ground = str2num(getStrTag('edit_Dt_GS'));


    % ------------------------------------------------  
    % TS diagram for 3d
    axes(findobj('Tag','ax_TS_DqDsDt'));
    cla;
    plotDqDtDs( Dq_ground, Ds_ground, Dt_ground );
    % ------------------------------------------------  
    



end

