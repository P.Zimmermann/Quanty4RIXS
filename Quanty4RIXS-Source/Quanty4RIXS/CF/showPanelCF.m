function [ output_args ] = showPanelCF( inTag )
%SHOWPARAPANEL Summary of this function goes here
%   Detailed explanation goes here


    % ============================================================================================================    
    debugMsg( 'calling showPanelCF()', 2 )
    
    % ----------------------------------------------------------------------    
    % check if cb value is correct (show function is also called internally)
    if getValTag({'cb_expertCF'}) == 0
        setValTag({'cb_expertCF'},1)
    end
    % ----------------------------------------------------------------------    

    
    shiftPanels = {'uiPan_Settings','uiPan_Configuration','uiPan_Atomic'};            
    dY_CalcPanel = 195;
    % show atomic parameter panel
    showControl({'uiPan_CF'})     

    % ----------------------------------------------------------------------
    % --- shifting the other panels downto make room for the Atomic Panel ---
%     shiftPanels = {'uiPan_crystalField','uiPan_Broadening','uipan_status'};
%     shiftPanels = {'uiPan_Configuration'};
    for k=1:length(shiftPanels)
        atomPos = get(findobj('Tag',shiftPanels{k}),'Position');
        newParaPos = [atomPos(1) atomPos(2)+dY_CalcPanel atomPos(3) atomPos(4)]; 
        set(findobj('Tag',shiftPanels{k}),'Position',newParaPos)
    end
    % ----------------------------------------------------------------------
    % increase size of main figure window
    figPos = get(findobj('Tag','mainFigure'),'Position');
    newParaPos = [figPos(1) figPos(2)-dY_CalcPanel figPos(3) figPos(4)+dY_CalcPanel];     
    set(findobj('Tag','mainFigure'),'Position',newParaPos)    
    % ----------------------------------------------------------------------

    % ============================================================================================================    
        

    clear dY_CalcPanel shiftPanels


end

