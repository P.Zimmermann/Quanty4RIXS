function [ output_args ] = debugMsg( debugString, level )
%DEBUGMSG Summary of this function goes here
%   This funciton is used to simplify the output of debug Messages 
%   outpout of debug messages can be enabled and disabled via the global variable debugSwitch
    

    % usage:  debugMsg('calcType RIXS (On)', 1)

       
%     if getValTag({'debugLevel'}) >= level
    if (getValTag({'pm_DebugLevel'}) - 1) >= level
        % disp(['#DebugMsg ' num2str(debugLevel) ' >= ' num2str(level) ' : ' debugString])
        disp(['#DebugMsg ' num2str(level) ' : ' debugString])
    end


end

