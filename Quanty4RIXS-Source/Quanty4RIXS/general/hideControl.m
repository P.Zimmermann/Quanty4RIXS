function hideControl(inTags)
%> @file hideControl.m
%> hideControl() changes the Visible properties to OFF for all given Tags

    for jj=1:length(inTags),
        set(findobj('Tag',inTags{jj}),'Visible','off');
    end
end