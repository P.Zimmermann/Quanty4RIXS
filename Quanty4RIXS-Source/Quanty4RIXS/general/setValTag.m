function [ output_args ] = setValTag( inTags, inVal )
%SETVALTAG Summary of this function goes here
%   Detailed explanation goes here

    if iscell(inTags)
        for n=1:length(inTags)
           set(findobj('Tag',inTags{n}),'Value',inVal);    
        end          
    else
        set(findobj('Tag',inTags),'Value',inVal);
    end
    
end

