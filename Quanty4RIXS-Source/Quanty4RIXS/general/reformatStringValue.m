function reformatStringValue( hObject,handles, digits )
%REFORMATSTRINGVALUE Summary of this function goes here
%   Detailed explanation goes here


    % ----------------------------------------------------------------------
    debugMsg( 'calling reformatStringValue()', 2 )
    % ----------------------------------------------------------------------
    
    % ------------------------------------------------------
    thisTag = hObject.Tag;                        % e.g.:  'edit_F0cv_GS'    
    
    % ----------------------------------------------------------------------
    debugMsg( ['Reformatting Tag: ' thisTag], 1 )
    % ----------------------------------------------------------------------

    
    % reformatting to 2 digits
    setStrTag(thisTag,num2str(str2num(getStrTag(thisTag)) , strcat('%1.', num2str(digits), 'f') ))    
    % ------------------------------------------------------

    % ------------------------------------------------------
    % Update handles structure
    guidata(hObject, handles);
    % ------------------------------------------------------


end

