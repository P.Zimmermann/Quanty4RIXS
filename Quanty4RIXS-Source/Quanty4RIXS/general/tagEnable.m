function [ output_args ] = tagEnable( inTags )
% enables ui items by tag
% inTags shall be a cell array with tags

    if iscell(inTags)
        for n=1:length(inTags)
           set(findobj('Tag',inTags{n}),'Enable','on')    
        end    
    else    
        set(findobj('Tag',inTags),'Enable','on')  
    end  


end

