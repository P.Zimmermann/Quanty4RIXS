function [ outVal ] = getValTag( inTag )
%GETVALTAG Summary of this function goes here
%   Detailed explanation goes here
   
   if iscell(inTag)
        for n=1:length(inTag)
           outVal{n} = get(findobj('Tag',inTag{n}),'Value');
        end        
        if length(outVal) == 1
           outVal = outVal{1}; 
        end    
    else
        outVal = get(findobj('Tag',inTag),'Value');
    end      
    
    
    
end

