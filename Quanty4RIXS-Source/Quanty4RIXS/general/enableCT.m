function [ output_args ] = enableCT( input_args )
%ENABLECT Summary of this function goes here
%   Detailed explanation goes here


    % enable all CT edit boxes
    tagEnable({'edit_CT_Delta','edit_CT_Udd','edit_CT_Upd','edit_CT_Tb1','edit_CT_Ta1','edit_CT_Tb2','edit_CT_Te'})
    tagEnable({'uitxt_CT_Delta','uitxt_CT_Udd','uitxt_CT_Upd','uitxt_CT_Tb1','uitxt_CT_Ta1','uitxt_CT_Tb2','uitxt_CT_Te'})


end

