function [ output_args ] = scaleValue( hObject, handles )
%SCALEVALUE Summary of this function goes here
%   Detailed explanation goes here


    debugMsg( 'calling scaleValue()', 2 )

%     handles    
    
    thisStr = hObject.String;
    thisTag = hObject.Tag;
    thisValue = str2num(thisStr);

    debugMsg( ['Scaling ' thisTag ' with ' thisStr] , 1 )
    
    
    writeTag = ['txtVal_' thisTag(6:end)];              % this should enable for common CALLback function, labels shoudl be adapted here correctly !??
    labelTag = ['txt_' thisTag(6:end)];  
    
    dataHandle = ['configStr' thisTag(end-1:end)];
    dataCells = handles.(dataHandle);
    
    if length(dataCells) > 4
%             coreStr = char(dataCells{2});
%             valStr  = char(dataCells{3});
    
            for i=5:length(dataCells)             % start with 5, because the first 4 are not ending up at 2 parts after splitting => triggers error below, better anyway cause its faster 
                
                thisCell = dataCells{i};
                thisCellParts = strsplit(thisCell,{'=','eV'}); 
                thisCellLabel =   char(thisCellParts(1));
                thisCellValue =   str2num(char(thisCellParts(2)));
                
                if strcmp(thisCellLabel,getStrTag(labelTag))        % check if label in gui is the same as in data string  e.g. F01s3d (checks label in gui and data string)
                   
                        % ------------------------------------------------------------------------
                        newVal = thisCellValue;
                        % ------------------------------------------------------------------------
                        debugMsg([thisCellLabel ' 100% Value: ' num2str(thisCellValue) ],1)

                        setStrTag({writeTag},[ num2str(thisCellValue * thisValue,'%1.3f') ' eV'] )
                        % ------------------------------------------------------------------------

                end    
                clear thisCell thisCellParts thisCellLabel thisCellValue


            end
            
            
    end

end

