function [ output_args ] = syncEditBox(hObject,handles, tagList )
%SYNCEDITBOX Summary of this function goes here
%   Detailed explanation goes here


    % TODO: check for array needed, NO sync for arrays!!!

    % ------------------------------------------------------------------------------------------------
    debugMsg( 'calling syncEditBox()', 2 )
    % ------------------------------------------------------------------------------------------------
    

    % ----------------------------------------------------
    thisTag = hObject.Tag;
    % thisTag(5:end)

    dataHandle = ['configStr' thisTag(end-1:end)];

    if isfield(handles,dataHandle)   
        % -------------------------------------------------------------
        % sync the edit boxes to with the above edit box
        srcTag = ['edit_F0' tagList{1}(8:end)];
        syncVal = getStrTag(srcTag);
        setStrTag(tagList,num2str(str2num(syncVal),'%1.2f') )           % writing the value FORMATTED to BOTH disabled edit boxes       
        % -------------------------------------------------------------

        % -------------------------------------------------------------
        % TODO: HERE, the txtVal needs to be resetted to new scale too -  What the hell did I mean by that???
        scaleValue( findobj('Tag',tagList{1}), handles )
        scaleValue( findobj('Tag',tagList{2}), handles )
        % -------------------------------------------------------------        
    end
    
    
%     
%     % --------------------------------------------------
%     % --------------------------------------------------
%     % Update handles structure and write it to the GUI
%     guidata(findobj('Tag','mainFigure'), handles);
%     % --------------------------------------------------
%     % --------------------------------------------------
%     
%     


end

