function showControl(inTags)
%> @file showControl.m
%> showControl() accepts a cell list of tags and makes all visible

    debugMsg( 'calling showControl()', 2 )


    for jj=1:length(inTags),
        set(findobj('Tag',inTags{jj}),'Visible','on');
    end