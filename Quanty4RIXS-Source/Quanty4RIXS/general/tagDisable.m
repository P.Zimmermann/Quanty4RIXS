function [ output_args ] = tagDisable( inTags )
% disables ui items by tag
% inTags shall be a cell array with tags

    if iscell(inTags)
        for n=1:length(inTags)
           set(findobj('Tag',inTags{n}),'Enable','off')    
        end
    else
        set(findobj('Tag',inTags),'Enable','off')    
    end  
    
    
end

