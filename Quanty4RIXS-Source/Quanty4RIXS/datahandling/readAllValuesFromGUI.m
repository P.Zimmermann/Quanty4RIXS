function para = readAllValuesFromGUI( handles, strTag  )
%READALLVALUESFROMGUI Summary of this function goes here
%   Detailed explanation goes here

    % --------------------------------------------------
    debugMsg( 'calling readAllValuesFromGUI()', 0 )
    % ------------------------------------------------------------
    stateString = strTag;                           % GS, IS or FS
    
    para.Ion = handles.currIonStr;                  % e.g. 
    para.Z   = handles.currentZ;
    para.Spectroscopy = handles.spectroscopy;
    
    coreConfStr = getStrTag( ['uitxt_core_' stateString] );
    valConfStr  = getStrTag( ['uitxt_val_' stateString] );
    para.(['config_' stateString]) = [coreConfStr ' ' valConfStr];         
    para.(['eleN_core_' stateString]) = str2num( coreConfStr(end-1:end) );         
    para.(['eleN_valence_' stateString]) = str2num( valConfStr(end-1:end) );         

    % ------------------------------------------------------------
    % dynamic labelling as it changes for 1s2p and 2p3d RIXS  (even the IS and FS in 1s2pRIXS are different!)
    labelF0cv = [getStrTag( ['txt_F0cv_' stateString] ) '_' stateString ];
    labelF2cv = [getStrTag( ['txt_F2cv_' stateString] ) '_' stateString ];
    labelG0cv = [getStrTag( ['txt_G0cv_' stateString] ) '_' stateString ];
    labelG2cv = [getStrTag( ['txt_G2cv_' stateString] ) '_' stateString ];
%     para.(labelF0cv) = getValue( ['txtVal_F0cv_' stateString] );          % saving with dynamic stuct labels
    para.(labelF0cv) = getValFromConfigStr( handles, labelF0cv(1:end-3), stateString ) * str2num(getStrTag({['edit_F0cv_' stateString]}));          % saving with dynamic stuct labels
    para.(labelF2cv) = getValFromConfigStr( handles, labelF2cv(1:end-3), stateString ) * str2num(getStrTag({['edit_F2cv_' stateString]}));          % saving with dynamic stuct labels
    para.(labelG0cv) = getValFromConfigStr( handles, labelG0cv(1:end-3), stateString ) * str2num(getStrTag({['edit_G0cv_' stateString]}));  
    para.(labelG2cv) = getValFromConfigStr( handles, labelG2cv(1:end-3), stateString ) * str2num(getStrTag({['edit_G2cv_' stateString]})); 

    labelF0vv = [getStrTag( ['txt_F0vv_' stateString] ) '_' stateString ];
    labelF2vv = [getStrTag( ['txt_F2vv_' stateString] ) '_' stateString ];
    labelF4vv = [getStrTag( ['txt_F4vv_' stateString] ) '_' stateString ];
    para.(labelF0vv) = getValFromConfigStr( handles, labelF0vv(1:end-3), stateString ) * str2num(getStrTag({['edit_F0vv_' stateString]})); 
    para.(labelF2vv) = getValFromConfigStr( handles, labelF2vv(1:end-3), stateString ) * str2num(getStrTag({['edit_F2vv_' stateString]})); 
    para.(labelF4vv) = getValFromConfigStr( handles, labelF4vv(1:end-3), stateString ) * str2num(getStrTag({['edit_F4vv_' stateString]})); 
    
    para.(['SOCc_' stateString ]) = getValFromConfigStr( handles, 'SOCc_', stateString ) * str2num(getStrTag({['edit_SOCc_' stateString]})); 
    para.(['SOCv_' stateString ]) = getValFromConfigStr( handles, 'SOCv_', stateString ) * str2num(getStrTag({['edit_SOCv_' stateString]})); 
    
    % ------------------------------------------------------------
    % ------------------------------------------------------------
    para.(['Dq_' stateString ]) = str2num(getStrTag( ['edit_10Dq_' stateString] )) / 10 ; 
    para.(['Ds_' stateString ]) = str2num(getStrTag( ['edit_Ds_' stateString] )) ; 
    para.(['Dt_' stateString ]) = str2num(getStrTag( ['edit_Dt_' stateString] )) ; 
    para.(['M_' stateString ])  = str2num(getStrTag( ['edit_M_' stateString] )) ; 
    % ------------------------------------------------------------
   
    % ------------------------------------------------------------
    para.Bx = handles.appSettings.Bx;
    para.By = handles.appSettings.By;
    para.Bz = handles.appSettings.Bz;
    % ------------------------------------------------------------
        
    
    % ------------------------------------------------------------
    para.(['L_' stateString ]) = str2num(getStrTag( ['edit_L_' stateString] )) ; 
    para.(['G_' stateString ])  = str2num(getStrTag( ['edit_G_' stateString] )) ;  
    % ------------------------------------------------------------
        
    % ------------------------------------------------------------
    % ------------------------------------------------------------    
        
%     edit_L_GS
%     edit_G_GS
%     edit_L_IS
%     edit_G_IS
%     edit_L_FS
%     edit_G_FS

    % ------------------------------------------------------------
    function outVal = getValue(inTag)
        txtvalStr = getStrTag( inTag );
        valStrParts = strsplit(txtvalStr,{' '});            % split cellData at ' '  (space)
        valueStr    = char(valStrParts(1));                 % first is the value, second is 'eV'
        outVal = str2num( valueStr );
    end
    % ------------------------------------------------------------

end

