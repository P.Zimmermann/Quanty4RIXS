function handles = suggestValues(handles)


    % TODO: Make faster//// more efficient - is still very slow when sugg data exists.


    % ----------------------------------------------------------------------
    debugMsg( 'calling suggestValues()', 2 )
    % ----------------------------------------------------------------------
    % ----------------------------------------------------------------------
    % checking if suggesting values exist for current ion and apply them.    
    if isfield(handles,'currIonStr') && isfield(handles,'currentElement')
        ionStr  = handles.currIonStr;
        elemStr = handles.currentElement;
%         oxidationLvl = str2num(ionStr(length(elemStr)+1:end-1));
        labels  = handles.appSettings.suggIons;                             % all ionString in suggestion table     
        if length(ionStr) > length(elemStr)+1
            
            if ismember(char(ionStr),labels)        % check if suggestion value in table
                
                updateStatus(['Saved values for Ion "' ionStr '" suggested.'])

                % ----------------------------------------------
                % find current ion in suggTab                
                found=0;
                for i=1:length(labels)
                    if char(ionStr) == labels{i}
                        found = i;
                        % ----------------------------------------------------------------------
                        debugMsg( '!!! Found Suggestion Values in Settings !!!  --- in suggestValues()', 1 )
                        % ---------------------------------------------------------------------- 
                    end    
                end
                % get sugg Table and extract data for selected ion
                suggTab = handles.appSettings.suggTab;              % get complete set of suggestion data               
                suggVals = suggTab(:,found);                        % extract found entry, suggVals has now the values for the current Ion
                clear suggTab found;
                % ---------------------------------------------------------------------- 
            
                % ----------------------------------------------
                % here setting the symmetry (popup box in CF panel)
                pm_Strings = getStrTag( ['pm_Symmetry'] ) ;                               
                selSym = find(ismember(pm_Strings,suggVals{1} ));    % first entry is symmtry
                setValTag(['pm_Symmetry'], selSym );     
                % ----------------------------------------------
                % call callback to update GUI for seletced symmetry
                pm_Symmetry_Callback(findobj('Tag','pm_Symmetry'), 0, handles)
                % ----------------------------------------------------------------------------------------

                % ----------------------------------------------------------------------     
                suggRows = handles.appSettings.suggRows;
                % go through all sugg values for the selected ion                
                for k=2:length(suggRows)
                  
                    thisTag = ['edit_' char(suggRows(k)) ];      % create editTag from row label
                    thisVal = str2num(char(suggVals(k,1)));
                    
%                     char(thisTag(1:end-3))
                    
                    % fixing/adding 10Dq (only exception)
                    if strcmp(char(thisTag(1:end-3)), 'edit_Dq') % Problem: 10Dq is not Dq, edit_tag needs to be dealt with
                        thisTag = ['edit_10Dq' thisTag(end-2:end)];
                        thisVal = thisVal * 10;
                    end    
                    % -------------------------------------
                    setStrTag(thisTag, num2str(thisVal)  )              % write value to the edit box by Tag   
                    % -------------------------------------
                    % TEST: Manual sync of absolute values
                    scaleValue( findobj('Tag',thisTag), handles )
                    % :) Seems much faste than the callback function
                    % -------------------------------------
                    pause(0.001)            % NOTE: Couterintuitive, but subjective it feels faster with this delay. (because it shows constant changes)
                    % -------------------------------------
                    
                end % END: for k=2:length(suggRows) 
                % ---------------------------------------------------------------------- 
                clear suggRows suggVals hEditBox thisTag
                
                % -----------------------------------
                % Values have changed after updating.
                % updating the CF TS for Dq Ds Dt
%                 updateTS_DqDtDs(  )     
                % ---------------------------------------------------------------------- 
                
                
                % ----------------------------------------------
            else
                % ELSE: No suggData for current Ion 
                % --------------------------------------------------  
                % Clear CF parameter if no sugg values exist
                setStrTag({'edit_10Dq_GS','edit_Ds_GS','edit_Dt_GS','edit_M_GS'},'0.000')          % setting unused values to 0.000 (for clarity)
                setStrTag({'edit_10Dq_IS','edit_Ds_IS','edit_Dt_IS','edit_M_IS'},'0.000')          % setting unused values to 0.000 (for clarity)
                setStrTag({'edit_10Dq_FS','edit_Ds_FS','edit_Dt_FS','edit_M_FS'},'0.000')          % setting unused values to 0.000 (for clarity)
             
                % --------------------------------------------------  
                % suggest broadenings based on sepctroscopy
                handles = fillBroadenings(handles);
                % --------------------------------------------------                

            end
        
        end
    

    end
        
      
        


end

