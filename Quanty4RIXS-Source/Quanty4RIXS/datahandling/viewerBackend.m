classdef viewerBackend < handle
    %VIEWERBACKEND Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        
        mapData;
        selectedMaps;
        
    end
    
    methods
        
        %% ----------------------------------------------------------------------------
        %> class constructor - creates and init's the backend instance
        %>
        function thisObj = viewerBackend()
            % ---------------------------------------------------------------
            debugMsg(' !*!*! Initial backend Setup (viewerBackend) started... !*!*!',1);
            % ---------------------------------------------------------------
            thisObj.mapData      = {};
            thisObj.selectedMaps = [];           
            % ---------------------------------------------------------------
            debugMsg(' !*!*! Initial backend Setup (viewerBackend) DONE !*!*!',1);
            % ---------------------------------------------------------------
        end % viewerBackend - constructor
        % --------------------------------------------------------------------------
        % --------------------------------------------------------------------------
        function importMapDirData( thisObj, pathname, filenames)
        
            % TODO: add here a basename check (to check not only the extension but also the basename)
            
            % go through selected files and check ending (OPvals.txt or Spec.txt)
            % sort into map files and Observables files
            xasFiles = {};
            mapFiles = {};
            obsFiles = {};
            eldFiles = {};
            % check the file ending and store in Spec or OPvals list
            if iscell(filenames)
                for j=1:length(filenames)
                    thisFile = filenames{j};
                    
%                     thisFile(end-12:end)
                    
                    if strcmp(thisFile(end-7:end),'Spec.txt')
                        if strcmp(thisFile(end-11:end),'XAS-Spec.txt')
                            % if XAS-Spec file then add to xasFiles cell list 
                        	xasFiles{length(xasFiles)+1} = thisFile;
                        else
                            % if Spec file then add to mapFiles cell list 
                            mapFiles{length(mapFiles)+1} = thisFile;
                        end
                    elseif strcmp(thisFile(end-9:end),'OPvals.txt')
                        % if OPval file then add to opFiles cell list 
                        obsFiles{length(obsFiles)+1} = thisFile;  
                    elseif strcmp(thisFile(end-12:end),'ELDiagram.txt')
                        % if EnerLvlDiag file then add to opFiles cell list 
                        eldFiles{length(eldFiles)+1} = thisFile;                          
                    end 
                end
            else
                % only one entry, then check ending and store as single cell
                thisFile = filenames;                   
                if strcmp(thisFile(end-7:end),'Spec.txt')
                    if strcmp(thisFile(end-11:end),'XAS-Spec.txt')
                        % if XAS-Spec file then add to mapFiles cell list 
                        xasFiles{length(xasFiles)+1} = thisFile;
                    else
                        % if Spec file then add to mapFiles cell list 
                        mapFiles{length(mapFiles)+1} = thisFile;
                    end
                elseif strcmp(thisFile(end-9:end),'OPvals.txt')       % thisFile(end-9:end)
                    % if OPval file then add to opFiles cell list 
                    obsFiles{length(obsFiles)+1} = thisFile ;  
                elseif strcmp(thisFile(end-12:end),'ELDiagram.txt')
                    % if EnerLvlDiag file then add to opFiles cell list 
                    eldFiles{length(eldFiles)+1} = thisFile ;                      
                end         

            end
            clear filenames

            % --------------------------------------------------------------------------------    
%             mapFiles
%             obsFiles
%             eldFiles
            % --------------------------------------------------------------------------------    
            % NOTE here: Observable file alone is only loaded when mapFiles is empty
            % If seperta loaded of OPvals with map is neede, the alter here IF instead of ELSEIF
            if length(xasFiles) > 0
            % load Maps
                thisObj.importXAS( pathname, xasFiles );
            end
            if length(mapFiles) > 0
            % load Maps
                thisObj.importMaps( pathname, mapFiles );
            end
            if length(obsFiles) > 0
                %load Observables
                thisObj.importOPvals( pathname, obsFiles );
            end    
            if length(eldFiles) > 0
                %load Observables
                thisObj.importELDiagram( pathname, eldFiles );                
            end
            % --------------------------------------------------------------------------------
                     
        end
     % --------------------------------------------------------------------------
        function importXAS( thisObj, pathname, filenames )
            
            % load the Quanty Maps files created with Quanty4RIXS
            % -------------------------------------------------------
            % merging path and filename into one string with the exact paths to the selected files
            for i=1:length(filenames)
                xasFiles{i} = fullfile(pathname, filenames{i} );
            end
            % -----------------------------------------
            xasFiles                % mapFiles has now the full file path for all map files
            % -----------------------------------------
            % here a check is needed if the folder has already been loaded....
            lenMapData = length(thisObj.mapData);
            found = 0;
            
            
            for i=1:lenMapData                
%                 pathCheck1 = thisObj.mapData{i}.mapFolder
%                 pathCheck2 = pathname                                
                if strcmp(thisObj.mapData{i}.mapFolder, pathname)
                    found = i;
                end         
            end
            
%             found
            
            if found > 0 
                % object already existing for current mapFolder 
                % --------------------------------------------------------------
                % Here a new mapSet Object needs to be added to local mapData entry                
                foundMapSet = thisObj.mapData{found}                 % select the mapset for the found path
                foundMapSet.importQuantyMaps(xasFiles);              % import the RAW mapfiles (.txt) , checks also for values.txt (expectations values)                                              
                % ---------------------------------------------------------------   
            else               
                % ---------------------------------------------------------------
                % classMapSet gets the complete list of mapFiles
                newXAS = classMapSet(xasFiles);                       % new class directly load mapdata 
                % ---------------------------------------------------------------
                % store the new mapset in a new entry in the backend
                thisObj.mapData{lenMapData+1} = newXAS;
                % ---------------------------------------------------------------  
            end    
            clear found
        end        
        % --------------------------------------------------------------------------
        function importMaps( thisObj, pathname, filenames )
            
            % load the Quanty Maps files created with Quanty4RIXS
            % -------------------------------------------------------
            % merging path and filename into one string with the exact paths to the selected files
            for i=1:length(filenames)
                mapFiles{i} = fullfile(pathname, filenames{i} );
            end
            % -----------------------------------------
            mapFiles                % mapFiles has now the full file path for all map files
            % -----------------------------------------
            % here a check is needed if the folder has already been loaded....
            lenMapData = length(thisObj.mapData);
            found = 0;
            for i=1:lenMapData                
                if strcmp(thisObj.mapData{i}.mapFolder,pathname)
                    found = i;
                end         
            end
            
            if found > 0 
                % object already existing for current mapFolder 
                % --------------------------------------------------------------
                % Here a new mapSet Object needs to be added to local mapData entry                
                foundMapSet = thisObj.mapData{found}                 % select the mapset for the found path
                foundMapSet.importQuantyMaps(mapFiles);              % import the RAW mapfiles (.txt) , checks also for values.txt (expectations values)                                              
                % ---------------------------------------------------------------   
            else               
                % ---------------------------------------------------------------
                % classMapSet gets the complete list of mapFiles
                newMap = classMapSet(mapFiles);                       % new class directly load mapdata 
                % ---------------------------------------------------------------
                % store the new mapset in a new entry in the backend
                thisObj.mapData{lenMapData+1} = newMap;
                % ---------------------------------------------------------------  
            end    
            clear found
        end
        % --------------------------------------------------------------------------
        function importOPvals( thisObj, pathname, filenames )
                   
            % load the Observable files created with Quanty4RIXS
            % -------------------------------------------------------
            % merging path and filename into one string with the exact paths to the selected files
            for i=1:length(filenames)
                obsFiles{i} = fullfile(pathname, filenames{i} );
            end

            % -----------------------------------------
            obsFiles             
            % (Usually there will be only one file in the folder, otherwise the data will be overwritten woith the last loaded file)
            % -----------------------------------------
            % here a check is needed if the folder has already been loaded....
            lenMapData = length(thisObj.mapData);
            found = 0;
            for i=1:lenMapData
                if strcmp(thisObj.mapData{i}.mapFolder,pathname)
                    found = i;
                end                      
            end
            
            if found > 0 
                % object already existing for current mapFolder 
                % --------------------------------------------------------------
                % add  Observable to extsing object
                foundMapSet = thisObj.mapData{found}                            % select the mapset for the found path
                foundMapSet.importValueTXT(obsFiles);                % import _values.txt (expectations values)
                % --------------------------------------------------------------
            else
                % ---------------------------------------------------------------
                % classMapSet gets the complete list of obsFiles  
                newMap = classMapSet();                       % CREATE EMPTY new class 
                newMap.importValueTXT(obsFiles);                % import values.txt (expectations values)
                newMap.mapFolder = pathname;
                % ---------------------------------------------------------------
                thisObj.mapData{lenMapData+1} = newMap;
                % ---------------------------------------------------------------                  
            end
            clear found
          
        end    
        % --------------------------------------------------------------------------        
        % --------------------------------------------------------------------------
        function importELDiagram( thisObj, pathname, filenames )
                   
            % load the Observable files created with Quanty4RIXS
            % -------------------------------------------------------
            % merging path and filename into one string with the exact paths to the selected files
            for i=1:length(filenames)
                eldFiles{i} = fullfile(pathname, filenames{i} );
            end

            % -----------------------------------------
            eldFiles             
            % (Usually there will be only one file in the folder, otherwise the data will be overwritten woith the last loaded file)
            % -----------------------------------------
            % here a check is needed if the folder has already been loaded....
            lenMapData = length(thisObj.mapData);
            found = 0;
            for i=1:lenMapData
                if strcmp(thisObj.mapData{i}.mapFolder,pathname)
                    found = i;
                end                      
            end
            
            if found > 0 
                % object already existing for current mapFolder 
                % --------------------------------------------------------------
                % add  Observable to extsing object
                foundMapSet = thisObj.mapData{found}                            % select the mapset for the found path
                foundMapSet.importELDiagram(eldFiles);                % import _values.txt (expectations values)
                % --------------------------------------------------------------
            else
                % ---------------------------------------------------------------
                % classMapSet gets the complete list of obsFiles  
                newMap = classMapSet();                       % CREATE EMPTY new class 
                newMap.importELDiagram(eldFiles);                % import values.txt (expectations values)
                newMap.mapFolder = pathname;
                % ---------------------------------------------------------------
                thisObj.mapData{lenMapData+1} = newMap;
                % ---------------------------------------------------------------                  
            end
            clear found
          
        end    
        % --------------------------------------------------------------------------        
        % --------------------------------------------------------------------------        
                
        function clearMapSets(thisObj)
            % ---------------------------------------------------------------
            % HERE ALL DATA IS DELETED from Backend
            thisObj.selectedMaps = [];
            for k=1:length(thisObj.mapData)
                thisObj.mapData{k}.delete();        % calls DESTRUCTOR of the mapset
            end
            thisObj.mapData      = {};            
            % ---------------------------------------------------------------
        end
        % --------------------------------------------------------------------------
     
        
    end % methods - public
    % =============================================================================
    methods (Access=private)
 
    end % methods - private
     % ============================================================================= 
     
     
end

