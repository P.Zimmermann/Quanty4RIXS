function zIonLines = getIonsByZ( z, handles )
%GETIONSBYZ Summary of this function goes here
%   Detailed explanation goes here


    zIonLines = {};
    lineIdx = 0;
    
    rcnData = handles.rcnDataLines;

%     szsTotal = size(rcnData)

    
    for l = 1:length(rcnData)
        if strcmp(['Z=' num2str(z)], rcnData{l}(1))         % compare first entry in line to find elements
            lineIdx = lineIdx+1;
            zIonLines{lineIdx} = rcnData{l};
        end
        
    end

   szIon = size(zIonLines);
   disp(['Found ' num2str(szIon(1,2)) ' entries for Z=' num2str(z) '.'])

    





end

