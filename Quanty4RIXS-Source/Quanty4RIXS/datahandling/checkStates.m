function result = checkStates( thisIonStr, ionList, handles )

% here we check if for the selected spectroscopy the 3 state (GS,IS,FS) configs are there here
% used f.e. to avoid oxidation states in the list that are not complete (enables to alter, add or if wanted to remove oxides)
    
     % --------------------------------------------------
    checkVal = 0;
    
     % --------------------------------------------------
%     if (handles.spectroscopy == '1s2pRIXS')
    if ( strcmp(handles.spectroscopy, '1s2pRIXS') || strcmp(handles.spectroscopy, '1s2pRIXSMCD') )        
%     if getValTag({'rb_1s2pRIXS'})
%         updateStatus(['Checking states for 1s2pRIXS...'])
        % the shells are spectroscopy specific
        coreGSconf = '1s02';
        coreISconf = '1s01';
        coreFSconf = '2p05';
        valShell = '3d';
        stateNum = 3;                           % number of states that are needed, chekVal must reach that value (see below)
    elseif (handles.spectroscopy == '2p3dRIXS')
%     elseif getValTag({'rb_2p3dRIXS'})
%         updateStatus(['Checking states for 2p3dRIXS...'])
        % the shells are spectroscopy specific
        coreGSconf = '2p06';
        coreISconf = '2p05';
        coreFSconf = '2p06';
        valShell = '3d';
        stateNum = 2;                         % number of states that are needed, chekVal must reach that value (see below)
    end     
 
    for row=1:length(ionList)
        if length(ionList{row}) >= 4
            tmpCoreStr = char(ionList{row}(2));
            tmpValStr = char(ionList{row}(3));
            tmpIonStr = char(ionList{row}(4));
            
%             disp('-')
%             strcmp(lower(tmpCoreStr), coreGSconf)
            
            if strcmp(tmpIonStr,thisIonStr)          
                if strcmp(lower(tmpCoreStr), coreGSconf)
                    checkVal = checkVal + 1;
                end
                if strcmp(lower(tmpCoreStr), coreISconf) 
                    checkVal = checkVal + 1;
                end
                if strcmp(lower(tmpCoreStr), coreFSconf)
                    checkVal = checkVal + 1;
                end
            end
                
                
        end    
    end

    % --------------------------------------------------
%     checkVal
    % --------------------------------------------------
   
    if checkVal >= stateNum
        result = 1;
    else
        result = 0;
    end        
        
    clear checkVal
    % --------------------------------------------------
    
    


end

