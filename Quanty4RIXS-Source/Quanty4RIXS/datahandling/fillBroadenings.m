function handles = fillBroadenings(handles)
%FILLCFVALUES Summary of this function goes here
%   Detailed explanation goes here

    % --------------------------------------------
    debugMsg( 'calling fillBroadenings()', 2 )
    % --------------------------------------------
 
    % ----------------------------------------------------------------------
    updateStatus('Broadenings for 1s2pRIXS')
    % ----------------------------------------------------------------------
    
    sugg_1s_L = 1.2;
    sugg_1s_G = 0.6;
    
    sugg_2p_L = 0.4;    % 0.4-0.6 for 2d ?
    sugg_2p_G = 0.3;

    sugg_3d_L = 0.2;    % 0.1-0.2 for 3d ?
    sugg_3d_G = 0.3;    

    if isfield(handles,'spectroscopy') 
        specStr = handles.spectroscopy;
        
        if strcmp(specStr,'1s2pRIXS')                    
            setStrTag('edit_L_GS',num2str(0.0));            % GS has infinite LT -> Broadening 0.0eV
            setStrTag('edit_L_IS',num2str(sugg_1s_L));
            setStrTag('edit_L_FS',num2str(sugg_2p_L));

            setStrTag('edit_G_GS',num2str(sugg_1s_G));
            setStrTag('edit_G_IS',num2str(sugg_1s_G));
            setStrTag('edit_G_FS',num2str(sugg_2p_G));
            
        elseif strcmp(specStr,'2p3dRIXS')
            setStrTag('edit_L_GS',num2str(0.0));            % GS has infinite LT -> Broadening 0.0eV
            setStrTag('edit_L_IS',num2str(sugg_2p_L));
            setStrTag('edit_L_FS',num2str(sugg_3d_L));

            setStrTag('edit_G_GS',num2str(sugg_2p_G));
            setStrTag('edit_G_IS',num2str(sugg_2p_G));
            setStrTag('edit_G_FS',num2str(sugg_2p_G));            
        end    
    end


end

