function para = readAllParamsFromGUI( handles, strTag  )
%READALLVALUESFROMGUI Summary of this function goes here
%   Detailed explanation goes here

    debugMsg( 'calling readAllParamsFromGUI()', 2 )

    % ------------------------------------------------------------
    stateString = strTag;                           % GS, IS or FS
    
    para.Ion = handles.currIonStr;                  % e.g. 
    para.Z   = handles.currentZ;
%     coreConfStr = getStrTag( ['uitxt_core_' stateString] );
%     valConfStr  = getStrTag( ['uitxt_val_' stateString] );
%     para.(['config_' stateString]) = [coreConfStr ' ' valConfStr];         
%     para.(['eleN_core_' stateString]) = str2num( coreConfStr(end-1:end) );         
%     para.(['eleN_valence_' stateString]) = str2num( valConfStr(end-1:end) );         

    % ------------------------------------------------------------
    % dynamic labelling as it changes for 1s2p and 2p3d RIXS  (even the IS and FS in 1s2pRIXS are different!)
    para.(['F0cv_' stateString]) = str2num(getStrTag({['edit_F0cv_' stateString]}));       
    para.(['F2cv_' stateString]) = str2num(getStrTag({['edit_F2cv_' stateString]}));          % saving with dynamic stuct labels
    para.(['G0cv_' stateString]) = str2num(getStrTag({['edit_G0cv_' stateString]}));  
    para.(['G2cv_' stateString]) = str2num(getStrTag({['edit_G2cv_' stateString]})); 


    para.(['F0vv_' stateString]) = str2num(getStrTag({['edit_F0vv_' stateString]})); 
    para.(['F2vv_' stateString]) = str2num(getStrTag({['edit_F2vv_' stateString]})); 
    para.(['F4vv_' stateString]) = str2num(getStrTag({['edit_F4vv_' stateString]})); 
    
    para.(['SOCc_' stateString ]) = str2num(getStrTag({['edit_SOCc_' stateString]})); 
    para.(['SOCv_' stateString ]) = str2num(getStrTag({['edit_SOCv_' stateString]})); 
    
    % ------------------------------------------------------------
%     para.(['Sym_' stateString ]) = str2num(getStrTag( ['pm_Symmetry'] )); 
    % ------------------------------------------------------------
    para.(['Dq_' stateString ]) = str2num(getStrTag( ['edit_10Dq_' stateString] )) / 10 ; 
    para.(['Ds_' stateString ]) = str2num(getStrTag( ['edit_Ds_' stateString] )) ; 
    para.(['Dt_' stateString ]) = str2num(getStrTag( ['edit_Dt_' stateString] )) ; 
    para.(['M_' stateString ])  = str2num(getStrTag( ['edit_M_' stateString] )) ; 
    % ------------------------------------------------------------
   
    % ------------------------------------------------------------
    para.(['L_' stateString ])  = str2num(getStrTag( ['edit_L_' stateString] )) ; 
    para.(['G_' stateString ])  = str2num(getStrTag( ['edit_G_' stateString] )) ;  
    % ------------------------------------------------------------
        
    % ------------------------------------------------------------
    % ------------------------------------------------------------    
    
%     edit_L_GS
%     edit_G_GS
%     edit_L_IS
%     edit_G_IS
%     edit_L_FS
%     edit_G_FS

%     % ------------------------------------------------------------
%     function outVal = getValue(inTag)
%         txtvalStr = getStrTag( inTag );
%         valStrParts = strsplit(txtvalStr,{' '});            % split cellData at ' '  (space)
%         valueStr    = char(valStrParts(1));                 % first is the value, second is 'eV'
%         outVal = str2num( valueStr );
%     end
%     % ------------------------------------------------------------

end

