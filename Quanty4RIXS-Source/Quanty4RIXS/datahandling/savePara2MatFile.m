function [ output_args ] = savePara2MatFile( saveParaFName, paraGS, paraIS, paraFS, paraRanges)

    % ------------------------------------------------------------------------------------------------  
    % create a matlab struct from the parameters (for later to used for the plotting)
    % ------------------------------------------------------------------------------------------------  
    % start with all paraGS copy to para 
    para = paraGS;                          % start with GS (then add the values for IS and FS) and the energy ranges
    % ------------------------------------------------    
    % adding all paraIS to para 
    fieldNames = fieldnames(paraIS);
    for i = 1:size(fieldNames,1)
        para.(fieldNames{i}) = paraIS.(fieldNames{i});
    end
    % ------------------------------------------------    
    % adding all paraFS to para     
    fieldNames = fieldnames(paraFS);
    for i = 1:size(fieldNames,1)
        para.(fieldNames{i}) = paraFS.(fieldNames{i});
    end
    % ------------------------------------------------    
    % adding all paraRanges to para 
    fieldNames = fieldnames(paraRanges);
    for i = 1:size(fieldNames,1)
        para.(fieldNames{i}) = paraRanges.(fieldNames{i});
    end
    % ------------------------------------------------    
%         para                                                  % DEBUG: para has now all GUI values (GS, IS, FS, and ranges)
    % ------------------------------------------------

    % ------------------------------------------------------------------------------------------------  
    debugMsg( ['saving parameter to matlab file: ' saveParaFName], 1 )
    save(saveParaFName,'para')
    % ------------------------------------------------------------------------------------------------     
    % ------------------------------------------------------------------------------------------------  


end

