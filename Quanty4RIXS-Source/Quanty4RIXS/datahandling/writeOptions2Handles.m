function handles = writeOptions2Handles( handles )


% TODO: compare this file to uiOptions for redundant checks...!!! 

    % ---------------------------------------
    debugMsg( 'writeOptions2Handles()', 2 )
    % ---------------------------------------

%     handles.appSettings.calcEigenVals = 1

    
    % --------------------------------------------
    % read checkbox and store in handles.options
    if getValTag('cb_calcExpectVals') == 1
%         handles.options.calcEigenVals = 1;
        handles.appSettings.calcEigenVals = 1;                  % NEW Store in appSettings  (is also saved to file)
    elseif getValTag('cb_calcExpectVals') == 0
%         handles.options.calcEigenVals = 0;
        handles.appSettings.calcEigenVals = 0;                  % NEW Store in appSettings  (is also saved to file)        
    else
%         setValTag('cb_calcExpectVals',handles.options.calcEigenVals)
        setValTag('cb_calcExpectVals',handles.appSettings.calcEigenVals)
    end  
    % --------------------------------------------
    % read checkbox and store in handles.options
    if getValTag('cb_calcCFdiagram') == 1
        handles.appSettings.calcELDiagram = 1;                  % NEW Store in appSettings  (is also saved to file)
    elseif getValTag('cb_calcCFdiagram') == 0
        handles.appSettings.calcELDiagram = 0;                  % NEW Store in appSettings  (is also saved to file)        
    else
        setValTag('cb_calcCFdiagram',handles.appSettings.calcELDiagram)
    end      
    
    % --------------------------------------------
    tmpEV = str2num(getStrTag('edit_noOfEV'));
    tmpGS = str2num(getStrTag('edit_noOfGS'));

    tmpBx = str2num(getStrTag('edit_magnBx')); 
    tmpBy = str2num(getStrTag('edit_magnBy')); 
    tmpBz = str2num(getStrTag('edit_magnBz'));

    if (tmpEV > 0) && (tmpEV >= tmpGS)      
%         handles.options.noOfEV = tmpEV;
        handles.appSettings.noOfEV = tmpEV;
    else    
%         setStrTag({'edit_noOfEV'},num2str(handles.options.noOfEV))
        setStrTag({'edit_noOfEV'},num2str(handles.appSettings.noOfEV))
    end    
    % --------------------------------------------
    if (tmpGS > 0) && (tmpGS <= tmpEV)      
%         handles.options.noOfGS = tmpGS;
        handles.appSettings.noOfGS = tmpGS;
    else    
%         setStrTag({'edit_noOfGS'},num2str(handles.options.noOfGS))
        setStrTag({'edit_noOfGS'},num2str(handles.appSettings.noOfGS))
    end  
    
    % --------------------------------------------
  
    % --------------------------------------------
    if isreal(tmpBx)
        handles.appSettings.Bx = tmpBx;
    end
    % --------------------------------------------
    if isreal(tmpBy)
        handles.appSettings.By = tmpBy;
    end   
    % --------------------------------------------
    if isreal(tmpBz)
        handles.appSettings.Bz = tmpBz;
    end           
    % --------------------------------------------

    
    % --------------------------------------------
%     debugMsg( 'Options saved in handles...', 1 )        
%     debugOptions = handles.options                    % OLD , options now saved in appSettings -> saved to file
    % --------------------------------------------
    saveAppSettingsFile( handles )
    % --------------------------------------------

end

