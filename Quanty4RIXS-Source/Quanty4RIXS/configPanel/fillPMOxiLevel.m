function handles = fillPMOxiLevel(z, handles )
%FILLPMOXILEVEL Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------------
    debugMsg( 'calling fillPMOxiLevel()', 0 )
    % -------------------------------------------------------

    % --------------------------------------------------
    ionList = getIonsByZ( z, handles );
    handles.oxiLevels = ionList;
    % --------------------------------------------------    
    % extracting the Element from the IonString
    tempIon = char(ionList{1}(4));
    thisElement = tempIon(1:end-3);
    if strcmp(tempIon(end-2:end),'10+')
        thisElement = tempIon(1:end-3);
    else
        thisElement = tempIon(1:end-2);
    end
    handles.currentElement = thisElement;               % storing element in handles
    % --------------------------------------------------
    % writing the Element and Z to the config panel
    setStrTag({'uitxt_elemZ'},[thisElement ' (Z=' num2str(z) ')'])
    % --------------------------------------------------
    % --------------------------------------------------
    n = 0;
    valencies = {};
    for r=1:length(ionList)             % run through ion configs list        
        if ( length(ionList{r}) >= 4 ) 
            thisIonStr = ionList{r}(4);         % take Ionstring
            % --------------------------------------------------
            % NOTE: here a check that all 3 datasets (GS, IS, FS) exist before adding
            if checkStates(thisIonStr, ionList, handles)     % check if row is at least 4 entries long
                n=n+1; 
                valencies{n} = char(thisIonStr);
            end
        end  
    end
    clear n
    % --------------------------------------------------    
%     thisIonStr   
%     ionList
%     valencies
    

    % --------------------------------------------------
    
    
    % --------------------------------------------------
    % --------------------------------------------------
    % fill the Oxilevel popupmenu with the data
    valencies = unique(valencies);    
    if length(valencies) > 0
        set(findobj('Tag','pm_oxiLevel'),'String',valencies)        % writing available valencies in the popup menu
        tagEnable({'pm_oxiLevel'})        
        setValTag({'pm_oxiLevel'},1)                                % selecting 1st popup menu
        selected_IonString = valencies{1};
    else
        set(findobj('Tag','pm_oxiLevel'),'String','none')        % writing available valencies in the popup menu
        tagDisable({'pm_oxiLevel'})
        setValTag({'pm_oxiLevel'},1)                                % selecting 1st popup menu
        selected_IonString = '';
    end
    
    handles.currIonStr = char(selected_IonString);
    % --------------------------------------------------

    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------


end

