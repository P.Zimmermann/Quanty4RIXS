function [ output_args ] = cb_calcRIXSmaps_Callback( hObject, eventdata, handles )

    % --------------------------------------------------------------------------------
    debugMsg( 'calling cb_calcRIXSmaps_Callback()', 0 )
    % --------------------------------------------------------------------------------

    currentVal = hObject.Value

    if currentVal == 1
        setValTag('cb_calcXAS',0); 
        handles.appSettings.calcXAS = 0;
        tagEnable({'cb_calcExpectVals'})           
    end    
    
    handles.appSettings.calcRIXSmaps = currentVal;
    clear currentVal     
    % --------------------------------------------
    saveAppSettingsFile( handles )
    % --------------------------------------------

    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------
    
end

