function handles = fillElectronConfig( configStrGS,configStrIS,configStrFS , handles)
%FILLELECTRONCONFIG Summary of this function goes here
%   Detailed explanation goes here

    if (length(configStrGS) > 2) && (length(configStrIS) > 2) && (length(configStrFS) > 2)
        
        % --------------------------------------------------  
        % writing the selected ele config to the GUI
        % core shell
        gs_Core = char(configStrGS(2));
        is_Core = char(configStrIS(2));
        fs_Core = char(configStrFS(2));
              
        setStrTag('uitxt_core_GS',gs_Core)
        setStrTag('uitxt_core_IS',is_Core)
        setStrTag('uitxt_core_FS',fs_Core)

        % valence shell
        gs_Val = char(configStrGS(3));
        is_Val = char(configStrIS(3));
        fs_Val = char(configStrFS(3));       
        
        setStrTag('uitxt_val_GS',gs_Val)
        setStrTag('uitxt_val_IS',is_Val)
        setStrTag('uitxt_val_FS',fs_Val)
        % --------------------------------------------------  
       
    end
        
    clear configStrGS configStrIS configStrFS
    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
    






end

