function handles = updateConfigPanel(hObject, eventdata, handles)
%FILLCONFIGPANEL Summary of this function goes here
%   Detailed explanation goes here


    % --------------------------------------------------
    debugMsg( 'calling updateConfigPanel()', 2 )
    % --------------------------------------------------
%     z = handles.currentZ;    
    ionList = handles.oxiLevels;
    currIon = handles.currIonStr;
    valShell = '3d';
    % --------------------------------------------------
%     % debug outout
%     for k=1:length(ionList)
%         ionList{k}
%     end  
    
    if length(currIon) > 2
        % --------------------------------------------------
%         if strcmp(handles.spectroscopy, '1s2pRIXS')
        if getValTag({'rb_1s2pRIXS'})
            handles = payLoad(handles, '1s02', '1s01', '2p05');
        % --------------------------------------------------
%         elseif strcmp(handles.spectroscopy, '2p3dRIXS')
        elseif getValTag({'rb_2p3dRIXS'})
            handles = payLoad(handles, '2p06', '2p05', '2p06');
        end    
        % --------------------------------------------------
        % updating the CF TS for Dq Ds Dt
        updateTS_DqDtDs(  )                             % repeated in suggestValues (disabled in suggeestValues)
        % --------------------------------------------------
        pause(0.1);
        % --------------------------------------------------
        % Update handles structure and write it to the GUI
        guidata(findobj('Tag','mainFigure'), handles);
        % --------------------------------------------------
    end
   
    
    % ============================================================================================    
    % ============================================================================================   
    function handles = payLoad(handles, coreGSshell, coreISshell, coreFSshell)

        % get from the ion list the ele configs for 
        for row=1:length(ionList)
            if length(ionList{row}) >= 4
                thisEntryStr = ionList{row};         % take row from ionList
                tmpCoreStr = char(thisEntryStr(2));
                tmpValStr = char(thisEntryStr(3));          
                tmpIonStr = char(thisEntryStr(4));

                if strcmp(tmpIonStr,currIon)
                    if strcmp(lower(tmpCoreStr), coreGSshell)
                        configStrGS = thisEntryStr;
                    end
                    if strcmp(lower(tmpCoreStr), coreISshell)
                        configStrIS = thisEntryStr;
                    end
                    if strcmp(lower(tmpCoreStr), coreFSshell) 
                        configStrFS = thisEntryStr;
                    end           

                end                
            end    
        end
        % configStrGS 
        % configStrIS
        % configStrFS
        % now configStrGS, configStrIS and configStrFS are the selected rows for the current Ion
        % Note: All three states shoudl exist, test is done with checkStates() in fillPMOxiLevel()          
        % --------------------------------------------------
        handles = fillElectronConfig( configStrGS,configStrIS,configStrFS , handles);
        % --------------------------------------------------  
        handles = fillAtomicParameter(configStrGS, configStrIS, configStrFS, handles);
        % --------------------------------------------------  
        % suggest Values if enebled in settings
        if getValTag('cb_SettingsSuggVals') == 1
            % --------------------------------------------------  
            % lokk in suggestion Table for stored values
            handles = suggestValues(handles);      
            % --------------------------------------------------  
        else 
            % --------------------------------------------------  
            % suggest broadenings based on sepctroscopy
            handles = fillBroadenings(handles);
            % --------------------------------------------------  
        end

    end
    % ============================================================================================  
    % ============================================================================================  
        

    
    
    
end

