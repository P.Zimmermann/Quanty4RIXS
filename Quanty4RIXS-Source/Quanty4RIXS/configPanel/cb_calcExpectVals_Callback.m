function [ output_args ] = cb_calcExpectVals_Callback( hObject, eventdata, handles )

    % -------------------------------------------------------
    debugMsg( 'calling cb_calcExpectVals_Callback()', 0 )
    % --------------------------------------------

    currentVal = hObject.Value

    handles.appSettings.calcEigenVals = currentVal;

   
    % --------------------------------------------
    saveAppSettingsFile( handles )
    % --------------------------------------------

    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------    
    clear currentVal
    % --------------------------------------------

    
end

