function [ output_args ] = cb_calcXAS_Callback( hObject, eventdata, handles )
%CB_CALCXAS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------------------------------------- 
%     getValTag({'cb_calcXAS'}) 
    % --------------------------------------------------------------------------------
    debugMsg( ' cb_calcXAS_Callback()', 0 )
    % --------------------------------------------------------------------------------

    currentVal = hObject.Value

    if currentVal == 1
%         % disable observables
%         setValTag({'cb_calcExpectVals'},0);   
%         tagDisable({'cb_calcExpectVals'})
        % uncheck RIXS
        setValTag('cb_calcRIXSmaps',0);   
        handles.appSettings.calcRIXSmaps == 0;
    else
        % enable observables
        tagEnable({'cb_calcExpectVals'})
    end
   
    handles.appSettings.calcRIXSmaps = getValTag('cb_calcRIXSmaps');
    handles.appSettings.calcXAS = currentVal;
    clear currentVal
    % --------------------------------------------
    saveAppSettingsFile( handles )
    % --------------------------------------------

    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------
    
end

