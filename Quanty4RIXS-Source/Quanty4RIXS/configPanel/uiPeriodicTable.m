function [ output_args ] = uiPeriodicTable(hObject, eventdata, handles)
% this function creates a Periodic table from buttons.
% the callback return just the selected Z.
% By default all elemnts/buttons are DISabled. With "eleOn" you can select which buttons/elements are enabled.
% 
% Written by Partic Zimmermann, Utrecht University, April 2016.
%
%


    % --------------------------------------------------------------------------------------------------------
    debugMsg( 'opening uiPeriodicTable()', 2 )
    % --------------------------------------------------------------------------------------------------------
%     eleOn = [21:30, 39:48];      % enabled elements
    eleOn = [21:30];      % enabled elements    
    % --------------------------------------------------------------------------------------------------------
    % Name Definitions for the GUI 
    namesRow1 = {'H','He'};  
    namesRow2 = {'Li','Be','B','C','N','O','F','Ne'};
    namesRow3 = {'Na','Mg','Al','Si','P','S','Cl','Ar'};
    namesRow4 = {'K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr'};
    namesRow5 = {'Rb','Sr' ,'Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe'};  
    namesRow6 = {'Cs','Ba','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn'};  
    namesRow6a= {'La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu'};
    namesRow7 = {'Fr','Ra','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg','Cn','Uut','Fl','Uup','Lv','Uus','Uuo'};  
    namesRow7a = {'Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Fr'};      
    % --------------------------------------------------------------------------------------------------------

    
    % -----------------------------------------------------------------------------
    % find position fo mainfigure and place uiPeriodic above
    hMainFigPOS = get(findobj('Tag','mainFigure'),'Position'); 
    % -----------------------------------------------------------------------------
    hFig_PerTab = figure;
%     figPos = get(hFig_PerTab,'Position');
    set(hFig_PerTab,...
        'Position',[hMainFigPOS(1)-100 hMainFigPOS(2)+hMainFigPOS(4)-500 800 450],...    % [x0, y0, w,h]  y0 relative to TOP of main figure (0,0 is bottom left, adapted to hieght)
        'Toolbar','none',...
        'Tag',['uiPerTab'],...
        'Menubar','none',...                
        'windowstyle','modal',...       % MODAL: STAY ON TOP                
        'Name','Select element of interest...',...
        'NumberTitle','off');
    % -----------------------------------------------------------------------------
    
    
    
    
    % -----------------------------------------------------------------------------
	% here the arrangement for the table id managed.
    x0 = 20;
    y0 = 260;
    
    dx = 42;
    dy = 40;
    
    row1 = [1:2];
    row2 = [3:10];
    row3 = [11:18];
   
    row4 = [19:36];
    row5 = [37:54];
    
    row6 = [55,56,72:86];
    row6a = [57:71];
    
    row7 = [87,88,104:118];
    row7a = [89:103];    
    

    % build rows of preriodic table
    for z=[row1,row2,row3 row4, row5, row6, row6a, row7, row7a]             
        
        if ismember(z,row1)                 % 1st row  Z=1,2
            row = 1; 
            if z == 1
                thisX0 = x0;
            else
                thisX0 = x0 + dx * 17;
            end    
        elseif ismember(z,row2)                 % 2nds row, Z = 3...10
           row = 2; 
            if ismember(z,[3,4])
                thisX0 = x0 + dx * (z-3);
            else
                thisX0 = x0 + dx * (z-3) + 10*dx;
            end
        elseif ismember(z,row3)                 % 3rd row, Z = 11...18
           row = 3; 
            if ismember(z,[11,12])
                thisX0 = x0 + dx * (z-11);
            else
                thisX0 = x0 + dx * (z-11) + 10*dx;
            end
        elseif ismember(z,row4)                 % 4rth row
           row = 4; 
           thisX0 = x0 + dx * (z-19);
        elseif ismember(z,row5)             % 4rth row
           row = 5;
           thisX0 = x0 + dx * (z-37);
        elseif ismember(z,row6)                 % 4rth row
           row = 6; 
            if ismember(z,[55,56])
                thisX0 = x0 + dx * (z-55);
            else
                thisX0 = x0 + dx * (z-55) - 14*dx;
            end
        elseif ismember(z,row6a)             % 4rth row
           row = 9;
           thisX0 = x0 + dx * (z-55);
        elseif ismember(z,row7)                 % 4rth row
           row = 7; 
            if ismember(z,[87,88])
                thisX0 = x0 + dx * (z-87);
            else
                thisX0 = x0 + dx * (z-87) - 14*dx;
            end
        elseif ismember(z,row7a)             % 4rth row
           row = 10;
           thisX0 = x0 + dx * (z-87);   
        end 
 
            
        thisY0 = y0 - dy * (row - 4);
        addElement(hFig_PerTab, thisX0, thisY0, z, handles)   ;     
    end     


    % -----------------------------------------------------------------------------
    % -----------------------------------------------------------------------------
    function hButton = addElement(hFig, x0,y0, z, handles)
        
        if ismember(z,row1)
            eleStr = namesRow1{z};
        elseif ismember(z,row2)
            eleStr = namesRow2{z-2};
        elseif ismember(z,row3)
            eleStr = namesRow3{z-10};
        elseif ismember(z,row4)
            eleStr = namesRow4{z-18};
        elseif ismember(z,row5)
            eleStr = namesRow5{z-36};
        elseif ismember(z,row6)
            gapLa = 0;
            if z>56 gapLa=15; end                   % coorection for La  (La in 6a) 
            eleStr = namesRow6{z-54-gapLa};
        elseif ismember(z,row6a)
            eleStr = namesRow6a{z-56};
        elseif ismember(z,row7)
            gapAc = 0;
            if z>88 gapAc=15; end
            eleStr = namesRow7{z-86-gapAc};
        elseif ismember(z,row7a)
            eleStr = namesRow7a{z-88};
        end
        
        if ismember(z,eleOn)
            hButton = uicontrol(hFig,...
                    'Position',[x0 y0 dx-1 dy-1],...
                    'String',['<html><center>' eleStr '<br><font size=2>Z=' num2str(z) '</font><center></html>'],...     %['Z=' num2str(z)],...    {eleStr; ['Z=' num2str(z)]}    'HorizontalAlignment','center',...
                    'FontSize',12,...
                    'Tag',['pb_Element' num2str(z)],...
                    'Callback',{@selectElement_Callback, handles, z});   
             
        else    
            hButton = uicontrol(hFig,...
                'Position',[x0 y0 dx-1 dy-1],...
                'String',['<html><center>' eleStr '<br><font size=2>Z=' num2str(z) '</font><center></html>'],...     %['Z=' num2str(z)],...    {eleStr; ['Z=' num2str(z)]}    'HorizontalAlignment','center',...
                'FontSize',12,...
                'Tag',['pb_Element' num2str(z)],...
                'Enable','off');   
         end   
            
            
    end
    % -----------------------------------------------------------------------------    
    % -----------------------------------------------------------------------------

end

