function [ output_args ] = cb_calcELDiagram_Callback( hObject, eventdata, handles )

    % -------------------------------------------------------
    debugMsg( 'calling cb_calcELDiagram_Callback()', 0 )
    % --------------------------------------------

    currentVal = hObject.Value

    handles.appSettings.calcELDiagram = currentVal;

   
    % --------------------------------------------
    saveAppSettingsFile( handles )
    % --------------------------------------------

    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------    
    clear currentVal
    % --------------------------------------------

    
end

