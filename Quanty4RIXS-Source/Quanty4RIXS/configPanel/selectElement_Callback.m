function selectElement_Callback(hObject, eventdata, handles, z)
%SELECTELEMENT_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

%     hObject   
%     eventdata
    
    % --------------------------------------------------
    hFigPT = findobj('Tag','uiPerTab');
    set(hFigPT,'windowstyle','normal')
    % --------------------------------------------------
    display('Closing Periodic Table');
%     close(gcf);
    close(hFigPT)
    % --------------------------------------------------
     
%     infoStr = ['Selected Z = ' num2str(z)];
%     updateStatus(infoStr)
    % --------------------------------------------------
    handles.currentZ = z;  
    % --------------------------------------------------    
    handles = fillPMOxiLevel(z, handles ); 
    % --------------------------------------------------
    handles = updateConfigPanel(hObject, eventdata, handles);
    % --------------------------------------------------
    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
    % --------------------------------------------------
    

end

