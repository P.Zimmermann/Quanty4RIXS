function edit_M_GS_Callback(hObject, eventdata, handles)
% hObject    handle to edit_M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_M as text
%        str2double(get(hObject,'String')) returns contents of edit_M as a double

    % ----------------------------------------------------------------------
    debugMsg( 'calling edit_M_GS_Callback()', 2 )
    % ----------------------------------------------------------------------    
    reformatStringValue( hObject,handles, 3 )
%     % ------------------------------------------------------
%     thisTag = hObject.Tag                        % e.g.:  'edit_F0cv_GS'    
%     % reformatting to 2 digits
%     setStrTag(thisTag,num2str(str2num(getStrTag(thisTag)) ,'%1.2f'))    
%     % ------------------------------------------------------
%          

    
    % copy value to IS when cb_IS is not checked
    if (get(findobj('Tag','cb_IS'),'Value')==0 )
        set(findobj('Tag','edit_M_IS'),'String',hObject.String)
    end  
    % copy value to FS when cb_FS is not checked
    if (get(findobj('Tag','cb_FS'),'Value')==0 )
        set(findobj('Tag','edit_M_FS'),'String',hObject.String)
    end       
    
end

