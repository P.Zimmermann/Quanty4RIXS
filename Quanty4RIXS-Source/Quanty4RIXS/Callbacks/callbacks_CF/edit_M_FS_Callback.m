function edit_M_FS_Callback(hObject, eventdata, handles)
% hObject    handle to edit_M_FS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_M_FS as text
%        str2double(get(hObject,'String')) returns contents of edit_M_FS as a double

    % ----------------------------------------------------------------------
    debugMsg( 'calling edit_M_FS_Callback()', 2 )
    % ----------------------------------------------------------------------    
    reformatStringValue( hObject,handles, 3 )
%     % ------------------------------------------------------
%     thisTag = hObject.Tag                        % e.g.:  'edit_F0cv_GS'    
%     % reformatting to 2 digits
%     setStrTag(thisTag,num2str(str2num(getStrTag(thisTag)) ,'%1.2f'))    
%     % ------------------------------------------------------


end

