function edit_10Dq_FS_Callback(hObject, eventdata, handles)
% hObject    handle to edit_10Dq_FS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_10Dq_FS as text
%        str2double(get(hObject,'String')) returns contents of edit_10Dq_FS as a double

    % ----------------------------------------------------------------------
    debugMsg( 'calling edit_10Dq_FS_Callback()', 2 )
    % ----------------------------------------------------------------------
        
    reformatStringValue( hObject,handles, 3 )


end

