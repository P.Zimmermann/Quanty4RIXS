% --- Executes on button press in cb_FS.
function cb_FS_Callback(hObject, eventdata, handles)
% hObject    handle to cb_FS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_FS


    % -----------------------------------------------------    
    if (hObject.Value == 1)
        % enable FS edit boxes

        if (get(findobj('Tag','pm_Symmetry'),'Value')==1)
            tagEnable({'edit_10Dq_FS'})
            tagDisable({'edit_Ds_FS','edit_Dt_FS','edit_M_FS'})
        elseif (get(findobj('Tag','pm_Symmetry'),'Value')==2)
            tagEnable({'edit_10Dq_FS','edit_Ds_FS','edit_Dt_FS'})
            tagDisable({'edit_M_FS'})
        elseif (get(findobj('Tag','pm_Symmetry'),'Value')==3)
            tagEnable({'edit_10Dq_FS','edit_Ds_FS','edit_Dt_FS','edit_M_FS'})
        end
    
    else
        % disable all FS edit boxes
        tagDisable({'edit_10Dq_FS','edit_Ds_FS','edit_Dt_FS','edit_M_FS'})
        
        % copy values to FS when cb_FS is not checked
        set(findobj('Tag','edit_10Dq_FS'),'String',get(findobj('Tag','edit_10Dq_GS'),'String'))
        set(findobj('Tag','edit_Ds_FS'),'String',get(findobj('Tag','edit_Ds_GS'),'String'))
        set(findobj('Tag','edit_Dt_FS'),'String',get(findobj('Tag','edit_Dt_GS'),'String'))
        set(findobj('Tag','edit_M_FS'),'String',get(findobj('Tag','edit_M_GS'),'String'))
        
    end
    
    % -----------------------------------------------------  

    
    
    

end

