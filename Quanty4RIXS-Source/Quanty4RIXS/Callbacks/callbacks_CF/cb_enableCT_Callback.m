% --- Executes on button press in cb_enableCT.
function cb_enableCT_Callback(hObject, eventdata, handles)
% hObject    handle to cb_enableCT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_enableCT


    disp('cb_enableCT_Callback')
    
    % -----------------------------------------------------    
    if (hObject.Value == 1)
        enableCT()
    else
        disableCT()
    end       
    % -----------------------------------------------------  


end

