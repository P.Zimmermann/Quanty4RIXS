function cb_IS_Callback(hObject, eventdata, handles)
% --- Executes on button press in cb_IS.
% hObject    handle to cb_IS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_IS



    % -----------------------------------------------------    
    if (hObject.Value == 1)
        % enable FS edit boxes

        if (get(findobj('Tag','pm_Symmetry'),'Value')==1)            % Oh => oinly 10Dq
            tagEnable({'edit_10Dq_IS'})
            tagDisable({'edit_Ds_IS','edit_Dt_IS','edit_M_IS'})
        elseif (get(findobj('Tag','pm_Symmetry'),'Value')==2)        % D4h => 10Dq, Dt, Ds
            tagEnable({'edit_10Dq_IS','edit_Ds_IS','edit_Dt_IS'})
            tagDisable({'edit_M_FS'})
        elseif (get(findobj('Tag','pm_Symmetry'),'Value')==3)        % C4 => 10Dq, Dt, Ds, M
            tagEnable({'edit_10Dq_IS','edit_Ds_IS','edit_Dt_IS','edit_M_IS'})
        end
    
    else
        % disable all FS edit boxes
        tagDisable({'edit_10Dq_IS','edit_Ds_IS','edit_Dt_IS','edit_M_IS'})
        
        % copy values to FS when cb_FS is not checked
        set(findobj('Tag','edit_10Dq_IS'),'String',get(findobj('Tag','edit_10Dq_GS'),'String'))
        set(findobj('Tag','edit_Ds_IS'),'String',get(findobj('Tag','edit_Ds_GS'),'String'))
        set(findobj('Tag','edit_Dt_IS'),'String',get(findobj('Tag','edit_Dt_GS'),'String'))
        set(findobj('Tag','edit_M_IS'),'String',get(findobj('Tag','edit_M_GS'),'String'))
        
    end
    
    % -----------------------------------------------------  

    



end

