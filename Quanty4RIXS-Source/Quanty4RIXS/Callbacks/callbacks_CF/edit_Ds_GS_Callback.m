function edit_Ds_GS_Callback(hObject, eventdata, handles)
% hObject    handle to edit_Ds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_Ds as text
%        str2double(get(hObject,'String')) returns contents of edit_Ds as a double


    % ----------------------------------------------------------------------
    debugMsg( 'calling edit_Ds_GS_Callback()', 2 )
    % ----------------------------------------------------------------------
    
    
    reformatStringValue( hObject,handles, 3 )    
%     handles
%     % ------------------------------------------------------
%     thisTag = hObject.Tag                        % e.g.:  'edit_F0cv_GS'    
%     % reformatting to 2 digits
%     setStrTag(thisTag,num2str(str2num(getStrTag(thisTag)) ,'%1.2f'))    
%     % ------------------------------------------------------
%          

    
    % copy value to IS when cb_IS is not checked
    if (get(findobj('Tag','cb_IS'),'Value')==0 )
        set(findobj('Tag','edit_Ds_IS'),'String',hObject.String)
    end      
    % copy value to FS when cb_FS is not checked
    if (get(findobj('Tag','cb_FS'),'Value')==0 )
        set(findobj('Tag','edit_Ds_FS'),'String',hObject.String)
    end  
    
    % -----------------------------------
    % updating the CF TS for Dq Ds Dt
    updateTS_DqDtDs(  )
    % -----------------------------------
        
    
    
    
end

