function edit_Dt_GS_Callback(hObject, eventdata, handles)
% hObject    handle to edit_Dt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_Dt as text
%        str2double(get(hObject,'String')) returns contents of edit_Dt as a double

    disp('edit_Dt_Callback')
    
    reformatStringValue( hObject,handles, 3 )
%     % ------------------------------------------------------
%     thisTag = hObject.Tag                        % e.g.:  'edit_F0cv_GS'    
%     % reformatting to 2 digits
%     setStrTag(thisTag,num2str(str2num(getStrTag(thisTag)) ,'%1.2f'))    
%     % ------------------------------------------------------

    
    % copy value to IS when cb_IS is not checked
    if (get(findobj('Tag','cb_IS'),'Value')==0 )
        set(findobj('Tag','edit_Dt_IS'),'String',hObject.String)
    end  
    % copy value to FS when cb_FS is not checked
    if (get(findobj('Tag','cb_FS'),'Value')==0 )
        set(findobj('Tag','edit_Dt_FS'),'String',hObject.String)
    end      
    
    
    % -----------------------------------
    % updating the CF TS for Dq Ds Dt
    updateTS_DqDtDs(  )
    % -----------------------------------
        
    
    
end

