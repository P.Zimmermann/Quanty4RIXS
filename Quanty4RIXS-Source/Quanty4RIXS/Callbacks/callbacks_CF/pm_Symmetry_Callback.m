% --- Executes on selection change in pm_Symmetry.
function pm_Symmetry_Callback(hObject, eventdata, handles)
% hObject    handle to pm_Symmetry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pm_Symmetry contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pm_Symmetry


    hObject

    % ============================================================= 
    % deaflault, only overwritten below for C3
    setStrTag({'txt_Ds'},'Ds')
    setStrTag({'txt_Dt'},'Dt')    
    % ============================================================= 
    if (hObject.Value==1)
        disp(' Symmetry Oh selected')
        tagDisable({'edit_Ds_GS','edit_Dt_GS','edit_M_GS'})
        tagDisable({'edit_Ds_IS','edit_Dt_IS','edit_M_IS'})
        tagDisable({'edit_Ds_FS','edit_Dt_FS','edit_M_FS'})
        % setting unised values to 0.000 (for clarity)
        setStrTag({'edit_Ds_GS','edit_Dt_GS','edit_M_GS'},'0.000')          % setting unused values to 0.000 (for clarity)
        setStrTag({'edit_Ds_IS','edit_Dt_IS','edit_M_IS'},'0.000')          % setting unused values to 0.000 (for clarity)
        setStrTag({'edit_Ds_FS','edit_Dt_FS','edit_M_FS'},'0.000')          % setting unused values to 0.000 (for clarity)
%         set(findobj('Tag','edit_Ds_GS'),'String','0.000')
%         set(findobj('Tag','edit_Dt_GS'),'String','0.000')
%         set(findobj('Tag','edit_M_GS'),'String','0.000')
        % ------------------------------------------
%         if (get(findobj('Tag','cb_IS'),'Value') == 0)
%            tagDisable({'edit_Ds_IS','edit_Dt_IS','edit_M_IS'})
%            % setting unised values to 0.000 (for clarity)
%            set(findobj('Tag','edit_Ds_IS'),'String','0.000')
%            set(findobj('Tag','edit_Dt_IS'),'String','0.000')
%            set(findobj('Tag','edit_M_IS'),'String','0.000')
%         end    
%         % ------------------------------------------
%         if (get(findobj('Tag','cb_FS'),'Value') == 0)
%            tagDisable({'edit_Ds_FS','edit_Dt_FS','edit_M_FS'})
%            % setting unised values to 0.000 (for clarity)
%            set(findobj('Tag','edit_Ds_FS'),'String','0.000')
%            set(findobj('Tag','edit_Dt_FS'),'String','0.000')
%            set(findobj('Tag','edit_M_FS'),'String','0.000')
%         end 
    % ============================================================= 
    elseif (hObject.Value==2)
        disp(' Symmetry D4h selected')   
        tagEnable({'edit_Ds_GS','edit_Dt_GS'})  
        tagDisable({'edit_M_GS'})
        setStrTag({'edit_M_GS','edit_M_IS','edit_M_FS'},'0.000')            % setting unused values to 0.000 (for clarity)
%         set(findobj('Tag','edit_M_GS'),'String','0.000')   
        % ------------------------------------------
        if (get(findobj('Tag','cb_IS'),'Value') == 1)
           tagEnable({'edit_Ds_IS','edit_Dt_IS'})
           tagDisable({'edit_M_IS'})
%            set(findobj('Tag','edit_M_IS'),'String','0.000')           
        end  
        % ------------------------------------------
        if (get(findobj('Tag','cb_FS'),'Value') == 1)
           tagEnable({'edit_Ds_FS','edit_Dt_FS'})
           tagDisable({'edit_M_FS'})
%            set(findobj('Tag','edit_M_FS'),'String','0.000')           
        end       
    % =============================================================        
    elseif (hObject.Value==3)
        disp(' Symmetry C4 selected')
        tagEnable({'edit_Ds_GS','edit_Dt_GS','edit_M_GS'})
        if (get(findobj('Tag','cb_IS'),'Value') == 1)
           tagEnable({'edit_Ds_IS','edit_Dt_IS','edit_M_IS'})
        end 
        if (get(findobj('Tag','cb_FS'),'Value') == 1)
           tagEnable({'edit_Ds_FS','edit_Dt_FS','edit_M_FS'})
        end         
    % ============================================================= 
    elseif (hObject.Value==4)
        disp(' Symmetry C3v selected')
        tagEnable({'edit_Ds_GS','edit_Dt_GS','edit_M_GS'})
        if (get(findobj('Tag','cb_IS'),'Value') == 1)
           tagEnable({'edit_Ds_IS','edit_Dt_IS','edit_M_IS'})
        end 
        if (get(findobj('Tag','cb_FS'),'Value') == 1)
           tagEnable({'edit_Ds_FS','edit_Dt_FS','edit_M_FS'})
        end        
        setStrTag({'txt_Ds'},'Dsigma')
        setStrTag({'txt_Dt'},'Dtau')
    % =============================================================     
    else
        disp(' !!! Warning !!! This should never happen.')
    end   
    
    % -----------------------------------
    % updating the CF TS for Dq Ds Dt
    updateTS_DqDtDs(  )
    % -----------------------------------
    
    

end

