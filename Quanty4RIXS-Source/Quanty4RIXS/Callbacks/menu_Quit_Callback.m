function [ output_args ] = menu_Quit_Callback(hObject, eventdata, handles)
%MENU_SETTINGS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    debugMsg( 'calling menu_Quit_Callback()', 2 )

    if (getValTag({'pm_DebugLevel'}) - 1) > 1 
        display('  Callback_CloseGUI')
        hObject
        eventdata
        handles
    end;
   
    
    
    %  --------------------------------------------
%     % save to config file
%     if ~isequal(saveDir,0)
%         handles.appSettings.lastDir  = saveDir; 
%         settings.lastDir = saveDir;
%         save('Resources/ctm4rixs.cfg','settings')             
%     end       
%     %  --------------------------------------------
    
    
    
    
    %> questdlg is a close request function to display a question dialog box 
    selection = questdlg('Do you want to exit the program?',...
                    'Close progam',...
                    'Yes','No','Yes'); 

    switch selection, 
        case 'Yes',
            
%             delete(findobj('Tag','WorkDirData'));           % deletes the uitable 
            


            delete(gcf)
        case 'No'
            return 
    end




end

