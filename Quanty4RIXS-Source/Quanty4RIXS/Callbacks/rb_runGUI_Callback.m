function [ output_args ] = rb_runGUI_Callback( hObject, eventdata, handles )
%RB_RUNGUI_CALLBACK Summary of this function goes here
%   Detailed explanation goes here


    % -------------------------------------------------------
    debugMsg( 'calling rb_runGUI_Callback()', 2 )
    % -------------------------------------------------------
    
    % -----------------------------------------------
    % radiobutton does not toggle, and disables RIXSMCD
    set(hObject,'Value',1)
    set(findobj('Tag','rb_runLUA'),'Value',0)
    % -----------------------------------------------
    set(findobj('Tag','cb_calcExpectVals'), 'Enable','on')    
    set(findobj('Tag','cb_calcCFdiagram'), 'Enable','on')    
    set(findobj('Tag','cb_calcRIXSmaps'), 'Enable','on')    
    set(findobj('Tag','cb_Options'), 'Enable','on')    
    % -----------------------------------------------

    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------

end

