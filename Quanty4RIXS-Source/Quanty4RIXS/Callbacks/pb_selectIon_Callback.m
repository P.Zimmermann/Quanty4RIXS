% --- Executes on button press in pb_selectIon.
function pb_selectIon_Callback(hObject, eventdata, handles)
% hObject    handle to pb_selectIon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


    disp('pb_selectIon_Callback')
    
%     clearAtomicParameter( )                               % not reeded? - Not cleared when Select Z is clicked; clear is done when Z is selected
    
    uiPeriodicTable(hObject, eventdata, handles)
    


end

