% --- Executes on button press in cb_autoplot.
function cb_autoplot_Callback(hObject, eventdata, handles)
% hObject    handle to cb_autoplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_autoplot

    debugMsg( ' cb_autoplot_Callback()', 2 )


    % -------------------------------------------------------------------------------- 
    if getValTag({'cb_Plotting'}) == 0 && getValTag({'cb_autoplot'}) == 1
        showPanelPlotting();
    end
    % --------------------------------------------------------------------------------
   
    
    % --------------------------------------------
    % nothing changed here
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------
    
    

end

