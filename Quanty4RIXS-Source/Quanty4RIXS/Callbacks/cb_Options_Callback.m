% --- Executes on button press in cb_Options.
function cb_Options_Callback(hObject, eventdata, handles)
% hObject    handle to cb_Options (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb_Options

    % ---------------------------------------
    debugMsg( 'cb_Options_Callback()', 2 )
    % ---------------------------------------
  
    if hObject.Value == 1
        % --------------------------------------------
        uiOptions(hObject, handles.mainFigure, eventdata, handles)
        % --------------------------------------------
    else
        % --------------------------------------------
        handles = writeOptions2Handles( handles );
        
        %search and delete old existing Options figure
        hOld = findobj('Tag','uiFig_Options');
        delete(hOld);        
        % --------------------------------------------        
    end
  
    
    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % --------------------------------------------
    
    
    
end

