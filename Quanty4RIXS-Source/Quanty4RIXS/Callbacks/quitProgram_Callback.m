function [ output_args ] = quitProgram_Callback( hObject, eventdata, handles )
%> @file Callback_CloseGUI.m
%> Callback_CloseGUI() deletes the MainFigure when the program is closed. 
%> (this is necessary to avoid duplication of gui instances)

% TODO: remove, should be replaced everywhere with:       menu_Quit_Callback(hObject, eventdata, handles)


    debugMsg( 'calling quitProgram_Callback()', 2 )

   
    if (getValTag({'pm_DebugLevel'}) - 1) > 1 
        display('  Callback_CloseGUI')
        hObject
        eventdata
        handles
    end;
    
    
    %  --------------------------------------------
%     % save to config file
%     if ~isequal(saveDir,0)
%         handles.appSettings.lastDir  = saveDir; 
%         settings.lastDir = saveDir;
%         save('Resources/ctm4rixs.cfg','settings')             
%     end       
%     %  --------------------------------------------
    
    
   
    %> questdlg is a close request function to display a question dialog box 
    selection = questdlg('Do you want to exit the program?',...
                    'Close progam',...
                    'Yes','No','Yes'); 

    switch selection, 
        case 'Yes',
            % -------------------------------------------------------------------
%             delete(findobj('Tag','WorkDirData'));           % deletes the uitable 
            % -------------------------------------------------------------------
            updateStatus([' Shutting down Multithread Pool... ' ])          
            % -------------------------------------------------------------------
            % closing old POol !!! TESTING if always fine.
            poolobj = gcp('nocreate'); % If no pool, do not create new one.
            if ~isempty(poolobj)
                delete(poolobj)                     % closing/clearing multi kernel pool before TESTING if always ok.
            end
            % -------------------------------------------------------------------
            updateStatus([' Quitting Program... ' ])          
            % -------------------------------------------------------------------
            % delete GUI figure
            delete(gcf)
            % -------------------------------------------------------------------
        case 'No'
            return 
    end

end

