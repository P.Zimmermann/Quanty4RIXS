% --- Executes on button press in uirb_RIXS.
function rb_1s2pRIXS_Callback(hObject, eventdata, handles)
% hObject    handle to uirb_RIXS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of uirb_RIXS

    % -------------------------------------------------------
    debugMsg( 'calling rb_1s2pRIXS_Callback()', 1 )
    % -------------------------------------------------------
    
    % -----------------------------------------------
%     updateStatus(['1s2pRIXS spectroscopy selected'])              % redundant info!
    % -----------------------------------------------    
    handles.spectroscopy = '1s2pRIXS';    
    % -----------------------------------------------
    % radiobutton does not toggle, and disables RIXSMCD    
    set(hObject,'Value',1)                                      % radiobutton does not toggle, and disables RIXSMCD
    set(findobj('Tag','rb_2p3dRIXS'),'Value',0)                 %     set(findobj('Tag','rb_1s2pRIXSMCD'),'Value',0)
%     set(findobj('Tag','cb_Options'),'Value',0 ,'Enable','on')   %     set(findobj('Tag','rb_2p3dRIXSMCD'),'Value',0)   
    % -----------------------------------------------
    
    % --------------------------------------------------  
    % setting the general states for a spelected spectroscopy even if no Z is selected 
    setStrTag('uitxt_core_GS','1s2')
    setStrTag('uitxt_core_IS','1s1')
    setStrTag('uitxt_core_FS','2p5')

    setStrTag('uitxt_val_GS','3dN')
    setStrTag('uitxt_val_IS','3dN+1')
    setStrTag('uitxt_val_FS','3dN+1')
    % --------------------------------------------------  
    clearAtomicParameter( )                                     % TODO: this might be redundant: clearAtomic is also called in updateConfigPanel
%     % --------------------------------------------------  
%     if isfield(handles,'currentZ')
%         z = handles.currentZ;        
%         if z > 0 
%             handles = fillPMOxiLevel(z, handles );
%             % --------------------------------------------------
%             % update config Panel   
%             handles = updateConfigPanel(hObject, eventdata, handles);
%             % --------------------------------------------------
%         end
%     end
%     % --------------------------------------------------  
    % --------------------------------------------------  
    if isfield(handles,'currentZ')              % checking if a Z is selected
%         z = handles.currentZ;        
        % --------------------------------------------------
        % update config Panel   
        handles = updateConfigPanel(hObject, eventdata, handles);
        % --------------------------------------------------
    end
    % --------------------------------------------------  
                  
    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
      

    
end

