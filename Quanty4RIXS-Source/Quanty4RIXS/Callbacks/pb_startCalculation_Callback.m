% --- Executes on button press in uipb_startCalculation.
function pb_startCalculation_Callback(hObject, eventdata, handles)

    % -----------------------------------------------------
    debugMsg( 'calling pb_startCalculation_Callback()', 2 )
    % -----------------------------------------------------
    clear mapBaseName mapSaveDir
    
    if handles.appSettings.calcRIXSmaps == 1
        baseName = ['RIXS' handles.spectroscopy(1:4)];
    elseif handles.appSettings.calcXAS == 1
        baseName = ['XAS' handles.spectroscopy(1:2)];
    else
        baseName = ['Calc_' handles.spectroscopy(1:4)];
    end    
    % -----------------------------------------------------
    
    if getValTag('rb_runGUI') == 1
        % =========================================================
        [mapBaseName, mapSaveDir]  = uiputfile(...
                {'*.*','All Files' },...
                'Select Output Directory (give BaseName only, extension is added)...',...
                [handles.appSettings.lastDir baseName])    % suggest name RIXS1s2p or RIXS2p3d, NOTE: no filesep, because mapDaveDir end on a /
        if ~isequal(mapSaveDir,0)
            handles.appSettings.lastDir = mapSaveDir;               % update lastDir in handles
        end
        % ------------------------------------------------
        % write last Dir and all current App settings to file   
        saveAppSettingsFile( handles )
        % ------------------------------------------------          
        if ~isequal(mapSaveDir,0)
            %  --------------------------------------------
    %         mapSaveDir = [mapSaveDir filesep];
            % =========================================================     
            if isfield(handles,'currentZ')                      % check if data loaded             
                % ------------------------------------------------    
                % create here the comlete lua file (script for Quanty)
                handles = createLUA( handles, mapSaveDir, mapBaseName );
                % now in mapSaveDir mapBaseName the complete lua file should be ready now
                % ------------------------------------------------    
                askAndStart()
                % ------------------------------------------------    
            else
                warndlg('No data loaded! Nothing to do.','!! Message !!')
                updateStatus(['Nothing to do.'])                           
            end     
        end
        % =========================================================
    elseif getValTag('rb_runLUA') == 1
        % =========================================================
        [luaFile, mapSaveDir]  = uigetfile(...
                {'*_Quanty.lua','Quanty LUA script';'*.*','All Files' },...
                'Select LUA file to run (output will be stored in the same directory)...',...
                [handles.appSettings.lastDir ])    % suggest name RIXS1s2p or RIXS2p3d

        if ~isequal(mapSaveDir,0)
            handles.appSettings.lastDir = mapSaveDir;               % update lastDir in handles
        end
        % ------------------------------------------------
%         luaFile
        luaEnd = luaFile(end-3:end);
        if strcmp(luaEnd,'.lua')          
            % ------------------------------------------------    
            mapBaseName = luaFile;
            askAndStart()
            % ------------------------------------------------
        end
        clear luaEnd
        % ------------------------------------------------
        
    end    
    
    % --------------------------------------------
    % Update handles structure
    guidata(hObject, handles);              % NEEDED to READ/UPDATE new handles structure !!  (e.g. lastDir)
    % ========================================================= 
    
    % ========================================================= 
    % ========================================================= 
    function askAndStart()
    
            % --------------------------------------------------------- 
            updateStatus(['Quanty starting...'])   
            % ---------------------------------------------------------   
            if (handles.appSettings.dualThread == 1) && getValTag('rb_runGUI')
                infoString = {'*** Start MULTITHREAT Calculation ***'};
            else
                infoString = {'     *** Start Calculation ***'};
            end
            if ((handles.appSettings.calcEigenVals == 1) && (getValTag('rb_runGUI') == 1))
                infoString = {char(infoString{:});...
                    ['Observables for ' num2str(handles.appSettings.noOfEV) ' ground state(s).']};                                
            end
            if ((handles.appSettings.calcRIXSmaps == 1) && (getValTag('rb_runGUI') == 1))
                infoString = {char(infoString{:});...
                    ['RIXS for the lowest ' num2str(handles.appSettings.noOfGS) ' ground state(s).']};    
            elseif (getValTag('rb_runGUI') == 1)
                infoString = {char(infoString{:});...
                    ['NO RIXS spectra will be calculated.']};                    
            end
            if ((handles.appSettings.calcXAS == 1) && (getValTag('rb_runGUI') == 1))
                infoString = {char(infoString{:});...
                    ['XAS for the lowest ' num2str(handles.appSettings.noOfGS) ' ground state(s).']};              
            end
            % questdlg with yes/no options
            choice = questdlg(infoString,...
                'Start Quanty engine?', ...
                'Yes','No','No');

            % ------------------------------------------------
            switch choice
                case 'Yes'                    
                    % call the Quanty runner function
                    handles = runQuanty(handles, mapSaveDir, mapBaseName );
                    
                case 'No'
                    disp([' Aborted.'])
                    updateStatus(['Aborted.'])                           
                    
            end
            
            
    
    end

end

