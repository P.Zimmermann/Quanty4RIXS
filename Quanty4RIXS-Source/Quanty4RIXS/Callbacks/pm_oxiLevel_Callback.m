function [ output_args ] = pm_oxiLevel_Callback( hObject, eventdata, handles )
%PM_OXILEVEL_CALLBACK Summary of this function goes here
%   Detailed explanation goes here


    % --------------------------------------------------
    % get index and string from selected IonString Listbox
    idx = get(hObject,'Value');
    ionStrList = get(hObject,'String');
    handles.currIonStr = ionStrList{idx};
    % --------------------------------------------------

    % --------------------------------------------------
    % update config Panel
    handles = updateConfigPanel(hObject, eventdata, handles);
    
    % --------------------------------------------------
    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
    % --------------------------------------------------

    
end

