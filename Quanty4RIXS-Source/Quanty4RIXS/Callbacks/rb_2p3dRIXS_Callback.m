% --- Executes on button press in uirb_RIXS.
function rb_2p3dRIXS_Callback(hObject, eventdata, handles)
% hObject    handle to uirb_RIXS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of uirb_RIXS

    % -------------------------------------------------------
    debugMsg( 'calling rb_2p3dRIXS_Callback()', 2 )
    % -------------------------------------------------------
    
    % -----------------------------------------------
%     updateStatus(['2p3dRIXS spectroscopy selected'])      % redundant info! The GUI shows it
    % -----------------------------------------------
    handles.spectroscopy = '2p3dRIXS'; 
    % -----------------------------------------------
    % radiobutton does not toggle, and disables RIXSMCD
    set(hObject,'Value',1)
    set(findobj('Tag','rb_1s2pRIXS'),'Value',0)
%     set(findobj('Tag','cb_Options'),'Value',0 ,'Enable','off')    
    % -----------------------------------------------

    % --------------------------------------------------
    % setting the general states for a spelected spectroscopy even if no Z is selected     
    setStrTag('uitxt_core_GS','2p6')
    setStrTag('uitxt_core_IS','2p5')
    setStrTag('uitxt_core_FS','2p6')

    setStrTag('uitxt_val_GS','3dN')
    setStrTag('uitxt_val_IS','3dN+1')
    setStrTag('uitxt_val_FS','3dN')
    % --------------------------------------------------  
    clearAtomicParameter( )
    % --------------------------------------------------  
    % TESTING: Removed fillPMOxiLevel()  to keep current selection of the Ion
%     if isfield(handles,'currentZ')
%         z = handles.currentZ;
%         if z > 0
%             handles = fillPMOxiLevel(z, handles );
%             % --------------------------------------------------
%             % update config Panel      
%             handles = updateConfigPanel(hObject, eventdata, handles);
%             % --------------------------------------------------    
%         end
%     end
%     % --------------------------------------------------  
    % --------------------------------------------------  
    if isfield(handles,'currentZ')          % using Z to check if soemthing is selected
%         z = handles.currentZ;
        % --------------------------------------------------
        % update config Panel      
        handles = updateConfigPanel(hObject, eventdata, handles);
        % --------------------------------------------------    
    end
    % --------------------------------------------------  
       
    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
         


end

