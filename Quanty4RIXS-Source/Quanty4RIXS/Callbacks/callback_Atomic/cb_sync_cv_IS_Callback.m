function [ output_args ] = cb_sync_cv_IS_Callback( hObject, eventdata, handles)
%CB_SYNC_CV_IS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    tagList = {'edit_F2cv_IS','edit_G0cv_IS','edit_G2cv_IS'};

    if hObject.Value == 0
        tagEnable( tagList )
    else
        tagDisable( tagList )  
        syncEditBox(hObject,handles, tagList )
    end
    
    
    

end

