function [ output_args ] = cb_sync_vv_IS_Callback(hObject, eventdata, handles)
%CB_SYNC_VV_IS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    tagList = {'edit_F2vv_IS','edit_F4vv_IS'};
    
    if hObject.Value == 0
        tagEnable( tagList )
    else
        tagDisable( tagList )  
        syncEditBox(hObject,handles, tagList )
    end
    

end

