function [ output_args ] = cb_sync_cv_GS_Callback( hObject, eventdata, handles)
%CB_SYNC_CV_GS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    tagList = {'edit_F2cv_GS','edit_G0cv_GS','edit_G2cv_GS'};
   
    if hObject.Value == 0
        tagEnable( tagList )
    else
        tagDisable( tagList )  
        syncEditBox(hObject,handles, tagList )
    end
    
    


end

