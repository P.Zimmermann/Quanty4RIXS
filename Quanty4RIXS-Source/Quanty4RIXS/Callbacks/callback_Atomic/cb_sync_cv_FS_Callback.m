function [ output_args ] = cb_sync_cv_FS_Callback( hObject, eventdata, handles)
%CB_SYNC_CV_FS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    tagList = {'edit_F2cv_FS','edit_G0cv_FS','edit_G2cv_FS'};

    if hObject.Value == 0
        tagEnable( tagList )
    else
        tagDisable( tagList )  
        syncEditBox(hObject,handles, tagList )
    end

end

