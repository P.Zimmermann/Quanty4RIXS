function [ output_args ] = cb_sync_vv_GS_Callback(hObject, eventdata, handles)
%CB_SYNC_VV_GS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    tagList = {'edit_F2vv_GS','edit_F4vv_GS'};
        
    if hObject.Value == 0
        tagEnable( tagList )
    else
        tagDisable( tagList )  
        syncEditBox(hObject,handles, tagList )
    end

    

end

