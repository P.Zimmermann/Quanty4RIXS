function [ output_args ] = edit_Slater_Callback( hObject, eventdata, handles)
%EDIT_F0CV_GS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here


    % ------------------------------------------------------------------------------------------------
    debugMsg( 'calling edit_Slater_Callback()', 2 )
    % ------------------------------------------------------------------------------------------------

    % ------------------------------------------------------
    thisTag = hObject.Tag;                        % e.g.:  'edit_F0cv_GS'
    
%     handles
   
    
    writeTag = ['txtVal_' thisTag(6:end)];       % this should enable for common CALLback function, labels shoudl be adapted here correctly !??
    dataHandle = ['configStr' thisTag(end-1:end)];
    
    
    debugMsg(['Debug in edit_Slater_Callback():  thisTag=' thisTag '  writeTag=' writeTag '  dataHandle=' dataHandle],1)
    
    
    if isfield(handles,dataHandle)   
        
        edNum = str2num(hObject.String);
        
        % === Only one number ================================================
        if length(edNum) == 1             
            % ------------------------------------------------------
            % reformatting to 2 digits
            setStrTag(thisTag,num2str(str2num(getStrTag(thisTag)) ,'%1.2f'))    
            % ------------------------------------------------------

            % -------------------------------------------------------------
            % scaling the value of corresponding parameter
            debugMsg(['Number entered: ' num2str(edNum) '    Tag: ' thisTag],1) % scale Value to given percentage
            scaleValue( hObject, handles )
            % -------------------------------------------------------------
            
            
            % -------------------------------------------------------------
            % sync the other 2 edit boxes when the corresponding cb sync button is on
            syncTag = ['cb_sync_' thisTag(end-4:end)];                            % syncTag = 'cb_sync_vv_IS'
            if getValTag({syncTag}) && strcmp( thisTag(end-4:end-3),'cv')
                % tagList = {'edit_G0cv_FS','edit_G2cv_FS'}                
                toSyncTagList = {['edit_F2cv_' thisTag(end-1:end)],['edit_G0cv_' thisTag(end-1:end)],['edit_G2cv_' thisTag(end-1:end)]} ;      
                syncEditBox(hObject,handles, toSyncTagList )
            elseif getValTag({syncTag}) && strcmp( thisTag(end-4:end-3),'vv')
                % tagList = {'edit_F2vv_FS','edit_F4vv_FS'}
                toSyncTagList = {['edit_F2vv_' thisTag(end-1:end)],['edit_F4vv_' thisTag(end-1:end)]};
                syncEditBox(hObject,handles, toSyncTagList )
                
            end
            % -------------------------------------------------------------
            % -------------------------------------------------------------
            
         
            
        % === Array/List of numbers entered ================================================
        elseif length(edNum) > 1 
            
            % Construct a questdlg with three options
            choice = questdlg(['List of values entered!!'], ...
                'List Entered', ...
                'Accept','Revert','Revert');
            % Handle response
            switch choice
                case 'Accept'
                    debugMsg([choice 'ing list values'],1)
                    
                    disp(['List ' edNum ' entered.'])   % no NOT scale Value, put 100% 
                    disp(edNum)
                    
                case 'Revert'
                    disp([choice 'ing to 1.00'])
                    set(hObject,'String','1.00')
            end
 
        end    
        % ------------------------------------------------------------------------------
    else
       disp('No Data loaded') 
    end    
    
    
    % ----------------------------------------------------------------------------------
  
    % --------------------------------------------------
    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
    % --------------------------------------------------
    
  
    
    % ----------------------------------------------------------------------------------
       

end

