function [ output_args ] = edit_SOCc_FS_Callback( hObject, eventdata, handles)
%EDIT_SOCC_FS_CALLBACK Summary of this function goes here
%   Detailed explanation goes here


    % ------------------------------------------------------
    thisTag = hObject.Tag; 
    
    % reformatting to 2 digits
    setStrTag(thisTag,num2str(str2num(getStrTag(thisTag)) ,'%1.2f'))    
    % ------------------------------------------------------
      
    dataHandle = ['configStr' thisTag(end-1:end)];
 
    
    debugMsg(['Debug in edit_SOCc_FS_Callback():  thisTag=' thisTag '  dataHandle=' dataHandle],1)
    
    
    if isfield(handles,dataHandle)   
        
        edNum = str2num(hObject.String);
        
        % --- Only one number ----------------------------------------------------------
        if length(edNum) == 1 
            debugMsg(['Number ' num2str(edNum) ' entered. Tag: ' thisTag],1) % scale Value to given percentage
            
            scaleValue( hObject, handles )
                        
        % --- Array/List of numbers entered --------------------------------------------
        elseif length(edNum) == 1 
            debugMsg(['List ' num2str(edNum) ' entered. --- in edit_SOCc_FS_Callback() '],0)   % no NOT scale Value, put 100% 
            disp(edNum)
        
        % --- ELSE wrong values entered ------------------------------------------------
        else
            disp('ELSE... edNum:  --- in edit_SOCc_FS_Callback() ')
            disp(edNum)
        end    
        % ------------------------------------------------------------------------------
    else
       disp('No Data loaded') 
    end    
    
    

end

