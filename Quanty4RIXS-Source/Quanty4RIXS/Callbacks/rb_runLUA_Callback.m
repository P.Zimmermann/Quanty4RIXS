function [ output_args ] = rb_runLUA_Callback( hObject, eventdata, handles )
%RB_RUNLUA_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------------
    debugMsg( 'calling rb_runLUA_Callback()', 2 )
    % -------------------------------------------------------
 
    % -----------------------------------------------
    % radiobutton does not toggle, and disables RIXSMCD
    set(hObject,'Value',1)
    set(findobj('Tag','rb_runGUI'),'Value',0)
    % -----------------------------------------------    
%     set(findobj('Tag','cb_Options'),'Value',0 ,'Enable','off')    
    set(findobj('Tag','cb_calcExpectVals'), 'Enable','off')    
    set(findobj('Tag','cb_calcCFdiagram'), 'Enable','off')        
    set(findobj('Tag','cb_calcRIXSmaps'), 'Enable','off')    
    set(findobj('Tag','cb_Options'), 'Enable','off')    
    % -----------------------------------------------

    % --------------------------------------------------
    % Update handles structure and write it to the GUI
    guidata(findobj('Tag','mainFigure'), handles);
    % --------------------------------------------------
         
end

