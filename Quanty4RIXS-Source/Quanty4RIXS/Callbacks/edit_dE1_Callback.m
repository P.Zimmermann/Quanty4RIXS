function [ output_args ] = edit_dE1_Callback(hObject, eventdata, handles)
%EDIT_DE1_CALLBACK Summary of this function goes here
%   Detailed explanation goes here

    % -------------------------------------------------
    reformatStringValue( hObject,handles, 3 )
    % -------------------------------------------------
    
    % TodO: add check that dE is <0.1 (should be smaller than GAMMA)
    
    % -------------------------------------------------
    % sync value to edit_dE2
    setStrTag('edit_dE2',hObject.String)
    % -------------------------------------------------
    
    
    
    
    
    % -------------------------------------------------
    % Run a check here for the size of the map (also check if min < max)
    checkEnergyRanges( handles )
    % -------------------------------------------------



end

