function [ output_args ] = createLUAParams(handles, mapSaveDir, luaBaseName, mapBaseName, paraGS, paraIS, paraFS, paraRanges )
%CREATEPARAMLUAFILE Summary of this function goes here
%   Detailed explanation goes here

    % --------------------------------------------------------------------
    debugMsg( 'calling createParamLUAfile()', 2 )
    % --------------------------------------------------------------------
%     quantyDir = ['Quanty' filesep]       
%     saveFullFileLua = [handles.appRootDir filesep 'Quanty' filesep saveFName ]
    saveFullFileLua = [mapSaveDir filesep luaBaseName '_Quanty.lua' ];
    % --------------------------------------------------------------------  
    debugMsg(['Saving config to ' saveFullFileLua], 1 )
    % --------------------------------------------------------------------  
%     paraGS
%     paraIS
%     paraFS 
    % --------------------------------------------------------------------  
    % Extract field data
    fieldsGS = repmat(fieldnames(paraGS), numel(paraGS), 1);
    valuesGS = struct2cell(paraGS)   
    % Extract field data
    fieldsIS = repmat(fieldnames(paraIS), numel(paraIS), 1);
    valuesIS = struct2cell(paraIS);   
    % Extract field data
    fieldsFS = repmat(fieldnames(paraFS), numel(paraFS), 1);
    valuesFS = struct2cell(paraFS);   
    % --------------------------------------------------------------------  
    % Extract field data
    fieldsEnergy = repmat(fieldnames(paraRanges), numel(paraRanges), 1);
    valuesEnergy = struct2cell(paraRanges);             
    % --------------------------------------------------------------------  
    % --------------------------------------------------------------------   
     % Write parameter to lua file
    fid = fopen(saveFullFileLua, 'wt');
    fprintf(fid, ['-- ****************************************************************************************' '\n']);
    fprintf(fid, ['-- *** Adapted from the QUANTY documentation                                            *** ' '\n']);
    fprintf(fid, ['-- *** by Myrtille Hunault  (UU, April 2016)                                            *** ' '\n']);
    fprintf(fid, ['-- *** and Patric Zimmermann (UU, April 2016)                                           *** ' '\n']);
    fprintf(fid, ['-- ****************************************************************************************' '\n']);
    fprintf(fid, ['-- *** This file was created with Quanty4RIXS                                           *** ' '\n']);
    fprintf(fid, ['-- *** report bugs to P.Zimmermann@uu.nl                                                *** ' '\n']);
    fprintf(fid, ['-- ****************************************************************************************' '\n']);
    fprintf(fid, ['-- *** BEGIN Quanty4RIXS Parameter                                                      *** ' '\n']);
    % -------------------------------------------------------------------- 
    fprintf(fid, ['--- Spectroscopy: ' handles.spectroscopy, ' --- \n']);    
    % -------------------------------------------------------------------- 
    % Common Data. Line1 is IonsStr, Line 2 is Z, Line 3 EleConfig
    line1 = [char(fieldsGS{1}) ' = "' num2str(valuesGS{1}) '"'];
    fprintf(fid, [line1, '\n']);
    line2 = [char(fieldsGS{2}) ' = "' num2str(valuesGS{2}) '"'];    
    fprintf(fid, [line2, '\n']);
    % -------------------------------------------------------------------- 
    % NOTE: skip fields (Ion and Z and Sepctroscopy and eleConfig stays the same)
    skipFields = 4;
    % --------------------------------------------------------------------    
    % GS values
    fprintf(fid, ['--- Ground State ---' '\n']);
    line3 = [char(fieldsGS{skipFields}) ' = "' num2str(valuesGS{skipFields}) '"'];    
    eleConfGS = strsplit(valuesGS{skipFields},' ');                             % get eleconfig for GS                        
    fprintf(fid, [line3, '\n']);
    for k=[skipFields+1:length(valuesGS)]                                      % skipping line 1 and 2 (Ion and Z stays the same)
        % check for F0cv or G0vv and replace c and v in string
        tmpLabel = char(fieldsGS{k});
        if ( strcmp(tmpLabel(1),'F') || strcmp(tmpLabel(1),'G')) && ( strcmp(tmpLabel(3),'c') ||...
              strcmp(tmpLabel(4),'c') ||...
              strcmp(tmpLabel(3),'v') ||...
              strcmp(tmpLabel(4),'v') )
            tmpLabel = [ tmpLabel(1:2) '_' tmpLabel(3:end) ];               % add missing underscore manually
            tmpLabel = strrep(tmpLabel,'c',eleConfGS{1}(1:2));            % replace 'c' with core eleString
            tmpLabel = strrep(tmpLabel,'v',eleConfGS{2}(1:2));            % replace 'v' with valence eleString        
        end    
%         thisLine = [char(fieldsGS{k}) ' = ' num2str(valuesGS{k})];
        thisLine = [tmpLabel ' = ' num2str(valuesGS{k})];                   % write Label to file (F0cv will be corrected with 1s3d)
        fprintf(fid, [thisLine, '\n']);
    end
    clear tmpLabel    
    % --------------------------------------------------------------------    
    % IS values
    fprintf(fid, ['--- Intermediate State ---' '\n']);   
    line3 = [char(fieldsIS{skipFields}) ' = "' num2str(valuesIS{skipFields}) '"'];
    eleConfIS = strsplit(valuesGS{skipFields},' ');                             % get eleconfig for IS                        
    fprintf(fid, [line3, '\n']);
    for k=[skipFields+1:length(valuesIS)]                                      % skipping line 1 and 2 (Ion and Z stays the same)
        % check for F0cv or G0vv and replace c and v in string
        tmpLabel = char(fieldsIS{k});
        if ( strcmp(tmpLabel(1),'F') || strcmp(tmpLabel(1),'G')) && ( strcmp(tmpLabel(3),'c') ||...
              strcmp(tmpLabel(4),'c') ||...
              strcmp(tmpLabel(3),'v') ||...
              strcmp(tmpLabel(4),'v') )
            tmpLabel = [ tmpLabel(1:2) '_' tmpLabel(3:end) ];               % add missing underscore manually
            tmpLabel = strrep(tmpLabel,'c',eleConfGS{1}(1:2));            % replace 'c' with core eleString
            tmpLabel = strrep(tmpLabel,'v',eleConfGS{2}(1:2));            % replace 'v' with valence eleString        
        end  
%         thisLine = [char(fieldsIS{k}) ' = ' num2str(valuesIS{k})];
        thisLine = [tmpLabel ' = ' num2str(valuesIS{k})];                    % write Label to file (F0cv will be corrected with 1s3d)
        fprintf(fid, [thisLine, '\n']);
    end 
    clear tmpLabel
    % --------------------------------------------------------------------        
    % FS values
    fprintf(fid, ['--- Final State ---' '\n']);    
    fprintf(fid, ['--- !!! NOTE: Currently Quanty uses for 2d3dRIXS FS = GS !!!' '\n']);    
    line3 = [char(fieldsFS{skipFields}) ' = "' num2str(valuesFS{skipFields}) '"'];
    eleConfFS = strsplit(valuesGS{skipFields},' ');                             % get eleconfig for FS                            
    fprintf(fid, [line3, '\n']);
    for k=[skipFields+1:length(valuesFS)]                                      % skipping line 1 and 2 (Ion and Z stays the same)
        % check for F0cv or G0vv and replace c and v in string
        tmpLabel = char(fieldsFS{k});
        if ( strcmp(tmpLabel(1),'F') || strcmp(tmpLabel(1),'G')) && ( strcmp(tmpLabel(3),'c') ||...
              strcmp(tmpLabel(4),'c') ||...
              strcmp(tmpLabel(3),'v') ||...
              strcmp(tmpLabel(4),'v') )
            tmpLabel = [ tmpLabel(1:2) '_' tmpLabel(3:end) ];               % add missing underscore manually
            tmpLabel = strrep(tmpLabel,'c',eleConfGS{1}(1:2));            % replace 'c' with core eleString
            tmpLabel = strrep(tmpLabel,'v',eleConfGS{2}(1:2));            % replace 'v' with valence eleString        
        end                 
%         thisLine = [char(fieldsFS{k}) ' = ' num2str(valuesFS{k})];
        thisLine = [tmpLabel ' = ' num2str(valuesFS{k})];           % write Label to file (F0cv will be corrected with 1s3d)
        fprintf(fid, [thisLine, '\n']);
    end    
    clear tmpLabel    
    % --------------------------------------------------------------------    
     % Writing Energy ranges to file
    fprintf(fid, ['--- Energy Ranges ---' '\n']);    
    for k=[1:length(valuesEnergy)]
        thisLine = [char(fieldsEnergy{k}) ' = ' num2str(valuesEnergy{k})];
        fprintf(fid, [thisLine, '\n']);
    end    
    % --------------------------------------------------------------------    
    % Writing SavePath and saveBaseName to file
    % Note: https://nl.mathworks.com/matlabcentral/newsreader/view_thread/249727
    
    mapSaveDir = strrep(mapSaveDir,'\','\\');            % replace '\' with '\\'      -- Needed for windows, should not have any effect in *nix
    
    fprintf(fid, ['--- Saving Data to... ---' '\n']);    
    fprintf(fid, '%s', ['mapSavePath = "' mapSaveDir ]);    
    fprintf(fid, '"\n');    
    fprintf(fid, '%s', ['mapBaseName = "' mapBaseName ]);        
    fprintf(fid, '"\n');    
    fprintf(fid, ['--- END Quanty4RIXS Parameter                    ---' '\n']);
    % --------------------------------------------------------------------
    % --------------------------------------------------------------------
    fclose(fid);            % closing file
    % --------------------------------------------------------------------
    % --------------------------------------------------------------------
%     updateStatus([' Parameter Lua saved to: ' saveFullFileLua])
    % --------------------------------------------------------------------
    % --------------------------------------------------------------------
  

    
    
    


end

