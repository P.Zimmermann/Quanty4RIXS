function [ outVal ] = getValFromConfigStr( handles, label, stateStr )
%GETVALTAG Summary of this function goes here
%   Detailed explanation goes here

    % --------------------------------------------------
    debugMsg( 'calling readAllValuesFromGUI()', 2 )
    % --------------------------------------------------

    outVal = 0.0;

    if isfield(handles,['configStr' stateStr] )             % e.g. 'configStrGS'   
        configStrCells = handles.(['configStr' stateStr]);
        
        if length(configStrCells) > 4 
            strCore = configStrCells{2}(1:2);  
            strVal  = configStrCells{3}(1:2);
                
            for i=5:length(configStrCells)
                thisCell = configStrCells{i};
                thisCellParts = strsplit(thisCell,{'=','eV'});      % split cellData at '=' and 'eV'
                thisCellLabel =   char(thisCellParts(1));                
                thisCellValue = str2num(char(thisCellParts(2)));    % returns raw unscaled Value from configStrGS _IS, _FS   
                if strcmp(label(1:4),'SOCc') && strcmp(thisCellLabel,['SOC_' strCore])
                    outVal = thisCellValue;
                elseif strcmp(label(1:4),'SOCv')  && strcmp(thisCellLabel,['SOC_' strVal])
                    outVal = thisCellValue;
                elseif strcmp(thisCellLabel,label)
                    outVal = thisCellValue;
                end     
                
                
            end
                
                
        end
    else
        
        updateStatue(['No Data Loaded'])
        
    end    
       
    
     
    
    
    
end

