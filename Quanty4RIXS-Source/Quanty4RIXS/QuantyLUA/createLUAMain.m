function [ output_args ] = createLUAMain( handles, mapSaveDir, luaBaseName, mtTags )

    % NOTE: mtTags = [1 1 1] is used as 'multithread Tag', normal mode is mutithread OFF, and all on: [1 1 1] 
    % selections for Observables and spectra are made with the GUI. 
    % in MULTITHREAD MODE: [1 0 0] = Observables on; [0 1 0] = LCP spectrum; [0 0 1] = RCP spectrum   
    % DEBUG:  mtTags = [1 1 1];

    % ------------------------------------------------------------------------------------------------
    debugMsg( 'calling createLUAMain()', 2 )    
    % ------------------------------------------------------------------------------------------------
    % ------------------------------------------------------------------------------------------------
    % OPTIONS as set in the GUI, are read here from handles.options    
%     noOfEigenstates = 45;
%     noOfGSToPlot    = 2;
%     noOfEigenstates = handles.options.noOfEV;
%     noOfGSToPlot    = handles.options.noOfGS;
    noOfEigenstates = handles.appSettings.noOfEV;
    noOfGSToPlot    = handles.appSettings.noOfGS;    
    vecBx = handles.appSettings.Bx;
    vecBy = handles.appSettings.By;
    vecBz = handles.appSettings.Bz;
    normVal = (vecBx*vecBx+vecBy*vecBy+vecBz*vecBz)^0.5;

    disp(['Magnetic field vector : B = (' num2str(vecBx) ', ' num2str(vecBy) ', ' num2str(vecBz)  ')    with  B || k.' ])

    % ------------------------------------------------------------------------------------------------
    % ------------------------------------------------------------------------------------------------   
    % assamble full file name for the lua file (parameter head exists already)
    saveFullFileLua = [mapSaveDir filesep luaBaseName '_Quanty.lua' ];    
    % ------------------------------------------------------------------------------------------------       
    fidOut = fopen(saveFullFileLua, 'at');
    fprintf(fidOut, ['-- **************************************************************************************** ' '\n']);    
    fprintf(fidOut, ['-- number of 3d electrons : eleN_valence_GS  ' '\n']);    
    fprintf(fidOut, ['-- Lifetime broadening    :  L_IS and L_FS  ' '\n']);    
    fprintf(fidOut, ['-- NOTE: no experimental broadening applied by Quanty!  ' '\n']);        
    fprintf(fidOut, ['-- **************************************************************************************** ' '\n']);    
    fprintf(fidOut, ['-- Slater ********************************************************************************* ' '\n']);    
    fprintf(fidOut, ['Udd =  0.000			-- Groundstate: 3d-3d - in crystal field theory U drops out of the equation  ' '\n']);    
    fprintf(fidOut, ['Upd =  0.000    		-- Final state: 2p-3d  ' '\n']);    
    fprintf(fidOut, ['-- *** NOTE: If uncomented F0 value from the GUI is OVERWRITTEN here ***   ' '\n']);    
    fprintf(fidOut, [' F0_3d3d_GS = Udd+(F2_3d3d_GS+F4_3d3d_GS)*2/63         -- F0 3d-3d Slater for ground state (GS)  ' '\n']);    
    fprintf(fidOut, [' F0_3d3d_IS = Udd+(F2_3d3d_IS+F4_3d3d_IS)*2/63         -- F0 3d-3d Slater for intermediate state (IS)  ' '\n']);    
    fprintf(fidOut, [' F0_3d3d_FS = Udd+(F2_3d3d_FS+F4_3d3d_FS)*2/63         -- F0 3d-3d Slater for Final state (FS)  ' '\n']);    
    fprintf(fidOut, ['  ' '\n']);         
    fprintf(fidOut, [' F0_2p3d_FS = Upd + G1_2p3d_FS*1/15 + G3_2p3d_FS*3/70	-- Final state: 2p-3d  ' '\n']);    
    fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);    
    fprintf(fidOut, ['-- CF / Magnetic field (to remove the degeneracy with Zeeman spliting)  ' '\n']);    
    fprintf(fidOut, ['-- only for paramagnets, for FM the own magn. moment is up 1000x stronger than external field.  ' '\n']);    
    fprintf(fidOut, ['--Bx = TeslaToeV * Bx;     --TeslaToeV = 5.788E-5;   ' '\n']);    
    fprintf(fidOut, ['Bx = ' num2str(vecBx/normVal) ' * M_GS / 1000 ; -- mag X field (meV in GUI to eV)     ' '\n']);    
    fprintf(fidOut, ['By = ' num2str(vecBy/normVal) ' * M_GS / 1000 ; -- mag Y field (meV in GUI to eV)     ' '\n']);    
    fprintf(fidOut, ['Bz = ' num2str(vecBz/normVal) ' * M_GS / 1000 ; -- mag Z field (meV in GUI to eV)     ' '\n']);   
    fprintf(fidOut, ['                                                                 ' '\n']);   
    fprintf(fidOut, ['-- Define incident direction kin here                                                                ' '\n']);   
    fprintf(fidOut, ['-- kin || B for nonzero M                                                          ' '\n']);   
    fprintf(fidOut, ['if M_GS == 0 then	                                                                 ' '\n']);   
    fprintf(fidOut, ['	kin = {' num2str(vecBx/normVal) ',' num2str(vecBy/normVal) ',' num2str(vecBz/normVal) '}    										-- k in parall Z                                                                 \n']);   
    fprintf(fidOut, ['else                                                                 ' '\n']);   
    fprintf(fidOut, ['	kin = {1000*Bx/M_GS, 1000*By/M_GS, 1000*Bz/M_GS}        -- k incident direction                                                                 \n']);   
    fprintf(fidOut, ['end                                                                 ' '\n']);   
    fprintf(fidOut, ['                                                                 ' '\n']);       
    fprintf(fidOut, ['                                                                                              ' '\n']);   
    fprintf(fidOut, ['-- define 2 polarisations perp to kin                                     ' '\n']);   
    fprintf(fidOut, ['eps1 = {kin[2], -kin[1]-kin[3], kin[2]}		          -- polarisation 1                     ' '\n']);   
    fprintf(fidOut, ['                                                                                              ' '\n']);   
    fprintf(fidOut, ['-- cross product generates eps2                                                               ' '\n']);   
    fprintf(fidOut, ['epx = kin[2]*eps1[3] - kin[3]*eps1[2]                                                         ' '\n']);   
    fprintf(fidOut, ['epy = kin[3]*eps1[1] - kin[1]*eps1[3]                                                         ' '\n']);   
    fprintf(fidOut, ['epz = kin[1]*eps1[2] - kin[2]*eps1[1]                                                         ' '\n']);   
    fprintf(fidOut, ['eps2 = {epx,epy,epz}                           -- polarisation 2 : !!! perp to eps1 || kin    ' '\n']);   
    fprintf(fidOut, ['                                                                     ' '\n']);   
    fprintf(fidOut, ['-- ********************************************************************************************************************************************************************************   ' '\n']);    
    fclose(fidOut);            % closing file
    % ================================================================================================       
    % ================================================================================================    
    % -------------------------------------------------------------------------    
    if strcmp(handles.spectroscopy, '1s2pRIXS')
        % ----------------------------------------------------------------------------------------------------
        appendLuaModule( handles, saveFullFileLua, ['RIXS1s2p_Operators.lua'] )    
        % ----------------------------------------------------------------------------------------------------
        fidOut = fopen(saveFullFileLua, 'at');   
        fprintf(fidOut, ['-- ********************************************************************************************************************************************************************************  ' '\n']);    
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['-- Number of Eigenstates  ' '\n']);    
        fprintf(fidOut, ['Npsi=' num2str(noOfEigenstates) '\n']);    
        fprintf(fidOut, ['  ' '\n']);  
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['-- in order to make sure we have a filling of 2 electrons we need to define some restrictions  ' '\n']);            
        fprintf(fidOut, ['GS_Restrictions = {NFermion, NBoson, {"11 000000 0000000000",2,2}, {"00 111111 0000000000",6,6}, {"00 000000 1111111111",eleN_valence_GS,eleN_valence_GS}}; 	-- here: 1s2 2p6 3dn        ' '\n']); 
        fprintf(fidOut, ['IS_Restrictions = {NFermion, NBoson, {"11 000000 0000000000",1,1}, {"00 111111 0000000000",6,6}, {"00 000000 1111111111",eleN_valence_GS+1,eleN_valence_GS+1}}; -- here: 1s1 2p6 3dn+1    ' '\n']); 
        fprintf(fidOut, ['FS_Restrictions = {NFermion, NBoson, {"11 000000 0000000000",2,2}, {"00 111111 0000000000",5,5}, {"00 000000 1111111111",eleN_valence_GS+1,eleN_valence_GS+1}}; -- here: 1s2 2p5 3dn+1    ' '\n']); 
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['  ' '\n']);  
        % --------------------------------------------------
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);    
        fclose(fidOut);                      % closing file
        % -------------------------------------------------------------------------  
    elseif strcmp(handles.spectroscopy, '2p3dRIXS')  
        % ----------------------------------------------------------------------------------------------------
        if (getValTag('pm_Symmetry') == 4)
            % if C3v is selected use a different Lua file
            appendLuaModule( handles, saveFullFileLua, ['RIXS2p3d_Operators_C3v.lua'] )                  
        else    
            % else, for Oh, D4h, C4 part below lua file
            appendLuaModule( handles, saveFullFileLua, ['RIXS2p3d_Operators.lua'] )      
        end
        % ----------------------------------------------------------------------------------------------------
        fidOut = fopen(saveFullFileLua, 'at');   
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);    
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['-- Number of Eigenstates  ' '\n']);    
        fprintf(fidOut, ['Npsi=' num2str(noOfEigenstates) '\n']);    
        fprintf(fidOut, ['  ' '\n']);  
        fprintf(fidOut, ['  ' '\n']);        
        fprintf(fidOut, ['-- in order to make sure we have a filling of 2 electrons we need to define some restrictions                        ' '\n']);  
        fprintf(fidOut, ['-- Note the 1s electrons stay 2,2 for all states: only 2p3d RIXS                                                      ' '\n']);  
        fprintf(fidOut, ['GS_Restrictions = {NFermion, NBoson, {"11 000000 0000000000",2,2}, {"00 111111 0000000000",6,6}, {"00 000000 1111111111",eleN_valence_GS,eleN_valence_GS}}; 	-- here: 1s2 2p6 3dn        ' '\n']);  
        fprintf(fidOut, ['IS_Restrictions = {NFermion, NBoson, {"11 000000 0000000000",2,2}, {"00 111111 0000000000",5,5}, {"00 000000 1111111111",eleN_valence_GS+1,eleN_valence_GS+1}}; -- here: 1s2 2p6 3dn+1    ' '\n']);  
        fprintf(fidOut, ['FS_Restrictions = {NFermion, NBoson, {"11 000000 0000000000",2,2}, {"00 111111 0000000000",6,6}, {"00 000000 1111111111",eleN_valence_GS,eleN_valence_GS}}; -- here: 1s2 2p5 3dn          ' '\n']);  
        fprintf(fidOut, ['  ' '\n']);            
        % -------------------------------------------------------------------------  
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);    
        fclose(fidOut);                      % closing file
        % -------------------------------------------------------------------------  
    end
    % ================================================================================================       
    % ================================================================================================     
    if (handles.appSettings.calcEigenVals == 1) && (mtTags(1)  == 1)                               % mtTag = [111] selects block depending on multithread mode
        appendLuaModule( handles, saveFullFileLua, ['RIXS_Observables.lua'] )    
    else
        % if the expectation values are not parsed, then the eigensystem must be added.
        % => is needed fot RIXS calucltaing
        fidOut = fopen(saveFullFileLua, 'at');   
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);    
        fprintf(fidOut, ['-- Solving the Eigensystem to obtain the groundstate  wavefunctions   ' '\n']);    
        fprintf(fidOut, ['oppListGS = {GS_Hamiltonian, OppF3d3d_2,OppF3d3d_4, OppSsqr,OppLsqr,OppJsqr,Oppldots3d, OppNxy,OppNxzyz,OppNx2y2,OppNz2 }	-- for D4h  ' '\n']);    
        fprintf(fidOut, ['psiList_GS = Eigensystem(GS_Hamiltonian, GS_Restrictions, Npsi); 	-- to calculate GS  ' '\n']);            
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);            
        fclose(fidOut);                      % closing file        
    end
    % ================================================================================================  
    % ================================================================================================  
    % parse the Part to calculate th CF diagram 
    if (handles.appSettings.calcELDiagram == 1)  && (mtTags(1)  == 1)                               % mtTag = [111] selects block depending on multithread mode   
        appendLuaModule( handles, saveFullFileLua, ['GS_ELDiagram.lua'] )
    end
    % ================================================================================================      
    % ================================================================================================       
    if (handles.appSettings.calcRIXSmaps == 1) || (handles.appSettings.calcXAS == 1)
        fidOut = fopen(saveFullFileLua, 'at');   
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);    
        fprintf(fidOut, ['NE1 = math.floor((Emax1-Emin1)/dE1); 			-- NE > 10*(Emax-Emin)/Gamma  ' '\n']);    
        fprintf(fidOut, ['print(string.format("!!! EnergyRange1    noOfPointsE1 = %%d", NE1+1))  ' '\n']);        % Note: to print % symbols to file use: %%      
        fprintf(fidOut, ['NE2 = math.floor((Emax2-Emin2)/dE2); 			-- NE > 10*(Emax-Emin)/Gamma  ' '\n']);    
        fprintf(fidOut, ['print(string.format("!!! EnergyRange2    noOfPointsE2 = %%d", NE2+1))  ' '\n']);        % Note: to print % symbols to file use: %%
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);    
        fprintf(fidOut, ['-- select the lowest pltPsiMax groundstates to be put in the energy table  ' '\n']); 
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['pltPsiMax = ' num2str(noOfGSToPlot) '   -- number of ground states  ' '\n']);    
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['-- Fill the psiListPlot variable with the wave functions that should be used ' '\n']);    
        fprintf(fidOut, ['-- NOTE: creates multiple states in one MAP.   ' '\n']);    
        fprintf(fidOut, ['psiListPlot = {}                        -- init empty list  ' '\n']);    
        fprintf(fidOut, ['if pltPsiMax > 1 then  ' '\n']);    
        fprintf(fidOut, ['	for i = 1,pltPsiMax do  ' '\n']);    
        fprintf(fidOut, ['	   psiListPlot[i] = psiList_GS[i]  ' '\n']);    
        fprintf(fidOut, ['	end  ' '\n']);    
        fprintf(fidOut, ['else  ' '\n']);    
        fprintf(fidOut, ['	psiListPlot = psiList_GS[1]  ' '\n']);    
        fprintf(fidOut, ['end	  ' '\n']);    
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);   
        fclose(fidOut);                      % closing file        
    end
    % ================================================================================================ 
    % ================================================================================================     
    if handles.appSettings.calcXAS == 1      
        fidOut = fopen(saveFullFileLua, 'at');           
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fprintf(fidOut, ['-- XAS section  ' '\n']);    
        % ================================================================================================    
        % directy writing the section to calculate LCP and RCP 
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fprintf(fidOut, ['   ' '\n']);        
        fprintf(fidOut, [' saveOn = 0; ' '\n']);    
        if mtTags(2) == 1                               % mtTag = [111] selects block depending on multithread mode
            fprintf(fidOut, ['   ' '\n']);        
            fprintf(fidOut, ['-- Here we calculate the actual XAS with the above defined function    ' '\n']);      
            fprintf(fidOut, ['   ' '\n']);        
            fprintf(fidOut, ['SpecXAS_L = CreateSpectra(XASHamiltonian, TXASl, psiListPlot, {{"Emin",Emin1}, {"Emax",Emax1}, {"NE",NE1},{"Gamma",L_IS}});   ' '\n']);        
            fprintf(fidOut, ['SpecXAS_L.Print({{"file",mapSavePath .. mapBaseName .. "-LCP-XAS-Spec.txt"}});   ' '\n']);       
        end
        if mtTags(3) == 1                               % mtTag = [111] selects block depending on multithread mode                        
            fprintf(fidOut, ['   ' '\n']);        
            fprintf(fidOut, ['SpecXAS_R = CreateSpectra(XASHamiltonian, TXASr, psiListPlot, {{"Emin",Emin1}, {"Emax",Emax1}, {"NE",NE1},{"Gamma",L_IS}});   ' '\n']);        
            fprintf(fidOut, ['SpecXAS_R.Print({{"file",mapSavePath .. mapBaseName .. "-RCP-XAS-Spec.txt"}});   ' '\n']);        
        end
%         if mtTags(3) == 1                               % mtTag = [111] selects block depending on multithread mode            
%             fprintf(fidOut, ['   ' '\n']);    
%             fprintf(fidOut, ['SpectraRCP = 1 * calculateRIXS(TXASr, TXESx, "rcp", "x", saveOn );   ' '\n']);    
%             fprintf(fidOut, ['SpectraRCP = 1 * SpectraRCP + calculateRIXS(TXASr, TXESy, "rcp", "y", saveOn );      ' '\n']);    
%             fprintf(fidOut, ['SpectraRCP = 1 * SpectraRCP + calculateRIXS(TXASr, TXESz, "rcp", "z", saveOn );      ' '\n']);    
%             fprintf(fidOut, ['SpectraRCP.Print({{"file",mapSavePath .. mapBaseName .. "-RCP-Spec.txt"}});  ' '\n']); 
%         end
        fprintf(fidOut, ['   ' '\n']);        
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fclose(fidOut);                      % closing file
    end    
    % ================================================================================================       
     
    
    % ================================================================================================     
    if handles.appSettings.calcRIXSmaps == 1    
        fidOut = fopen(saveFullFileLua, 'at');           
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fprintf(fidOut, ['-- DEFINE FUNCTION to calculate each RIXS spectrum (simplifies the call for each map)  ' '\n']);    
        fprintf(fidOut, ['   ' '\n']);        
        fprintf(fidOut, ['function calculateRIXS(opXAS, opXES, strXAS, strXES, saveTag )  ' '\n']);    
        fprintf(fidOut, ['	print("start: Ka RIXS-" .. strXAS .. "-" .. strXES .. " spectra calculation")  ' '\n']);    
        fprintf(fidOut, ['	-- Spectrum = CreateResonantSpectra(XASHamiltonian,XESHamiltonian,{opXAS},{opXES},psiListPlot, {{"Emin1",Emin1},{"Emax1",Emax1},{"NE1",NE1},{"Gamma1",L_IS}, {"Emin2",Emin2},{"Emax2",Emax2},{"NE2",NE2},{"Gamma2",L_FS},{"restrictions1",IS_Restrictions},{"restrictions2",FS_Restrictions}});   ' '\n']);    
        fprintf(fidOut, ['	Spectrum = CreateResonantSpectra(XASHamiltonian,XESHamiltonian,{opXAS},{opXES},psiListPlot, {{"Emin1",Emin1},{"Emax1",Emax1},{"NE1",NE1},{"Gamma1",L_IS}, {"Emin2",Emin2},{"Emax2",Emax2},{"NE2",NE2},{"Gamma2",L_FS}});   ' '\n']);    
        fprintf(fidOut, ['	Spectrum = 1/math.pi * Spectrum;  ' '\n']);    
        fprintf(fidOut, ['	if (saveTag == 1) then  ' '\n']);    
        fprintf(fidOut, ['		Spectrum.Print({{"file",mapSavePath .. mapBaseName .. "-" .. strXAS .. "-" .. strXES .. "-Spec.txt"}});  ' '\n']);    
        fprintf(fidOut, ['	end  ' '\n']);    
        fprintf(fidOut, ['  ' '\n']);    
        fprintf(fidOut, ['  ' 'collectgarbage()  ' '\n']);     
        fprintf(fidOut, ['	return Spectrum;   ' '\n']);  
        fprintf(fidOut, ['end  ' '\n']);    
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fprintf(fidOut, ['  ' '\n']);      
        % ================================================================================================    
        % directy writing the section to calculate LCP and RCP 
        % could be used to split and make two files (-> Multithreading.)
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fprintf(fidOut, ['   ' '\n']);        
        fprintf(fidOut, [' saveOn = 0;      -- set to 1 to save XES xomponents seperately (see above function)' '\n']);    
        if mtTags(2) == 1                               % mtTag = [111] selects block depending on multithread mode
            fprintf(fidOut, ['   ' '\n']);        
            fprintf(fidOut, ['-- Here we calculate the actual RIXS maps with the above defined function    ' '\n']);      
            fprintf(fidOut, ['SpectraLCP = 1 * calculateRIXS(TXASl, TXESx, "lcp", "x", saveOn );   ' '\n']);    
            fprintf(fidOut, ['SpectraLCP = 1 * SpectraLCP + calculateRIXS(TXASl, TXESy, "lcp", "y", saveOn );      ' '\n']);    
            fprintf(fidOut, ['SpectraLCP = 1 * SpectraLCP + calculateRIXS(TXASl, TXESz, "lcp", "z", saveOn );      ' '\n']);    
            fprintf(fidOut, ['SpectraLCP.Print({{"file",mapSavePath .. mapBaseName .. "-LCP-Spec.txt"}});  ' '\n']); 
        end
        fprintf(fidOut, ['   ' 'collectgarbage()  ' '\n']);     
        if mtTags(3) == 1                               % mtTag = [111] selects block depending on multithread mode            
            fprintf(fidOut, ['   ' '\n']);    
            fprintf(fidOut, ['SpectraRCP = 1 * calculateRIXS(TXASr, TXESx, "rcp", "x", saveOn );   ' '\n']);    
            fprintf(fidOut, ['SpectraRCP = 1 * SpectraRCP + calculateRIXS(TXASr, TXESy, "rcp", "y", saveOn );      ' '\n']);    
            fprintf(fidOut, ['SpectraRCP = 1 * SpectraRCP + calculateRIXS(TXASr, TXESz, "rcp", "z", saveOn );      ' '\n']);    
            fprintf(fidOut, ['SpectraRCP.Print({{"file",mapSavePath .. mapBaseName .. "-RCP-Spec.txt"}});  ' '\n']); 
        end
        fprintf(fidOut, ['   ' '\n']);        
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fprintf(fidOut, ['-- ****************************************************************************************  ' '\n']);      
        fclose(fidOut);                      % closing file

    end    
    % ================================================================================================       
    % ================================================================================================ 
    
%     appendLuaModule( handles,  saveFullFileLua, ['RIXS1s2p_CalculateMaps.lua'] )    
    
    % ================================================================================================       
    % ================================================================================================       

    
    
    
    
    
    
    
    
    
    
    
    
    
    % ------------------------------------------------------------------------------------------------     


end

