function handles = runQuanty(handles, mapSaveDir, mapBaseName )


%     saveFullFileLua = [handles.appRootDir filesep 'Quanty' filesep saveFName ]
    % ------------------------------------------------    
%     luaDir = [handles.appRootDir filesep 'Quanty'];
    
    quantyPath = char(getStrTag({'edit_QuantyPath'}));          % read the path to the binary from the settings panel
    
    debugMsg( ['QuantyPath: ' quantyPath], 1 )

%     tmpNEnd = mapBaseName(1:end-10)
%     tmpNEnd = mapBaseName(end-10:end)
         
    % ------------------------------------------------     
    % prepare the CMD line to be used (assigned the right LUA file for the spectroscopy)
    if getValTag('rb_multithreadOn') && getValTag('rb_runGUI')
        quantyArgIn{1} = [mapSaveDir mapBaseName '_LCP_Quanty.lua' ];
        quantyArgIn{2} = [mapSaveDir mapBaseName '_RCP_Quanty.lua' ];        
    elseif getValTag('rb_runGUI')
        % same filename for 1s2p and 3p3d RIXS (check removed)
        % in single thread
        quantyArgIn = [mapSaveDir mapBaseName '_Quanty.lua' ];
    elseif strcmp(mapBaseName(end-10:end),'_Quanty.lua')     % elseif getValTag('rb_runLUA')    % not needed, is only option left
        mapBaseName = mapBaseName(1:end-11)
        % usung the RAW basename, becasue in lua mode the mapBaseNameVariable
        quantyArgIn = [mapSaveDir mapBaseName '_Quanty.lua' ];
    end

    % ------------------------------------------------ 
    % start the Quanty calculation
    updateStatus(['Quanty running - Please be patient ...'])                           
    pause(0.1);

    if getValTag('rb_multithreadOn') && getValTag('rb_runGUI')
        % ------------------------------------------------
        % running Quanty with the above defined quantyArgIn
        outVal = runQuantyDualThread(quantyPath, quantyArgIn) 
        % ------------------------------------------------
        if outVal == 0
            % ------------------------------------------------
            playFinishedAudio()
            handles = autoLoadMaps(handles, mapSaveDir, mapBaseName);
            % ------------------------------------------------                        
        else    
            disp(' Something is wrong. - Check Quanty_LOG.txt')
        end  
        % ------------------------------------------------        
    else
        % ------------------------------------------------
        % running Quanty with the above defined quantyArgIn
        outVal = runQuantySingleThread(quantyPath, quantyArgIn) 
        % ------------------------------------------------
        
        if outVal == 0
            % ------------------------------------------------
            playFinishedAudio()
            handles = autoLoadMaps(handles, mapSaveDir, mapBaseName);
            % ------------------------------------------------                        
        else    
            disp(' Something is wrong. - Check Quanty_LOG.txt')
        end  
        % ------------------------------------------------
    end

    % ===================================================================================================================  
    % ===================================================================================================================  
    function [outVal, tElapsed] = runQuantyThread(quantyPath, luaFile)
 
        % --------------------------------------------   
        luaFile = strrep(luaFile,'\','\\');            % replace '\' with '\\'      -- Needed for windows, should not have any effect in *nix

        cmdLine = luaCheck(quantyPath, luaFile)
        % --------------------------------------------        
        tStart = tic;
        [outVal,cmdOutput] = system(cmdLine);                   % run system thread with given cmdLine        
        tElapsed = toc(tStart);
        % --------------------------------------------        
        cmdOutput
%         quantyOutput = evalc(cmdOutput)

        % --------------------------------------------        
        % save Quanty output to LOGfile
        logFName = [luaFile(1:end-4) 'LOG.txt']
        
        logFName = strrep(logFName,'\','\\');            % replace '\' with '\\'      -- Needed for windows, should not have any effect in *nix

        
%         fid = fopen([mapSaveDir filesep mapBaseName '_QuantyLOG.txt'], 'w');
        fid = fopen(logFName, 'wt');
        fprintf(fid, cmdOutput);
        fprintf(fid, '---------------------------------------------------------------------------------------- \n');
        if outVal == 0
            if tElapsed > 60
                fprintf(fid, ['Time used to complete calculation:  ' num2str(tElapsed/60) ' minutes. \n' ]);
%                 updateStatus(['Quanty FINISHED in '  num2str(tElapsed/60) ' minutes. ' ])        
            else
                fprintf(fid, ['Time used to complete calculation:  ' num2str(tElapsed) ' seconds. \n' ]);
%                 updateStatus(['Quanty FINISHED in '  num2str(tElapsed) ' seconds. ' ])        
            end            
        else
            fprintf(fid, ['Error during Calculation - Time used to complete calculation:  ' num2str(tElapsed) ' seconds. \n' ]);
%             updateStatus(['Quanty ERROR, check LogFile! - Time needed: '  num2str(tElapsed) ' seconds. ' ])          
        end    
        fprintf(fid, '---------------------------------------------------------------------------------------- \n');
        fclose(fid);            % closing file
        clear fid
        % --------------------------------------------                

    end    
    % ----------------------------------------------------------------------------------------    
    function outValDual = runQuantyDualThread(quantyPath, quantyArgIn)
        
%         luaFileLCP = quantyArgIn{1};
%         luaFileRCP = quantyArgIn{2};
%         
        % funLCP = @(thisCMD) system(thisCMD);

        % multithreading... to speed up calc
%         funList = {@funLCP,@funRCP}                       
%         funList = {@funLCP,@funRCP}                       



        % ****** begin JAVA hack for busy indicator (R2010a and newer) ***********************************
        iconsClassName = 'com.mathworks.widgets.BusyAffordance$AffordanceSize';
        iconsSizeEnums = javaMethod('values',iconsClassName);
        SIZE_32x32 = iconsSizeEnums(2);  % (1) = 16x16,  (2) = 32x32
        jObj = com.mathworks.widgets.BusyAffordance(SIZE_32x32 );  % icon, label
        jObj.setPaintsWhenStopped(true);  % default = false      
        jObj.useWhiteDots(false);         % default = false (true is good for dark backgrounds)
        jComp = javacomponent(jObj.getComponent, [540,-5,50,50], findobj('Tag','uiPan_Status'));
        jObj.start;
        pause(0.1);
        % ****** END JAVA hack ************************************************************************************
        
        % ===================== START QUANTY CALCULATION ==========================================================
        % -------------------------------------------------------------------
%         % closing old POol !!! TESTING if always fine.
%         delete(gcp)                     % closing/clearing multi kernel pool before TESTING if always ok.
        % -------------------------------------------------------------------
        funList = {@funLCP,@funRCP}         
        % -------------------------------------------------------------------      
%         parpool(length(funList));         % start multi kernel pool: length(quantyArgIn) == 2 when mutilthread is on        
        % check if pool is open, open new if needed.
        poolobj = gcp('nocreate'); % If no pool, do not create new one.
        if isempty(poolobj)
            parpool(length(funList));         % start multi kernel pool: length(quantyArgIn) == 2 when mutilthread is on
%         else
%             poolobj(length(funList));  
        end
        % -------------------------------------------------------------------

        parfor i=1:length(funList)
            %# call the threaded functions
%             funList{i}();
             [outVal{i}, tElapsed{i}] = funList{i}(quantyArgIn{i});
%              [outVal{i}, tElapsed{i}] = funList{i}();
            
            % ----------------------------------------------------------------------------------------        
%             [outVal{i}, tElapsed{i}] = runQuantyThread(quantyPath, quantyArgIn{i})
            % ----------------------------------------------------------------------------------------       
        
            
        end  
%         delete(gcp)                     % closing/clearing multi kernel pool
        outValDual = outVal{1} + outVal{2};        
        % -------------------------------------------------------------------
        if outValDual == 0
            if tElapsed{1} > 60
                updateStatus(['Quanty FINISHED - LCP: '  num2str(tElapsed{1}/60) '     RCP: '  num2str(tElapsed{2}/60) ' minutes. ' ])        
            else
                updateStatus(['Quanty FINISHED - LCP: '  num2str(tElapsed{1}) '     RCP: '  num2str(tElapsed{2}) ' seconds. ' ])        
            end            
        else
            updateStatus(['Quanty ERROR, check LogFile! - Time needed: '  num2str(tElapsed) ' seconds. ' ])          
        end          
        % -------------------------------------------------------------------
        % ===================== END QUANTY CALCULATION ==========================================================
        % ****** begin JAVA hack for busy indicator ***************************************************************
        jObj.stop;
        delete(jComp);
        % ****** END JAVA hack ************************************************************************************
        
 
    end
    % ----------------------------------------------------------------------------------------
    function outVal = runQuantySingleThread(quantyPath, luaFile)
        % runs the given command as system thread
        % includes a check for the lua script
        % ****** begin JAVA hack for busy indicator (R2010a and newer) ***********************************
        iconsClassName = 'com.mathworks.widgets.BusyAffordance$AffordanceSize';
        iconsSizeEnums = javaMethod('values',iconsClassName);
        SIZE_32x32 = iconsSizeEnums(2);  % (1) = 16x16,  (2) = 32x32
        jObj = com.mathworks.widgets.BusyAffordance(SIZE_32x32 );  % icon, label
        jObj.setPaintsWhenStopped(true);  % default = false      
        jObj.useWhiteDots(false);         % default = false (true is good for dark backgrounds)
        jComp = javacomponent(jObj.getComponent, [540,-5,50,50], findobj('Tag','uiPan_Status'));
        jObj.start;
        pause(0.1);
        % ****** END JAVA hack ************************************************************************************
        % ===================== START QUANTY CALCULATION ==========================================================
        % ----------------------------------------------------------------------------------------        
        [outVal,tElapsed] = runQuantyThread(quantyPath, luaFile)
        % ----------------------------------------------------------------------------------------       
        if outVal == 0
            if tElapsed > 60
                updateStatus(['Quanty FINISHED in '  num2str(tElapsed/60) ' minutes. ' ])        
            else
                updateStatus(['Quanty FINISHED in '  num2str(tElapsed) ' seconds. ' ])        
            end            
        else
            updateStatus(['Quanty ERROR, check LogFile! - Time needed: '  num2str(tElapsed) ' seconds. ' ])          
        end         
        
        % ===================== END QUANTY CALCULATION ==========================================================
        % ****** begin JAVA hack for busy indicator ***************************************************************
        jObj.stop;
        delete(jComp);
        % ****** END JAVA hack ************************************************************************************

    end    
    % --------------------------------------------
    function [outVal, tElapsed] = funLCP(luaFile)
        [outVal, tElapsed] = runQuantyThread(quantyPath, luaFile);
    end
    function [outVal, tElapsed] = funRCP(luaFile)
        [outVal, tElapsed] = runQuantyThread(quantyPath, luaFile);
    end
    % --------------------------------------------
%     function [outVal, tElapsed] = funLCP()
%         [outVal, tElapsed] = runQuantyThread(quantyPath, luaFileLCP);
%     end
%     function [outVal, tElapsed] = funRCP()
%         [outVal, tElapsed] = runQuantyThread(quantyPath, luaFileRCP);
%     end

    % --------------------------------------------
    function cmdLine = luaCheck(quantyPath, luaFile)
        % check lua for Author Names
        LineNumMH = 3;            
        LineNumPZ = 4;            
        luaStr = textread(luaFile, '%s','delimiter', '\n');             % holds complete lua file as cell array (one line per cell)
        checkVal = 0;
        if strfind(luaStr{LineNumMH},'Myrtille Hunault') && strfind(luaStr{LineNumPZ},'Patric Zimmermann')
            checkVal = 1; 
        end
        clear checkLine LineNumMH LineNumPZ;
        % remove lua file path if CHECK FAILS
        if checkVal
            cmdLine = [quantyPath ' ' luaFile];        % adding script when check succeeds
        else
            cmdLine = [quantyPath ' ' ];             % removing script argument if check fails            
            disp(' !!! ERROR !!! LUA FILE CHECK FAILED in runQuanty() - contact developer !')
        end               
    end  
    % --------------------------------------------
    function playFinishedAudio()
        % when enabled play audio
        if getValTag({'cb_doneMessage'})
            % reads audio file and plays finishing sound
            mp3File = [handles.appRootDir filesep 'Resources' filesep 'matlab_done.mp3'];
            if exist(mp3File) == 2
                [y,Fs] = audioread(mp3File);
                sound(y,Fs);
            else
                disp('mp3File missing')
            end
        end
    end
    % ===================================================================================================================
    % ===================================================================================================================  



end

