function handles = createLUA( handles, mapSaveDir, mapBaseName )
%
% This function is used to create the complete lua script for quanty
%

    % ------------------------------------------------------------------------------------------------
    debugMsg( 'calling createLUA()', 2 )
    % ------------------------------------------------------------------------------------------------
    % ------------------------------------------------------------------------------------------------
    updateStatus(['Collecting and Saving parameters...'])
    % ------------------------------------------------------------------------------------------------
    paraGS = readAllValuesFromGUI(handles, 'GS');
    paraIS = readAllValuesFromGUI(handles, 'IS');
    paraFS = readAllValuesFromGUI(handles, 'FS');
    % ------------------------------------------------------------
    % getting the common Energy ranges from the gui    
    paraRanges.Emin1 = str2num(getStrTag( 'edit_Emin1' ));
    paraRanges.Emax1 = str2num(getStrTag( 'edit_Emax1' ));
    paraRanges.dE1   = str2num(getStrTag( 'edit_dE1' ));

    paraRanges.Emin2 = str2num(getStrTag( 'edit_Emin2' ));
    paraRanges.Emax2 = str2num(getStrTag( 'edit_Emax2' ));
    paraRanges.dE2   = str2num(getStrTag( 'edit_dE2' ));        
    % ------------------------------------------------
    % debug output
    paraGS   
    paraIS
    paraFS
    paraRanges
    % ------------------------------------------------------------------------------------------------

    
    if arrayCheck(paraGS) && arrayCheck(paraIS) && arrayCheck(paraFS)
        % is arrayCheck is okay for all then just single values in GUI    
        
        % ------------------------------------------------------------------------------------------------     
        % saving parameter also to a matfile (used for plotting the maps)
        saveNameParaMat = [mapSaveDir filesep mapBaseName '_para.mat'];        
        savePara2MatFile(saveNameParaMat, paraGS, paraIS, paraFS, paraRanges) 
        % ------------------------------------------------------------------------------------------------     
        if ~getValTag('rb_multithreadOn')               
            % ------------------------------------------------
            % normal mode is multithreading OFF
            % ------------------------------------------------
            assembleFile(mapSaveDir, mapBaseName, mapBaseName, [1 1 1])
            % ------------------------------------------------
        else                        
            % ------------------------------------------------
            % multithreading is turned on
            % ------------------------------------------------
            % assemable two seperate LUA files
            assembleFile(mapSaveDir, [mapBaseName '_LCP'], mapBaseName, [1 1 0])
            assembleFile(mapSaveDir, [mapBaseName '_RCP'], mapBaseName, [0 0 1] )
            % ------------------------------------------------
        end
        
    else
        % at least one value is a list, multiple config files are needed
        % ------------------------------------------------             
        %TODO: CREATE MULTIPLE CONGIG files
        disp('!!! CREATE MULTIPLE CONGIG files !!! ')
        % ------------------------------------------------         
    end      
    
%     if arrayCheck(paraGS) && arrayCheck(paraIS) && arrayCheck(paraFS)
%         % is arrayCheck is okay for all then just single values in GUI
%         % ------------------------------------------------------------------------------------------------     
%         % saving parameter also to a matfile (used for plotting the maps)
%         saveNameParaMat = [mapSaveDir filesep mapBaseName '_para.mat'];        
%         savePara2MatFile(saveNameParaMat, paraGS, paraIS, paraFS, paraRanges) 
%         % ------------------------------------------------------------------------------------------------     
%         % create the initial lua file with all the parameter from the GUI
%         % saves all to:   [mapSaveDir filesep mapBaseName '_Quanty.lua' ];
%         createLUAParams(handles, mapSaveDir, mapBaseName, paraGS, paraIS, paraFS, paraRanges )
%         % ------------------------------------------------------------------------------------------------     
%         % add the rest of the lua here....
%         createLUAMain( handles, mapSaveDir, mapBaseName )                 
%         % ------------------------------------------------------------------------------------------------     
%     else    % at least one value is a list, multiple config files are needed
%         % ------------------------------------------------             
%         %TODO: CREATE MULTIPLE CONGIG files
%         disp('!!! CREATE MULTIPLE CONGIG files !!! ')
%         % ------------------------------------------------         
%     end
    % ================================================================================================
    % ================================================================================================
    function assembleFile(mapSaveDir, luaBaseName, mapBaseName, mtTags)
        % ------------------------------------------------------------------------------------------------         
        % NOTE: mtTags = [1 1 1] is used as 'multithread Tag', normal mode is mutithread OFF, and all on: [1 1 1] 
        % selections for Observables and spectra are made with the GUI. 
        % in MULTITHREAD MODE: [1 0 0] = Observables on; [0 1 0] = LCP spectrum; [0 0 1] = RCP spectrum   
        % DEBUG:  mtTags = [1 1 1];
        % ------------------------------------------------------------------------------------------------     
        % create the initial lua file with all the parameter from the GUI
        % saves all to:   [mapSaveDir filesep mapBaseName '_Quanty.lua' ];
        createLUAParams(handles, mapSaveDir, luaBaseName, mapBaseName, paraGS, paraIS, paraFS, paraRanges )
        % ------------------------------------------------------------------------------------------------     
        % add the rest of the lua here....
        createLUAMain( handles, mapSaveDir, luaBaseName, mtTags )                 
        % ------------------------------------------------------------------------------------------------     
    end
    
    % ================================================================================================
    % function to check if one of the GUI values is a list/field => we need a trigger to make MULTIPLE files for a batch mode later
    function outVal = arrayCheck(paraSet)
        outVal = 1;                         % always one, if not changed below

        % DISABLED because Bx, By,Bz trigger error -- or not???
        
%         fields = fieldnames(paraSet);
%         for j = 1:size(fields,1)
%             thisField = char(fields{j});
%             if (length(paraSet.(thisField)) > 1) 
%                 
%                 % IF config and Ionstring is NOT in this field (both are alwasy longer > 1)
%                 % NOTE: Exceptions for the field check !!!
%                 if ((length(strfind(thisField,'config')) == 0) &&...
%                         (length(strfind(thisField,'Ion')) == 0 ) &&...
%                         (length(strfind(thisField,'Spectroscopy')) == 0 ))            
%                          
%                     outVal = 0;             % change outVal to 0 if any list is found
% 
%                     thisValueList = paraSet.(thisField);
%                     disp([ '!!! NOTE The field ' thisField ' is a list of Values: ' num2str(thisValueList) ' !!!' ])
%                     
% %                     for m=1:length(thisValueList)
% %                         disp([' Num: ' num2str(m) '  '  num2str(thisValueList(m)) ] )                        
% %                     end
%                         
%                     
%                 end
%             end    
%                 
%         end
        
    end
    % ================================================================================================
    % ================================================================================================



end

