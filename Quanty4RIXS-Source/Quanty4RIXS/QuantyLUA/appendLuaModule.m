function [ output_args ] = appendLuaModule( handles, saveFullFileLua, moduleFileName )

    
    % ------------------------------------------------------------------------------------------------
    debugMsg( 'calling appendLuaModule()', 2 )    
    % ------------------------------------------------------------------------------------------------
    % Quanty directory with the lua scripts/modules  (should be HomeDir on *nix systems)
    quantyDir = [handles.appRootDir filesep 'Quanty' filesep ];
    % ------------------------------------------------------------------------------------------------   
    debugMsg([ 'Parsing lua-Module: ' moduleFileName '    Source: ' quantyDir ],0)

    % -------------------------------------------------------------------- 
    % open InputLua file in read mode (lua module section) 
    fidIN = fopen([quantyDir moduleFileName],'rt');
    % open OutputLua file in append mode     
    fidOut = fopen(saveFullFileLua, 'at');
    % --------------------------------------------------------------------
    % write header lines before actual module is parsed
    fprintf(fidOut, ['-- ******************************************************************************************************************************************************************************** \n']);    
    fprintf(fidOut, ['-- ******************************************************** BEGIN parsing module ' moduleFileName ' ******************************************************************************* \n']);
    fprintf(fidOut, ['-- ******************************************************************************************************************************************************************************** \n']);    
    % --------------------------------------------------------------------
    while ~feof(fidIN)
        
        % read line from module
        currInLine = fgetl(fidIN);
        % replace special characters before parsing current line
        currInLine = strrep(currInLine, '%', '%%');              % Note: to print % symbols to file use: %%
        currInLine = strrep(currInLine, '\n', '\\n');            % Note: to print a \n to the file use: \\n
        % write/append line to the output lua
        fprintf(fidOut,[currInLine '\n']);
        
    end
    % --------------------------------------------------------------------
    fprintf(fidOut, ['-- ********************************************************************************************************************************************************************************  \n']);    
    fprintf(fidOut, ['-- ******************************************************** END parsing module ' moduleFileName ' *********************************************************************************  \n']);
    fprintf(fidOut, ['-- ********************************************************************************************************************************************************************************  \n']);    
    % --------------------------------------------------------------------
    fclose(fidIN);
    fclose(fidOut);            % closing file
    % --------------------------------------------------------------------
    % --------------------------------------------------------------------
 
    

 







end

