function varargout = Quanty4RIXS(varargin)
% Quanty4RIXS MATLAB code for Quanty4RIXS.fig
%      Quanty4RIXS, by itself, creates a new Quanty4RIXS or raises the existing
%      singleton*.
%
%      H = Quanty4RIXS returns the handle to a new Quanty4RIXS or the handle to
%      the existing singleton*.
%
%      Quanty4RIXS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Quanty4RIXS.M with the given input arguments.
%
%% 
%      Quanty4RIXS('Property','Value',...) creates a new Quanty4RIXS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Quanty4RIXS_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Quanty4RIXS_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
% -------------------------------------------------------
%
%> Options may still contain bugs, Value check not yet 100% tested
%

    % --- 1st add all needed paths -------------------------------------------------------
    addpath(genpath('Quanty4RIXS'))    % adds subfolder with all other files to the path

    % ------------------------------------------------------------------------------------ 
    delete(findobj('Tag','mainFigure'))
    % ------------------------------------------------------------------------------------
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @Quanty4RIXS_OpeningFcn, ...
                       'gui_OutputFcn',  @Quanty4RIXS_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    

end % End initialization code - DO NOT EDIT


% --- Executes just before Quanty4RIXS is made visible.
function Quanty4RIXS_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Quanty4RIXS (see VARARGIN)

    % --------------------------------------------
    sysHome = getenv('HOME');        % empty on Windows
    sysOS   = getenv('OS');          % empty on MacOS
      
    if length(sysHome) > 0             % not true on Windows
        disp('Unix-like Operating System detected')
        handles.appRootDir   = fullfile(getenv('HOME'),'Quanty4RIXS');            %
    elseif length(sysOS) > 0           % not true on MacOS
        disp('Windows Operating System detected')
        handles.appRootDir   = fullfile('c:','Quanty4RIXS');            %
    else
        handles.appRootDir   = uigetdir('','Select Quanty4RIXS repository folder')
    end    


    

%     handles.appRootDir   = fullfile(getenv('HOME'),'Quanty4RIXS');            % 

    
    
    % --------------------------------------------
    handles = initUI(hObject, eventdata, handles);
    % --------------------------------------------    
    % Update handles structure
    guidata(hObject, handles);
    % --------------------------------------------
    % UIWAIT makes Quanty4RIXS wait for user response (see UIRESUME)
    % uiwait(handles.mainFigure);
end
    

% --- Outputs from this function are returned to the command line.
function varargout = Quanty4RIXS_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;
end
% ------------------------------------------------------------------------------------------
