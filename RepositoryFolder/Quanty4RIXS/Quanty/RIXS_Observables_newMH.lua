-- **********************************************************************************************************************************
-- *** BEGIN Observables.lua ********************************************************************************************************
--***********************************************************************************************************************************
-- Calculation of the wavefunctions and expectation values **************************************************************************

-- !!! NOTE: oppList is used for the expectation values. MAKE SURE the header fo the output correspons to this List!!! 
oppListGS = {GS_Hamiltonian, OppSz,OppLz,OppJz, OppSsqr,OppLsqr,OppJsqr,Oppldots3d, OppNxy,OppNxzyz,OppNx2y2,OppNz2, OppF3d3d_2,OppF3d3d_4 }	-- for D4h
oppListIS = {XASHamiltonian, OppSz,OppLz,OppJz, OppSsqr,OppLsqr,OppJsqr,Oppldots3d, OppNxy,OppNxzyz,OppNx2y2,OppNz2, OppF3d3d_2,OppF3d3d_4 }	-- for D4h
oppListFS = {XESHamiltonian, OppSz,OppLz,OppJz, OppSsqr,OppLsqr,OppJsqr,Oppldots3d, OppNxy,OppNxzyz,OppNx2y2,OppNz2, OppF3d3d_2,OppF3d3d_4 }	-- for D4h



-- **********************************************************************************************************************************
-- PATCHED by MH on July 24 --
-- this is a test to unable calculations with wrong numbers of Npsi (i.e. when Npsi > to Npsimax, where Npsi max is the maximum possible number of states for a electronic configuration 3dn)
Npsimax = math.fact(10)/math.fact(eleN_valence_GS)/math.fact(10-eleN_valence_GS)
if Npsi > Npsimax then
	Npsi = Npsimax
	print("The number of calculated states is too high,")
	print("it is changed to the maximum number given by ")
	print("the electronic configuration 3d^n")
end


-- **********************************************************************************************************************************

-- **********************************************************************************************************************************
-- PATCHED by MH on July 24 --
  if eleN_valence_GS == 0 then -- for Sc3+ or Ti4+
    psiList_GS= {}
        psiList_GS[1]=NewWavefunction(NFermion,NBoson,{{"10 000000 0000000000 000000",1}});
        psiList_GS[1].Name = "psiUp"
        psiList_GS[2]=NewWavefunction(NFermion,NBoson,{{"01 000000 0000000000 000000",1}});
        psiList_GS[2].Name = "psiDn"
        
    else
    -- we now can create the lowest Npsi eigenstates:
    print("Diagonalisation of the GS Hamiltonian")
psiList_GS = Eigensystem(GS_Hamiltonian, GS_Restrictions, Npsi); 	-- to calculate GS
    end

-- PATCHED by MH on August 11 --
if eleN_valence_GS > 0 then
Npsi_IS=math.fact(10)/math.fact(eleN_valence_GS+1)/math.fact(10-eleN_valence_GS-1)
else 
Npsi_IS=1
end
print("Diagonalisation of the IS Hamiltonian")
psiList_IS = Eigensystem(XASHamiltonian, IS_Restrictions, Npsi_IS*2); 	-- to calculate IS

print("Diagonalisation of the FS Hamiltonian")
Npsi_FS=Npsi_IS*6
psiList_FS = Eigensystem(XESHamiltonian, FS_Restrictions, Npsi_FS); 	-- to calculate FS

--**********************************************************************************************************************************
-- Now we calculate the expectation values for the 3 states (GS, IS,FS) for the above defined OPPlists
	   
if Npsi > 1 then
	-- Opens a file in write mode
	file = io.open(mapSavePath .. mapBaseName .. "-OPvals.txt", "w")
	-- sets the default output to file 
	io.output(file)	
	
	io.write(string.format("# <E> <Sz> <Lz> <Jz>  <S^2>  <L^2>  <J^2>  <l.s>  <Nxy>  <Nxzyz>  <Nx^2-y^2>  <Nz^2>  <F[2]> <F[4]>\n"));
	io.write("#GS --- Ground State --------------------------------------------------------------- \n");
	for key,psi in pairs(psiList_GS) do
	   	expvalue = psi * oppListGS * psi
	    for k,v in pairs(expvalue) do
	    	io.write(string.format("%6.3f ",v))
	    end;
	    io.write("\n")
	end
	
	io.write("#IS --- Intermediate State --------------------------------------------------------------- \n");
	for key,psi in pairs(psiList_IS) do
	   	expvalue = psi * oppListIS * psi
	    for k,v in pairs(expvalue) do
	    	io.write(string.format("%6.3f ",v))
	    end;
	    io.write("\n")
	end
	
	io.write("#FS --- Final State --------------------------------------------------------------- \n");
	for key,psi in pairs(psiList_FS) do
	   	expvalue = psi * oppListFS * psi
	    for k,v in pairs(expvalue) do
	    	io.write(string.format("%6.3f ",v))
	    end;
	    io.write("\n")
	end		
		
	io.close(file) 	-- closes the open file   
end  
-- **********************************************************************************************************************************
-- *** END Observables.lua *************************************************************************************************************
-- **********************************************************************************************************************************
