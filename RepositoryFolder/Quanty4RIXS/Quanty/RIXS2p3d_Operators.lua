-- ****************************************************************************************
-- All operators for the 2p3d RIXS are defined in this file
-- building the basis functions
NFermion = 18;
NBoson = 0;

IndexUp_1s = {0}; 				-- 1s
IndexDn_1s = {1};
IndexUp_2p = {2,3,4}; 			-- 2p
IndexDn_2p = {5,6,7};
IndexUp_3d = {8,9,10,11,12}; 	-- 3d
IndexDn_3d = {13,14,15,16,17};

----- Operators definiton -----
OppSx    = NewOperator("Sx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSy    = NewOperator("Sy"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSz    = NewOperator("Sz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSsqr  = NewOperator("Ssqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppSplus = NewOperator("Splus",NFermion,IndexUp_3d,IndexDn_3d);
OppSmin  = NewOperator("Smin" ,NFermion,IndexUp_3d,IndexDn_3d);

OppLx    = NewOperator("Lx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLy    = NewOperator("Ly"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLz    = NewOperator("Lz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLsqr  = NewOperator("Lsqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppLplus = NewOperator("Lplus",NFermion,IndexUp_3d,IndexDn_3d);
OppLmin  = NewOperator("Lmin" ,NFermion,IndexUp_3d,IndexDn_3d);

OppJx    = NewOperator("Jx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJy    = NewOperator("Jy"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJz    = NewOperator("Jz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJsqr  = NewOperator("Jsqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppJplus = NewOperator("Jplus",NFermion,IndexUp_3d,IndexDn_3d);
OppJmin  = NewOperator("Jmin" ,NFermion,IndexUp_3d,IndexDn_3d);

-- Spin orbit operators
Oppldots3d = NewOperator("ldots",NFermion,IndexUp_3d,IndexDn_3d);
Oppldots2p = NewOperator("ldots",NFermion,IndexUp_2p,IndexDn_2p);

-- define the coulomb operator
-- we here define the part depending on F0 seperately from the part depending on F2
-- when summing we can put in the numerical values of the slater integrals
-- 3d-3d
OppF3d3d_0 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{1,0,0});
OppF3d3d_2 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{0,1,0});
OppF3d3d_4 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{0,0,1});

-- 2p-3d
OppF2p3d_0 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{1,0},{0,0});
OppF2p3d_2 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,1},{0,0});
OppG2p3d_1 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,0},{1,0});
OppG2p3d_3 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,0},{0,1});


-- *****************************************************************
-- Core Level Spectroscopy, FMF de Groot, p 120
-- x2y2 = 6*Dq + 2*Ds - Dt;
-- z2   = 6*Dq - 2*Ds - 6*Dt;
-- xy   = -4*Dq + 2*Ds - Dt;
-- xzyz = -4*Dq - Ds + 4*Dt;
-- *****************************************************************
-- Definitions for the D4h distortion (Ds,Dt)
--********************************************
Akm = PotentialExpandedOnClm("D4h", 2, {6,6,-4,-4})								-- Dq
OppDq = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {2,-2,2,-1})
OppDs = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm) 			  	-- Ds

Akm = PotentialExpandedOnClm("D4h", 2, {-1,-6,-1,4})
OppDt = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm) 		  		-- Dt

--********************************************
-- Definition of Operators that counts the electrons in x2y2, z2, xy and xzyz
Akm = PotentialExpandedOnClm("D4h", 2, {1,0,0,0})
OppNx2y2 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,1,0,0})
OppNz2 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,0,1,0})
OppNxy = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,0,0,1})
OppNxzyz = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)
-- *****************************************************************
-- *****************************************************************


--********************************************
-- Definition of the transition operators
--********************************************

t=math.sqrt(1/2)
-- XAS:
-- X component of the electric dipole
Akm = {{1,-1,t},{1, 1,-t}}
TXASx = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)
-- Y component of the electric dipole
Akm = {{1,-1,t*I},{1, 1,t*I}}
TXASy = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)
-- Z component of the electric dipole
Akm = {{1,0,1}}
TXASz = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)

--- Circular light defined with linear light (works only for dipole)
-- Circular Right transition component of the electric dipole
TXASr = t*(TXASx - I * TXASy)
-- Circular Left transition component of the electric dipole
TXASl =-t*(TXASx + I * TXASy)

-- XES: NOTE for 2p3d RIXS the Conjucates of the transition operators XAS are the XES (2p3d XAS and 2p3d XES)
TXESx = ConjugateTranspose(TXASx)
TXESy = ConjugateTranspose(TXASy)
TXESz = ConjugateTranspose(TXASz)

-- Other stuffs to truncate negligible terms and speed up calc
TXASx.Chop()
TXASy.Chop()
TXASr.Chop()
TXASl.Chop()

TXESx.Chop()
TXESy.Chop()
TXESz.Chop()


-- *****************************************************************
-- Definition of the Hamiltonians of the system
-- *****************************************************************

-- Ground state (1s2 2p6 3dn) ------------------------------------
----- create Hamiltonian : Coulomb -----
OppGrdU = F0_3d3d_GS * OppF3d3d_0
        + F2_3d3d_GS * OppF3d3d_2
        + F4_3d3d_GS * OppF3d3d_4     
		-- + F0_2p3d_GS * OppF2p3d_0 	-- should not contribute, included for completeness
		-- + F2_2p3d_GS * OppF2p3d_2 	-- should not contribute, included for completeness
		-- + F4_2p3d_GS * OppF2p3d_4 	-- should not contribute, included for completeness
	
----- create Hamiltonian : Crystal Field
OppGrdCF = Dq_GS * OppDq + Ds_GS * OppDs + Dt_GS * OppDt;							-- CF Hamiltonian for D4h

----- create Hamiltonian : Spin Orbit
OppGrdSO = SOCv_GS * Oppldots3d
       
----- create Hamiltonian : Zeeman / magnetic
OppZeeman = Bx * (2*OppSx + OppLx) + By * (2*OppSy + OppLy) + Bz * (2*OppSz + OppLz);

----- create Hamiltonian for the ground state 1s2,3dn
GS_Hamiltonian = OppGrdU + OppGrdCF  + OppGrdSO + OppZeeman 
----- partial Hamiltonians for ELDiagrams
Hamiltonian_noCF     = OppGrdU + OppGrdSO + OppZeeman 
Hamiltonian_noZeeman = OppGrdU + OppGrdSO + OppGrdCF
 
--- Intermediate state of RIXS   ( 2p5 3dn+1) ------------------------------------------
----- create Hamiltonian : Coulomb -----
OppExiU = F0_3d3d_IS * OppF3d3d_0
        + F2_3d3d_IS * OppF3d3d_2
        + F4_3d3d_IS * OppF3d3d_4 		-- + F0_2p3d_IS * OppF2p3d_0     -- added
        + F2_2p3d_IS * OppF2p3d_2
        + G1_2p3d_IS * OppG2p3d_1
        + G3_2p3d_IS * OppG2p3d_3;
       

----- create Hamiltonian : Crystal Field
OppExiCF = Dq_IS * OppDq + Ds_IS * OppDs + Dt_IS * OppDt;							-- CF Hamiltonian for D4h
     
----- create Hamiltonian : Spin Orbit
OppExiSO = SOCv_IS * Oppldots3d
		 + SOCc_IS * Oppldots2p;
		 
----- create Hamiltonian for the final state 1s1,3d,4p)n+1 -----
XASHamiltonian = OppExiU + OppExiCF  + OppExiSO + OppZeeman

--- Final state of RIXS of RIXS  (2p6 3dn) ------------------------------------------
----- create Hamiltonian : Coulomb -----
OppFinU = F0_3d3d_FS * OppF3d3d_0
        + F2_3d3d_FS * OppF3d3d_2
        + F4_3d3d_FS * OppF3d3d_4
		
----- create Hamiltonian : Crystal Field
-- OppFinCF = Fin10Dq * Opp10Dq;
OppFinCF = Dq_FS * OppDq + Ds_FS * OppDs + Dt_FS * OppDt;							-- CF Hamiltonian for D4h

----- create Hamiltonian : Spin Orbit
OppFinSO = SOCv_FS * Oppldots3d
         + SOCc_FS * Oppldots2p;

----- create Hamiltonian for the final state 1s2,2p5,3dn+1 -----
XESHamiltonian = OppFinU + OppFinCF  + OppFinSO + OppZeeman 
		
-- in order to remove small values from the operator one can chop these
GS_Hamiltonian.Chop();
XASHamiltonian.Chop();
XESHamiltonian.Chop(); 	

-- *****************************************************************
-- END Operators module - Now the wavefunctions can be calculated
-- *****************************************************************