

-- XAS:
-- X component of the electric dipole
Akm = {{1,-1,t},{1, 1,-t}}
TXAS_Lx = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)
-- Y component of the electric dipole
Akm = {{1,-1,t*I},{1, 1,t*I}}
TXAS_Ly = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)
-- Z component of the electric dipole
Akm = {{1,0,1}}
TXAS_Lz = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)




function calculateXAS(opXAS, strXAS, saveTag )  
	print("start: L2,3 XAS-" .. strXAS .. "- spectra calculation")  
	Spectrum = CreateSpectra(XASHamiltonian,{opXAS},psiListPlot, {{"Emin1",Emin1},{"Emax1",Emax1},{"NE1",NE1},{"Gamma1",L_IS},});   
	Spectrum = 1/math.pi * Spectrum;  
	if (saveTag == 1) then  
		Spectrum.Print({{"file",mapSavePath .. mapBaseName .. "-" .. strXAS .. "-Spec.txt"}});  
	end  
  
	return Spectrum;   
end  


SpectraXAS = calculateXAS(TXAS_Lx, "Lx", saveOn ); 
SpectraXAS = SpectraXAS + calculateXAS(TXAS_Ly, "Ly", saveOn ); 
SpectraXAS = SpectraXAS + calculateXAS(TXAS_Lz, "Lz", saveOn ); 

SpectraXAS.Print({{"file",mapSavePath .. mapBaseName .. "-" .. strXAS .. "-L23-Spec.txt"}});  
