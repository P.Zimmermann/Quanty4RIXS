-- **********************************************************************************************************************************
-- *** Begin Energy Level diagramm ***************************************************************************************************
-- **********************************************************************************************************************************
-- copied and altered from QuantyDoc.pdf (2/2016) p.167ff
file = io.open(mapSavePath .. mapBaseName .. "-ELDiagram.txt", "w")
io.output(file)			-- sets the default output to file 


-- **********************************************************************************************************************************
-- make sure the following definition exists above:     Hamiltonian_noCF    -- Hamiltonian WITHOUT Crystal Field
psiList_CF = Eigensystem(Hamiltonian_noCF, GS_Restrictions, Npsi); 	-- to calculate just the number of groundstates         

-- building the header string
-- io.write("#CF_10Dq -------------------------------------------------------------------------------------------------------------- \n");
idString = "#CF_10Dq " .. string.format("%13.10f ",Dq_GS) .. string.format("%13.10f ",Ds_GS) .. string.format("%13.10f ",Dt_GS) .. string.format("%10.6f ",M_GS) .. " -------------------------------------------------------------------------------------------------------------- \n"
io.write(idString);
header = "# <10Dq> "
i = 0
for k,v in pairs(psiList_CF) do
	i = i+1
	header = header .. " <psi" .. string.format("%d",i) .. "> "
end
header = header .. "\n"
-- writing header to file 
io.write(string.format(header));
-- getting and writing the data to file
for i=0, 3000 do
	tenDq = 0.001*i 									-- run tenDq from 0.0 to 3.0 eV
	io.write(string.format("%5.3f ",tenDq))
	io.flush()
	-- file:write(string.format("%14.7E ",tenDq))
	-- file:write(string.format("%13.10f ",tenDq))
	
	-- HamiltonianTMP = Hamiltonian_noCF + tenDq/10 * OppDq  				-- add the Crystal Field to the Hamiltonian0      -- NOTE Hamiltonian expects Dq NOT 10Dq
	HamiltonianTMP = Hamiltonian_noCF + tenDq/10 * OppDq + Ds_GS * OppDs + Dt_GS * OppDt     -- NOTE Hamiltonian expect Dq NOT 10Dq
	
	-- psiList_ELD = Eigensystem(HamiltonianTMP,GS_Restrictions, Npsi)
	Eigensystem(HamiltonianTMP, psiList_CF) 					-- Updating psiList_CF with this ????
	
	-- for key,value in pairs(psiList_ELD) do
	for key,value in pairs(psiList_CF) do
		-- energy = value * HamiltonianTMP * value 					-- calculate EigenValue
		energy = Chop(value * HamiltonianTMP * value) 					-- calculate EigenValue
		-- file:write(string.format("%14.7E ",energy))
		file:write(string.format("%13.10f ",energy))
	end
	file:write("\n")
	collectgarbage()
end
-- **********************************************************************************************************************************

-- **********************************************************************************************************************************
-- building the header string
-- io.write("#CF_Ds -------------------------------------------------------------------------------------------------------------- \n");
idString = "#CF_Ds " .. string.format("%10.6f ",Dq_GS) .. string.format("%10.6f ",Ds_GS) .. string.format("%10.6f ",Dt_GS) .. string.format("%10.6f ",M_GS) .. " --------------------------------------------- \n"
io.write(idString);
header = "# <Ds> "
i = 0
for k,v in pairs(psiList_CF) do
	i = i+1
	header = header .. " <psi" .. string.format("%d",i) .. "> "
end
header = header .. "\n"
-- writing header to file 
io.write(string.format(header));
-- getting and writing the data to file
for i=-100, 100 do
	cfDs = 0.001*i 									-- run tenDq from 0.0 to 3.0 eV
	io.write(string.format("%5.3f ",cfDs))
	io.flush()
	-- file:write(string.format("%14.7E ",cfDs))
	-- file:write(string.format("%13.10f ",cfDs))
	
	-- HamiltonianTMP = Hamiltonian_noCF + cfDs * cfDs  				-- add the Crystal Field to the Hamiltonian0
	
	HamiltonianTMP = Hamiltonian_noCF + Dq_GS * OppDq + cfDs * OppDs + Dt_GS * OppDt
	
	Eigensystem(HamiltonianTMP, psiList_CF) 					-- Updating psiList_CF with this ????

	
	for key,value in pairs(psiList_CF) do
		-- energy = value * HamiltonianTMP * value 					-- calculate EigenValue
		energy = Chop(value * HamiltonianTMP * value) 					-- calculate EigenValue
		-- file:write(string.format("%14.7E ",energy))
		file:write(string.format("%13.10f ",energy))
	end
	file:write("\n")
	collectgarbage()
end
-- **********************************************************************************************************************************

-- **********************************************************************************************************************************
-- building the header string
-- io.write("#CF_Dt -------------------------------------------------------------------------------------------------------------- \n");
idString = "#CF_Dt " .. string.format("%10.6f ",Dq_GS) .. string.format("%10.6f ",Ds_GS) .. string.format("%10.6f ",Dt_GS) .. string.format("%10.6f ",M_GS) .. " --------------------------------------------- \n"
io.write(idString);

header = "# <Dt> "
i = 0
for k,v in pairs(psiList_CF) do
	i = i+1
	header = header .. " <psi" .. string.format("%d",i) .. "> "
end
header = header .. "\n"
-- writing header to file 
io.write(string.format(header));
-- getting and writing the data to file
for i=-100, 100 do
	cfDt = 0.001*i 									-- run tenDq from 0.0 to 3.0 eV
	io.write(string.format("%5.3f ",cfDt))
	io.flush()
	-- file:write(string.format("%14.7E ",cfDt))
	-- file:write(string.format("%13.10f ",cfDt))
	
	-- HamiltonianTMP = Hamiltonian_noCF + cfDs * cfDs  				-- add the Crystal Field to the Hamiltonian0
	
	HamiltonianTMP = Hamiltonian_noCF + Dq_GS * OppDq + Ds_GS * OppDs + cfDt * OppDt
	
	Eigensystem(HamiltonianTMP, psiList_CF)
	
	for key,value in pairs(psiList_CF) do
		-- energy = value * HamiltonianTMP * value 					-- calculate EigenValue
		energy = Chop(value * HamiltonianTMP * value) 					-- calculate EigenValue
		-- file:write(string.format("%14.7E ",energy))
		file:write(string.format("%13.10f ",energy))
	end
	file:write("\n")
	collectgarbage()
end
-- **********************************************************************************************************************************

-- **********************************************************************************************************************************
-- make sure the following definition exists above:     Hamiltonian_noCF    -- Hamiltonian WITHOUT Crystal Field
psiList_Zeeman = Eigensystem(Hamiltonian_noZeeman, GS_Restrictions, Npsi); 	-- to calculate GS         

-- Floating M= 0...50meV
-- building the header string
-- io.write("#CF_Dt -------------------------------------------------------------------------------------------------------------- \n");
idString = "#CF_M " .. string.format("%10.6f ",Dq_GS) .. string.format("%10.6f ",Ds_GS) .. string.format("%10.6f ",Dt_GS) .. string.format("%10.6f ",M_GS) .. " --------------------------------------------- \n"
io.write(idString);

header = "# <M> "
i = 0
for k,v in pairs(psiList_Zeeman) do
	i = i+1
	header = header .. " <psi" .. string.format("%d",i) .. "> "
end
header = header .. "\n"
-- writing header to file 
io.write(string.format(header));
-- getting and writing the data to file
for i=0, 100 do
	cf_M = 0.001*i 									-- run tenDq from 0.0 to 3.0 eV
	io.write(string.format("%5.3f ",cf_M))
	io.flush()
	-- file:write(string.format("%14.7E ",cfDt))
	-- file:write(string.format("%13.10f ",cf_M))
	
	-- HamiltonianTMP = Hamiltonian_noCF + cfDs * cfDs  				-- add the Crystal Field to the Hamiltonian0
	
	HamiltonianTMP = Hamiltonian_noZeeman + Bx * (2*OppSx + OppLx) + By * (2*OppSy + OppLy) + cf_M * (2*OppSz + OppLz)
	-- HamiltonianTMP = Hamiltonian_noZeeman + Bvec[1] * (2*OppSx + OppLx) + Bvec[2] * (2*OppSy + OppLy) + Bvec[3] * (2*OppSz + OppLz);
	
	Eigensystem(HamiltonianTMP, psiList_Zeeman)
	
	for key,value in pairs(psiList_Zeeman) do
		-- energy = value * HamiltonianTMP * value 					-- calculate EigenValue
		energy = Chop(value * HamiltonianTMP * value) 					-- calculate EigenValue
		-- file:write(string.format("%14.7E ",energy))
		file:write(string.format("%13.10f ",energy))
	end
	file:write("\n")
	collectgarbage()
end
-- **********************************************************************************************************************************
io.write("\n")
file:close()
collectgarbage()

-- **********************************************************************************************************************************
-- *** END 10Dq energy diagramm ***************************************************************************************************
-- **********************************************************************************************************************************
