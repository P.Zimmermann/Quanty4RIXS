-- **********************************************************************************************************************************
-- *** BEGIN Observables.lua ********************************************************************************************************
--***********************************************************************************************************************************
-- Calculation of the wavefunctions and expectation values **************************************************************************

-- !!! NOTE: oppList is used for the expectation values. MAKE SURE the header fo the output correspons to this List!!! 
oppListGS = {GS_Hamiltonian, OppSz,OppLz,OppJz, OppSsqr,OppLsqr,OppJsqr,Oppldots3d, OppNxy,OppNxzyz,OppNx2y2,OppNz2, OppF3d3d_2,OppF3d3d_4 }	-- for D4h
oppListIS = {XASHamiltonian, OppSz,OppLz,OppJz, OppSsqr,OppLsqr,OppJsqr,Oppldots3d, OppNxy,OppNxzyz,OppNx2y2,OppNz2, OppF3d3d_2,OppF3d3d_4 }	-- for D4h
oppListFS = {XESHamiltonian, OppSz,OppLz,OppJz, OppSsqr,OppLsqr,OppJsqr,Oppldots3d, OppNxy,OppNxzyz,OppNx2y2,OppNz2, OppF3d3d_2,OppF3d3d_4 }	-- for D4h


-- we now can create the lowest Npsi eigenstates:
psiList_GS = Eigensystem(GS_Hamiltonian, GS_Restrictions, Npsi); 	-- to calculate GS
psiList_IS = Eigensystem(XASHamiltonian, IS_Restrictions, Npsi); 	-- to calculate IS
psiList_FS = Eigensystem(XESHamiltonian, FS_Restrictions, Npsi); 	-- to calculate FS

-- **********************************************************************************************************************************
-- Now we calculate the expectation values for the 3 states (GS, IS,FS) for the above defined OPPlists
	   
if Npsi > 1 then
	-- Opens a file in write mode
	file = io.open(mapSavePath .. mapBaseName .. "-OPvals.txt", "w")
	-- sets the default output to file 
	io.output(file)	
	
	io.write(string.format("# <E> <Sz> <Lz> <Jz>  <S^2>  <L^2>  <J^2>  <l.s>  <Nxy>  <Nxzyz>  <Nx^2-y^2>  <Nz^2>  <F[2]> <F[4]>\n"));
	io.write("#GS --- Ground State --------------------------------------------------------------- \n");
	for key,psi in pairs(psiList_GS) do
	   	expvalue = Chop(psi * oppListGS * psi)		
	    for k,v in pairs(expvalue) do
	    	io.write(string.format("%6.3f ",v))
	    end;
	    io.write("\n")
	end
	collectgarbage()
	
	io.write("#IS --- Intermediate State --------------------------------------------------------------- \n");
	for key,psi in pairs(psiList_IS) do
	   	-- expvalue = psi * oppListIS * psi
	   	expvalue = Chop(psi * oppListIS * psi)		
	    for k,v in pairs(expvalue) do
	    	io.write(string.format("%6.3f ",v))
	    end;
	    io.write("\n")
	end
	collectgarbage()
	
	io.write("#FS --- Final State --------------------------------------------------------------- \n");
	for key,psi in pairs(psiList_FS) do
	   	-- expvalue = psi * oppListFS * psi
	   	expvalue = Chop(psi * oppListFS * psi)		
	    for k,v in pairs(expvalue) do
	    	io.write(string.format("%6.3f ",v))
	    end;
	    io.write("\n")
	end
	collectgarbage()		
		
	io.close(file) 	-- closes the open file   
end  
-- **********************************************************************************************************************************
-- *** END Observables.lua *************************************************************************************************************
-- **********************************************************************************************************************************
