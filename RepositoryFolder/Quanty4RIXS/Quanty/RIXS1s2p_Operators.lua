-- *****************************************************************
-- All operators for the 1s2p RIXS are defined in this file
-- building the basis functions
NFermion = 18;
NBoson = 0;
IndexUp_1s = {0}; 				-- 1s
IndexDn_1s = {1};
IndexUp_2p = {2,4,6}; 			-- 2p
IndexDn_2p = {3,5,7};
IndexUp_3d = {8,10,12,14,16}; 	-- 3d
IndexDn_3d = {9,11,13,15,17};

----- Operators definiton -----
OppSx    = NewOperator("Sx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSy    = NewOperator("Sy"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSz    = NewOperator("Sz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSsqr  = NewOperator("Ssqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppSplus = NewOperator("Splus",NFermion,IndexUp_3d,IndexDn_3d);
OppSmin  = NewOperator("Smin" ,NFermion,IndexUp_3d,IndexDn_3d);

OppLx    = NewOperator("Lx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLy    = NewOperator("Ly"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLz    = NewOperator("Lz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLsqr  = NewOperator("Lsqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppLplus = NewOperator("Lplus",NFermion,IndexUp_3d,IndexDn_3d);
OppLmin  = NewOperator("Lmin" ,NFermion,IndexUp_3d,IndexDn_3d);

OppJx    = NewOperator("Jx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJy    = NewOperator("Jy"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJz    = NewOperator("Jz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJsqr  = NewOperator("Jsqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppJplus = NewOperator("Jplus",NFermion,IndexUp_3d,IndexDn_3d);
OppJmin  = NewOperator("Jmin" ,NFermion,IndexUp_3d,IndexDn_3d);

-- Spin orbit operators
Oppldots3d = NewOperator("ldots",NFermion,IndexUp_3d,IndexDn_3d);
Oppldots2p = NewOperator("ldots",NFermion,IndexUp_2p,IndexDn_2p);

-- define the coulomb operator
-- we here define the part depending on F0 seperately from the part depending on F2
-- when summing we can put in the numerical values of the slater integrals
-- 3d-3d
OppF3d3d_0 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{1,0,0});
OppF3d3d_2 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{0,1,0});
OppF3d3d_4 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{0,0,1});
-- 2p-3d
OppF2p3d_0 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{1,0},{0,0});
OppF2p3d_2 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,1},{0,0});

OppG2p3d_1 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,0},{1,0});
OppG2p3d_3 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,0},{0,1});
-- 1s-3d
OppF1s3d_0 = NewOperator("U",NFermion,IndexUp_1s,IndexDn_1s,IndexUp_3d,IndexDn_3d,{1},{0}); -- ,F,G);
OppG1s3d_2 = NewOperator("U",NFermion,IndexUp_1s,IndexDn_1s,IndexUp_3d,IndexDn_3d,{0},{1});

-- *****************************************************************
-- Core Level Spectroscopy, FMF de Groot, p 120
-- x2y2 = 6*Dq + 2*Ds - Dt;
-- z2   = 6*Dq - 2*Ds - 6*Dt;
-- xy   = -4*Dq + 2*Ds - Dt;
-- xzyz = -4*Dq - Ds + 4*Dt;
-- *****************************************************************
-- Definitions for the D4h distortion (Ds,Dt)
--********************************************
Akm = PotentialExpandedOnClm("D4h", 2, {6,6,-4,-4})								-- Dq
OppDq = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {2,-2,2,-1})
OppDs = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm) 			  	-- Ds

Akm = PotentialExpandedOnClm("D4h", 2, {-1,-6,-1,4})
OppDt = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm) 		  		-- Dt

-- Definition of Operators that counts the electrons in x2y2, z2, xy and xzyz
Akm = PotentialExpandedOnClm("D4h", 2, {1,0,0,0})
OppNx2y2 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,1,0,0})
OppNz2 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,0,1,0})
OppNxy = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,0,0,1})
OppNxzyz = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)
-- *****************************************************************
-- *****************************************************************
-- Definition of the transition operators
--********************************************
-----------------------------------------------------------------------------------------
t=math.sqrt(1/2)
-- -----------------------------------------------------------------------------------------
-- DIRECT definityion of LCP/RCP in spherical harminics
-- -----------------------------------------------------------------------------------------
-- -- CIRCULAR POLARIZED LIGHT with k parallel to z
-- Akm = {{2,1,1}};
-- TXASl = Chop(NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_1s, IndexDn_1s, Akm)); 		-- directly chopping, same as doing TXASl.Chop(); as next step
-- Akm = {{2,-1,1}};
-- TXASr = Chop(NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_1s, IndexDn_1s, Akm));
-- -----------------------------------------------------------------------------------------



-----------------------------------------------------------------------------------------
-- DEFINITION of LCP via the 5 Basis vector (enables for angular change)
-----------------------------------------------------------------------------------------
-- xas K-edge quad transition 1s->3d for generalized polarization (K // z ???)
-- Akm = {k,m,value} (has to be normed sum(Akm^2)=1)

-- 5 basis Polarisations in Quadrupole
Akm = {{2,-2,t*I},{2, 2,-t*I}};
TXASKxy = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_1s, IndexDn_1s, Akm);

Akm = {{2,-1,t},{2, 1,-t}};
TXASKxz = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_1s, IndexDn_1s, Akm);

Akm = {{2,-1,t*I},{2, 1,t*I}};
TXASKyz = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_1s, IndexDn_1s, Akm);

Akm = {{2,-2,t},{2, 2,t}};
TXASKx2y2 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_1s, IndexDn_1s, Akm);

Akm = {{2,0,1}};
TXASKz2 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_1s, IndexDn_1s, Akm);

-- in order to remove small values from the operator one can chop these
TXASKxy.Chop();     
TXASKxz.Chop();     
TXASKyz.Chop();     
TXASKx2y2.Chop();   
TXASKz2.Chop();     
-----------------------------------------------------------------------------------------
-- HERE ANGULAR DEFINTIONS of the incident beam for XAS
-----------------------------------------------------------------------------------------
-- "linear polarisation 1"
TXAS_KinEps1 = (eps1[1]*kin[2] + eps1[2]*kin[1]) * TXASKxy + (eps1[1]*kin[3] + eps1[3]*kin[1]) * TXASKxz + (eps1[2]*kin[3] + eps1[3]*kin[2]) * TXASKyz + (eps1[1]*kin[1] + eps1[2]*kin[2]) * TXASKx2y2 + (eps1[3]*kin[3]) * TXASKz2
-- "linear polarisation 2"
TXAS_KinEps2 = (eps2[1]*kin[2] + eps2[2]*kin[1]) * TXASKxy + (eps2[1]*kin[3] + eps2[3]*kin[1]) * TXASKxz + (eps2[2]*kin[3] + eps2[3]*kin[2]) * TXASKyz + (eps2[1]*kin[1] + eps2[2]*kin[2]) * TXASKx2y2 + (eps2[3]*kin[3]) * TXASKz2

-- in order to remove small values from the operator one can chop these
TXAS_KinEps1.Chop();
TXAS_KinEps2.Chop();
-----------------------------------------------------------------------------------------
-- "Right" Circular Polarisation, transition operator defined with linear OPs
TXASr = t*(TXAS_KinEps1 - I * TXAS_KinEps2)
-- "Left" Circular Polarisation, transition operator defined with linear OPs
TXASl =-t*(TXAS_KinEps1 + I * TXAS_KinEps2)
-----------------------------------------------------------------------------------------
-- in order to remove small values from the operator one can chop these
TXASl.Chop();
TXASr.Chop();
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


-- xes dip transition 2p->1s
Akm = {{1,-1,t},{1, 1,-t}};
TXESx = NewOperator("CF", NFermion, IndexUp_1s, IndexDn_1s, IndexUp_2p, IndexDn_2p, Akm);
Akm = {{1,-1,t*I},{1, 1,t*I}};
TXESy = NewOperator("CF", NFermion, IndexUp_1s, IndexDn_1s, IndexUp_2p, IndexDn_2p, Akm);
Akm = {{1,0,1}};
TXESz = NewOperator("CF", NFermion, IndexUp_1s, IndexDn_1s, IndexUp_2p, IndexDn_2p, Akm);

TXESx.Chop();
TXESy.Chop();
TXESz.Chop();


-- *****************************************************************
-- Definition of the Hamiltonians of the system
-- *****************************************************************
-- Ground state (1s2 2p6 3dn) ------------------------------------------------------------------------------------
----- create Hamiltonian : Coulomb -----
OppGrdU = F0_3d3d_GS * OppF3d3d_0
        + F2_3d3d_GS * OppF3d3d_2
        + F4_3d3d_GS * OppF3d3d_4
        
----- create Hamiltonian : Crystal Field !!  (For CF Tabene diagram)
-- OppGrdCF = Grd10Dq * Opp10Dq;
OppGrdCF = Dq_GS * OppDq + Ds_GS * OppDs + Dt_GS * OppDt;							-- CF Hamiltonian for D4h

----- create Hamiltonian : Spin Orbit
OppGrdSO = SOCv_GS * Oppldots3d
       
----- create Hamiltonian : Zeeman / magnetic
-- OppZeeman = Bx * (2*OppSx + OppLx) + By * (2*OppSy + OppLy) + Bz * (2*OppSz + OppLz);			-- for PARA magnets
OppZeeman = Bx * (2*OppSx) + By * (2*OppSy) + Bz * (2*OppSz);										-- for FERRO magnets

----- create final Hamiltonian for the initial state 1s2,3dn -----
GS_Hamiltonian = OppGrdU + OppGrdSO + OppZeeman + OppGrdCF 
----- partial Hamiltonians for ELDiagrams
Hamiltonian_noCF     = OppGrdU + OppGrdSO + OppZeeman 
Hamiltonian_noZeeman = OppGrdU + OppGrdSO + OppGrdCF
 
-- ***************************************************************
--- Intermediate state of RIXS   (1s1 2p6 3dn+1) ------------------------------------------
----- create Hamiltonian : Coulomb -----
OppExiU = F0_3d3d_IS * OppF3d3d_0
        + F2_3d3d_IS * OppF3d3d_2
        + F4_3d3d_IS * OppF3d3d_4 	--		+ F0_1s3d_IS * OppF1s3d_0 	--	added
        + G2_1s3d_IS * OppG1s3d_2
       
----- create Hamiltonian : Crystal Field
-- OppExiCF = Exi10Dq * Opp10Dq;
OppExiCF = Dq_IS * OppDq + Ds_IS * OppDs + Dt_IS * OppDt;							-- CF Hamiltonian for D4h
        
----- create Hamiltonian : Spin Orbit
OppExiSO = SOCv_IS * Oppldots3d;

----- create Hamiltonian for the final state 1s1,3d,n+1 -----
XASHamiltonian = OppExiU + OppExiCF  + OppExiSO + OppZeeman

--- Final state of RIXS of RIXS  (1s2 2p5 3dn+1) ------------------------------------------
----- create Hamiltonian : Coulomb -----
OppFinU = F0_3d3d_FS * OppF3d3d_0 	--	
        + F2_3d3d_FS * OppF3d3d_2
        + F4_3d3d_FS * OppF3d3d_4   --		+ F0_2p3d_FS * OppF2p3d_0 	--	added
        + F2_2p3d_FS * OppF2p3d_2
        + G1_2p3d_FS * OppG2p3d_1
        + G3_2p3d_FS * OppG2p3d_3;
        
----- create Hamiltonian : Crystal Field
-- OppFinCF = Fin10Dq * Opp10Dq;
OppFinCF = Dq_FS * OppDq + Ds_FS * OppDs + Dt_FS * OppDt;							-- CF Hamiltonian for D4h

----- create Hamiltonian : Spin Orbit
OppFinSO = SOCv_FS * Oppldots3d
         + SOCc_FS * Oppldots2p;

----- create Hamiltonian for the final state 1s2,2p5,3dn+1 -----
XESHamiltonian = OppFinU + OppFinCF  + OppFinSO + OppZeeman 

-- in order to remove small values from the operator one can chop these
GS_Hamiltonian.Chop();
XASHamiltonian.Chop();
XESHamiltonian.Chop();

-- -- !!! NOTE: oppList is used for the expectation values. MAKE SURE the header fo the output correspons to this List!!!
-- oppList={GS_Hamiltonian,OppSsqr,OppLsqr,OppJsqr,Oppldots3d,OppF3d3d_2,OppF3d3d_4,OppNx2y2,OppNz2, OppNxy,OppNxzyz }	-- for D4h

-- *****************************************************************
-- END Operators module - Now the wavefunctions can be calculated
-- *****************************************************************
