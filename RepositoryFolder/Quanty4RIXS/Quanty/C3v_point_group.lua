 -- Hamiltonian for the crystal field in C3v symmetry
    Akm = {{4,0,-7/5},{4,3, -math.sqrt(14/5)},{4, -3, math.sqrt(14/5)}};
    Opp10Dq = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm);


    Akm = {{0,0,0},{2,0,-7},{4,0,0}}
    OppDsigma = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

   
    Akm = {{0,0,0},{2,0,0},{4,0,-21}}
    OppDtau = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

 -- counting the # of electrons in C3v symmetry 

    -- counts the # of electron on a'1
    Akm = PotentialExpandedOnClm("D3h",2,{1,0,0})
    OppNap1 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

    -- counts the # of electron on e'
    Akm = PotentialExpandedOnClm("D3h",2,{0,1,0})
    OppNep = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

    -- count the # of electron on e"
    Akm = PotentialExpandedOnClm("D3h",2,{0,0,1})
    OppNepp = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

-- total number of d electrons 
    OppNd = OppNap1 + OppNep + OppNepp -- number of d electrons