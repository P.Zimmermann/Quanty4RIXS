-- ****************************************************************************************
-- All operators for the 2p3d RIXS are defined in this file
-- building the basis functions
NFermion = 18;
NBoson = 0;

IndexUp_1s = {0}; 				-- 1s
IndexDn_1s = {1};
IndexUp_2p = {2,3,4}; 			-- 2p
IndexDn_2p = {5,6,7};
IndexUp_3d = {8,9,10,11,12}; 	-- 3d
IndexDn_3d = {13,14,15,16,17};

----- Operators definiton -----
OppSx    = NewOperator("Sx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSy    = NewOperator("Sy"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSz    = NewOperator("Sz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppSsqr  = NewOperator("Ssqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppSplus = NewOperator("Splus",NFermion,IndexUp_3d,IndexDn_3d);
OppSmin  = NewOperator("Smin" ,NFermion,IndexUp_3d,IndexDn_3d);

OppLx    = NewOperator("Lx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLy    = NewOperator("Ly"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLz    = NewOperator("Lz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppLsqr  = NewOperator("Lsqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppLplus = NewOperator("Lplus",NFermion,IndexUp_3d,IndexDn_3d);
OppLmin  = NewOperator("Lmin" ,NFermion,IndexUp_3d,IndexDn_3d);

OppJx    = NewOperator("Jx"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJy    = NewOperator("Jy"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJz    = NewOperator("Jz"   ,NFermion,IndexUp_3d,IndexDn_3d);
OppJsqr  = NewOperator("Jsqr" ,NFermion,IndexUp_3d,IndexDn_3d);
OppJplus = NewOperator("Jplus",NFermion,IndexUp_3d,IndexDn_3d);
OppJmin  = NewOperator("Jmin" ,NFermion,IndexUp_3d,IndexDn_3d);

-- Spin orbit operators
Oppldots3d = NewOperator("ldots",NFermion,IndexUp_3d,IndexDn_3d);
Oppldots2p = NewOperator("ldots",NFermion,IndexUp_2p,IndexDn_2p);

-- define the coulomb operator
-- we here define the part depending on F0 seperately from the part depending on F2
-- when summing we can put in the numerical values of the slater integrals
-- 3d-3d
OppF3d3d_0 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{1,0,0});
OppF3d3d_2 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{0,1,0});
OppF3d3d_4 = NewOperator("U",NFermion,IndexUp_3d,IndexDn_3d,{0,0,1});

-- 2p-3d
OppF2p3d_0 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{1,0},{0,0});
OppF2p3d_2 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,1},{0,0});
OppG2p3d_1 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,0},{1,0});
OppG2p3d_3 = NewOperator("U",NFermion,IndexUp_2p,IndexDn_2p,IndexUp_3d,IndexDn_3d,{0,0},{0,1});


-- *****************************************************************
-- Core Level Spectroscopy, FMF de Groot, p 120
-- x2y2 = 6*Dq + 2*Ds - Dt;
-- z2   = 6*Dq - 2*Ds - 6*Dt;
-- xy   = -4*Dq + 2*Ds - Dt;
-- xzyz = -4*Dq - Ds + 4*Dt;

-- *****************************************************************
-- Definitions for the C3v distortion (Dsigma/Dtau)
--********************************************
	-- Dsigma 
	Akm = {{0,0,0},{2,0,-7},{4,0,0}}
    OppDsigma = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

    -- Dtau
    Akm = {{0,0,0},{2,0,0},{4,0,-21}}
    OppDtau = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)
    
    -- 10Dq
    Akm = {{4,0,-7/5},{4,3, -math.sqrt(14/5)},{4, -3, math.sqrt(14/5)}};
    Opp10Dq = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm);


--********************************************
-- Definition of Operators that counts the electrons in x2y2, z2, xy and xzyz
Akm = PotentialExpandedOnClm("D4h", 2, {1,0,0,0})
OppNx2y2 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,1,0,0})
OppNz2 = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,0,1,0})
OppNxy = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)

Akm = PotentialExpandedOnClm("D4h", 2, {0,0,0,1})
OppNxzyz = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, Akm)
-- *****************************************************************
-- *****************************************************************


--********************************************
-- Definition of the transition operators
--********************************************

t=math.sqrt(1/2)
-- XAS:
-- X component of the electric dipole
Akm = {{1,-1,t},{1, 1,-t}}
TXASx = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)
-- Y component of the electric dipole
Akm = {{1,-1,t*I},{1, 1,t*I}}
TXASy = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)

-- Z component of the electric dipole
Akm = {{1,0,1}}
TXASz = NewOperator("CF", NFermion, IndexUp_3d, IndexDn_3d, IndexUp_2p,  IndexDn_2p, Akm)

-- RCP operator
TXASr = t*(TXASx - I * TXASy)
-- LCP operator
TXASl =-t*(TXASx + I * TXASy)

-- XES: 
-- X component of the electric dipole (L23)
Akm = {{1,-1,t},{1, 1,-t}}
TXESx = NewOperator("CF", NFermion, IndexUp_2p,  IndexDn_2p, IndexUp_3d, IndexDn_3d, Akm)
-- Y component of the electric dipole
Akm = {{1,-1,t*I},{1, 1,t*I}}
TXESy = NewOperator("CF", NFermion, IndexUp_2p,  IndexDn_2p, IndexUp_3d, IndexDn_3d, Akm)
-- Z component of the electric dipole
Akm = {{1,0,1}}
TXESz = NewOperator("CF", NFermion, IndexUp_2p,  IndexDn_2p, IndexUp_3d, IndexDn_3d, Akm)

-- RCP operator for emission
TXESr = t*(TXESx - I * TXESy)
-- LCP operator for emission
TXESl =-t*(TXESx + I * TXESy)

-- Other stuffs to truncate negligible terms and speed up calc
TXASx.Chop()
TXASy.Chop()
TXASz.Chop()
TXASr.Chop()
TXASl.Chop()

TXESx.Chop()
TXESy.Chop()
TXESz.Chop()
TXESr.Chop()
TXESl.Chop()


-- *****************************************************************
-- Definition of the Hamiltonians of the system
-- *****************************************************************

-- Ground state (1s2 2p6 3dn) ------------------------------------
----- create Hamiltonian : Coulomb -----
OppGrdU = F0_3d3d_GS * OppF3d3d_0
        + F2_3d3d_GS * OppF3d3d_2
        + F4_3d3d_GS * OppF3d3d_4     
		-- + F0_2p3d_GS * OppF2p3d_0 	-- should not contribute, included for completeness
		-- + F2_2p3d_GS * OppF2p3d_2 	-- should not contribute, included for completeness
		-- + F4_2p3d_GS * OppF2p3d_4 	-- should not contribute, included for completeness

----- create Hamiltonian : Crystal Field
OppGrdCF = Ds_GS * OppDsigma + Dt_GS * OppDtau + Dq_GS * Opp10Dq;		-- CF Hamiltonian for C3

----- create Hamiltonian : Spin Orbit
OppGrdSO = SOCv_GS * Oppldots3d
       
----- create Hamiltonian : Zeeman / magnetic
OppZeeman = Bx * (2*OppSx + OppLx) + By * (2*OppSy + OppLy) + Bz * (2*OppSz + OppLz);

----- create Hamiltonian for the ground state 1s2,3dn
GS_Hamiltonian = OppGrdU + OppGrdCF  + OppGrdSO + OppZeeman 
----- partial Hamiltonians for ELDiagrams
Hamiltonian_noCF     = OppGrdU + OppGrdSO + OppZeeman 
Hamiltonian_noZeeman = OppGrdU + OppGrdSO + OppGrdCF
 
--- Intermediate state of RIXS   ( 2p5 3dn+1) ------------------------------------------
----- create Hamiltonian : Coulomb -----
OppExiU = F0_3d3d_IS * OppF3d3d_0
        + F2_3d3d_IS * OppF3d3d_2
        + F4_3d3d_IS * OppF3d3d_4 		-- + F0_2p3d_IS * OppF2p3d_0     -- added
        + F2_2p3d_IS * OppF2p3d_2
        + G1_2p3d_IS * OppG2p3d_1
        + G3_2p3d_IS * OppG2p3d_3;
       

  ----- create Hamiltonian : Crystal Field
OppExiCF = Ds_IS * OppDsigma + Dt_IS * OppDtau + Dq_IS * Opp10Dq;		-- CF Hamiltonian for C3
   
----- create Hamiltonian : Spin Orbit
OppExiSO = SOCv_IS * Oppldots3d
		 + SOCc_IS * Oppldots2p;
		 
----- create Hamiltonian for the final state 1s1,3d,4p)n+1 -----
XASHamiltonian = OppExiU + OppExiCF  + OppExiSO + OppZeeman

--- Final state of RIXS of RIXS  (2p6 3dn) ------------------------------------------
----- create Hamiltonian : Coulomb -----
OppFinU = F0_3d3d_FS * OppF3d3d_0
        + F2_3d3d_FS * OppF3d3d_2
        + F4_3d3d_FS * OppF3d3d_4
		
----- create Hamiltonian : Crystal Field
OppFinCF = Ds_FS * OppDsigma + Dt_FS * OppDtau + Dq_FS * Opp10Dq;		-- CF Hamiltonian for C3

----- create Hamiltonian : Spin Orbit
OppFinSO = SOCv_FS * Oppldots3d
         + SOCc_FS * Oppldots2p;

----- create Hamiltonian for the final state 1s2,2p5,3dn+1 -----
XESHamiltonian = OppFinU + OppFinCF  + OppFinSO + OppZeeman 
		
-- in order to remove small values from the operator one can chop these
GS_Hamiltonian.Chop();
XASHamiltonian.Chop();
XESHamiltonian.Chop(); 	

-- *****************************************************************
-- END Operators module - Now the wavefunctions can be calculated
-- *****************************************************************